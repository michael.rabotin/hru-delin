#!/bin/bash
############################################################################
#
# MODULE:       hru-delin_step3.sh
# AUTHOR(S):    Julien Veyssier
# 
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################
# HRU-DELIN
# 3rd step : hru-delin_hrugen


MYPATH=`readlink -f $0`
MYDIR=`dirname $MYPATH`
. $MYDIR/tools.sh

function printUsage(){
    scriptName=`basename $MYPATH`
    echo "USAGE:"
    echo
    echo "$scriptName [-d|--debug] [-p|--nbprocess] [-c|--cluster] [-f|--file]"
    echo
    echo "-d|--debug : enable debug output"
    echo "-p|--nbprocess N : use N parallel processes (default=number of available CPU cores)"
    echo "-c|--cluster : mandatory for using on HIICS cluster"
    echo "-f|--file : path to hru-delin configuration file. DEFAULT VALUE: 'hrudelin_config.cfg'"
}

# allow empty arg list
if [ $? -ne 0 ] || [ $# -eq 0 ];
then
    printUsage
    exit
fi


DEBUG=""
CLUSTER=""
NBPROCESS=""
CONFIGFILE=""

while getopts 'dp:cf:' flag; do
    case "${flag}" in
        d) DEBUG='true' ;;
        p) NBPROCESS="${OPTARG}" ;;
        c) CLUSTER='true' ;;
        f) CONFIGFILE="${OPTARG}" ;;
        *) printUsage
            exit 1;;
    esac

done




if [ -z "$CONFIGFILE" ]; then
    CONFIGFILE=hrudelin_config.cfg
fi

if [ ! -f "$CONFIGFILE" ]; then
    echo "!!! file '$CONFIGFILE' does not exist"
    echo
    printUsage
    exit
fi

echo "Config file: $CONFIGFILE"
CONFIGFILEPATH=`dirname $CONFIGFILE`
# get absolute path of config file directory
CONFIGFILEPATH=`readlink -f $CONFIGFILEPATH`

## predict the environment
grassDbPath=$CONFIGFILEPATH/grass_db
rc_file=$grassDbPath/grassdata/hru-delin/.grassrc

# find grass base dir only if no calcul on cluster 
if [ -z "$CLUSTER" ]; then
    res=`find /usr/lib/grass8* -name "r.thin"`
    if [ -z "$res" ]; then
        echo 'Grass8x not found, did you install grass package?'
        exit 1
    else
        par=`dirname "$res"`
        par2=`dirname "$par"`
        export GISBASE="$par2"
    fi
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GISBASE/lib
    export PYTHONPATH=$PYTHONPATH:$GISBASE/etc/python
fi

export PATH=$PATH:$GISBASE/bin:$GISBASE/scripts
# DEBUG
if [ ! -z "$DEBUG" ]; then
    echo
    echo "execute following lines to be able to call grass tools from this terminal:"
    echo
    echo export GISRC=$rc_file
    echo export GISBASE="$par2"
    echo export PATH=$PATH:$GISBASE/bin:$GISBASE/scripts
    echo export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GISBASE/lib
    echo export PYTHONPATH=$PYTHONPATH:$GISBASE/etc/python
    echo
fi
# ----------------------
# clean work environment
# ----------------------
clean_files=`grep "files\s*:" $CONFIGFILE | cut -d ':' -f2 | sed -e 's/\s*$//' | sed -e 's/^\s*//'`
if [ -z "$clean_files" ]; then
    clean_files=`grep "files\s*=" $CONFIGFILE | cut -d '=' -f2 | sed -e 's/\s*$//' | sed -e 's/^\s*//'`
fi

#if [ -z "$clean_files" ]; then
#    echo "------------> ERROR : Output FILE Directory not provided !"
#    exit 1
#fi

# is the path absolute?
#if [[ "$clean_files" = /* ]]; then
#    rm -f $clean_files/step3*
#else
#    rm -f $CONFIGFILEPATH/$clean_files/step3*
#fi
#clean_results=`grep "results\s*:" $CONFIGFILE | cut -d ':' -f2 | sed -e 's/\s*$//' | sed -e 's/^\s*//'`
#if [ -z "$clean_results" ]; then
#    clean_results=`grep "results\s*=" $CONFIGFILE | cut -d '=' -f2 | sed -e 's/\s*$//' | sed -e 's/^\s*//'`
#"fi

#if [ -z "$clean_results" ]; then
#    echo "------------> ERROR : Output RESULTS Directory not provided !"
#    exit 1
#fi

# is the path absolute?
#if [[ "$clean_results" = /* ]]; then
#    rm -rf $clean_results
#    mkdir $clean_results
#else
#    rm -rf $CONFIGFILEPATH/$clean_results
#    mkdir $CONFIGFILEPATH/$clean_results
#fi

# test if mkdir works
if [ $? -ne 0 ] ; then
    echo "------------> ERROR : Impossible to create Output directory !"
    exit 1
fi

# -----------------------------------
# exec third step of HRU-DELIN batch
# -----------------------------------

python3 $MYDIR/../modules/hrudelin_3_1_generate_hru.py $CONFIGFILE $NBPROCESS
python3 $MYDIR/../modules/hrudelin_3_2_merging_hru.py $CONFIGFILE $NBPROCESS
