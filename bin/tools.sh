#!/bin/bash
############################################################################
#
# MODULE:       tools.sh
# AUTHOR(S):    Julien Veyssier
# 
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################

buildGrassEnv() {
    envPath=$1
    location=$2
    if [ -d "$envPath" ]; then
        rm -rf "$envPath"
    fi
    gisdbase=$envPath/grassdata
    mkdir -p $gisdbase

    buildGrassLocation $envPath $location
}

buildGrassLocation() {
    envPath=$1
    location=$2

    gisdbase=$envPath/grassdata
    mapset=PERMANENT

    mkdir -p $gisdbase/$location/$mapset
    gisdbase=`readlink -f $gisdbase`
    rc_file=$gisdbase/$location/.grassrc

    echo "GISDBASE: $gisdbase" > $rc_file
    echo "LOCATION_NAME: $location" >> $rc_file
    echo "MAPSET: $mapset" >> $rc_file

    echo "proj:       1
zone:       0
north:      1
south:      0
east:       1
west:       0
cols:       1
rows:       1
e-w resol:  1
n-s resol:  1
top:        1
bottom:     0
cols3:      1
rows3:      1
depths:     1
e-w resol3: 1
n-s resol3: 1
t-b resol:  1
" > $gisdbase/$location/$mapset/WIND

    cp $gisdbase/$location/$mapset/WIND $gisdbase/$location/$mapset/DEFAULT_WIND
}
