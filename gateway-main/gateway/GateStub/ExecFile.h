//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef GATEWAY_EXECFILE_H
#define GATEWAY_EXECFILE_H

#include "OutputFile.h"

namespace gateway {

class ExecFile: public File {
private:
	OutputFile *output;
	std::string cmd;
	std::string cmdLinux = "";

public:
	ExecFile(const std::string &path, const std::string &name, OutputFile *output, const std::string &cmd);
	ExecFile(const std::string &path, const std::string &name, OutputFile *output, const std::string &cmd, const std::string &cmdLinux);

	std::string run(const std::string& jsonLine) const;
};
} // gateway

#endif //GATEWAY_EXECFILE_H
