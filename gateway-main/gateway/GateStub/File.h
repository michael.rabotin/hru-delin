//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef GATEWAY_FILE_H
#define GATEWAY_FILE_H

#include <string>

namespace gateway {

class File {
protected:
	std::string path;
	std::string name;

public:
	File(const std::string &path, const std::string &name);

	virtual ~File() =0;

	const std::string &getPath() const;
	const std::string &getName() const;
};

} // gateway

#endif //GATEWAY_FILE_H
