//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include <fstream>
#include "OutputFile.h"
#include "../lib/json.hpp"

using json = nlohmann::json;

namespace gateway {
//private
	std::string OutputFile::read() {
		if(this->name != "" && this->path != "") {
			std::ifstream file (this->path + this->name);
			if(!file.is_open()) {
				return "{}";
			}

			std::string line, output = "";
			while ( getline (file,line) )
			{
				output += line;
			}
			file.close();

			return output;
		}
		return "{}";
	}

//public
	OutputFile::OutputFile(const std::string &path, const std::string &name) : File(path, name) {}


	int OutputFile::initialize() const {
		if(this->name != "" && this->path != "") {
			std::ofstream file(this->path + this->name);

			file << R"( {"Outputs":[]})" << std::endl;

			file.close();
			return 0;
		}
		return -1;
	}



} // gateway