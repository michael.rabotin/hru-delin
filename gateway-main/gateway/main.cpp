//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include <iostream>
#include <string>
#include <vector>
#include "lib/json.hpp"

#include "GateStub/GateStub.h"

using namespace std;
using json = nlohmann::json;

int main(int argc, char *argv[]) {
    string args;
    for(int k = 0; k < argc; k++) {
		if(k != 0)
       		args += string(argv[k]);
    }

	// ===============CODE START===============
	
	cout << args << endl;
	gateway::GateStub *gateStub = new gateway::GateStub(args);


	cout << " =============== Running program =============== " << endl;
	cout << gateStub->getExecFile()->run(gateStub->getJsonLine()) << endl;

	cout << " =============== Program ending ================ " << endl;

    return 0;
}
