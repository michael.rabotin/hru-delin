//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_EXECFILE_H
#define CPPGATE_EXECFILE_H

#include "File.h"

class ExecFile: public File {
private:
    std::string cmd;
    std::string cmdAlt;

public:
    ExecFile(std::string path, std::string name, std::string cmd, std::string cmdAlt = "");

    std::string serialize() const override;

    void display() const override;
};


#endif //CPPGATE_EXECFILE_H
