//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include "SenderStub.h"

SenderStub::SenderStub(ExecFile *execFile, OutputFile *outputFile) : Stub(outputFile) {
    this->execFile = execFile;
}

void SenderStub::run(std::string gatePath, std::string gateName) {
    std::string command = gatePath + gateName + " " + this->getSentLine();

    std::cout << " =============== Running gateway =============== " << std::endl;
    FILE *file = popen(command.c_str(), "r");

    char buffer[100];
    std::string stringBuff;

    if (file == nullptr) perror ("Error opening file");
    else {
        while ( !feof(file) ) {
            if ( fgets (buffer , 100 , file) == nullptr ) break;
            stringBuff += buffer;
        }
        fclose (file);
    }
    std::cout << stringBuff << std::endl;
    std::cout << " =============== Gateway ending ================ " << std::endl;

}

std::string SenderStub::getSentLine() const {
    std::string line = this->serialize();
    int pos = line.find("\"");
    while(pos != std::string::npos) {
        line.replace(pos, 1, "\\\"");
        pos = line.find("\"", pos + 2);

        std::cout << line << std::endl;
        std::cout << pos << " " << std::string::npos << std::endl;
    }

    return line;
}

std::string SenderStub::serialize() const {
    std::string output = this->execFile->serialize();

    output.pop_back(); output += ", ";
    std::string outputFile = this->outputFile->serialize();
    outputFile.erase(outputFile.begin());
    output += outputFile;

    output.pop_back(); output += R"(, "Dictionaries": [)";

    for(int k = 0; k < this->dictionaries.size(); k++) {
        output += this->dictionaries[k]->serialize();
        if(k != this->dictionaries.size()-1)
            output += ", ";
    }
    output += "]}";

    return output;
}
std::string SenderStub::displayExecFile() const{
    std::string output = this->serialize();

    std::cout << output << std::endl;
    return output;
}
std::string SenderStub::displayAll() const{
    std::string output = this->displayExecFile() + Stub::displayAll();

    std::cout << output << std::endl;
    return output;
}

ExecFile *SenderStub::getExecFile() const {
    return execFile;
}

void SenderStub::setExecFile(ExecFile *execFile) {
    SenderStub::execFile = execFile;
}
