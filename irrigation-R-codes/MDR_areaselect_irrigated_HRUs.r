#~******************************************************************************
#~* Selects the HRUs where irrigation is to be deployed, based on their area 
#~* , canton location (irrigated or not) and comparison to SAU_irr_in_canton
#~*   
#~* The area of diverse combinations of HRUs is compared to the SAU_irr_in_canton,
#~* starting with combinations of only 1 HRU in the canton, and increasing till
#~* being over the SAU_irr_in_canton.
#~* If the single-element alreading brings the HRU irrigated area above the SAU_irr_in_canton
#~* , the smallest HRU in the canton is irrigated and all others are not.
#~* The eligible combinations are tested in 3 passes with increasing tolerance to
#~* error in the total HRU irrigated area : 10 %, 30 % and 100 %.
#~*
#~* The results are 
#~*           * a vector irrigated (value : 0 or 1) with length: N_hrus_total
#~*           * a vector irrig_type (value : 0, 1 or 2) with length: N_hrus_total.
#~*                          1 = asp  ; 2 = gag
#~*           * a vector area_error indicating the % of error in surface committed with the new HRUirr
#~*
#~* - Le programme calcule les indices des HRUs qui irriguent un canton, en utilisant l'algorithme de combinaison de nombres afin d'additionner les surfaces des HRUs pour trouver la surface la plus proche possible de la surface totale du canton.
#~* 
#~* - Le programme calcule également la différence en pourcentage entre la surface totale du canton et la surface totale des HRUs irriguant le canton. 
#~* 
#~* - Le programme écrit les résultats dans un fichier externe.
# 
#~* - Le fichier externe contient 5 colonnes. La première est le numéro du HRU, la deuxième est sa surface, la troisième indique si le HRU irrigue le canton (1) ou non (0), la quatrième donne le type d'irrigation du HRU et la cinquième donne la différence en pourcentage entre la surface totale du canton et la surface totale des HRUs irriguant le canton. 
#  %).

#~******************************************************************************
#~* PROGRAMMER: Isabelle GOUTTEVIN (Irstea Lyon)
#~******************************************************************************
#~* CREATED/MODIFIED:
# Created 2015-12-09 by Isabelle GOUTTEVIN (Irstea Lyon)
# Modified 2022-05-04 by Theo L (INRAE Lyon)
#~******************************************************************************
setwd("/home/tlabrosse/Bureau/maestro/irrigation-R-codes/")
library(combinat)
library(foreign)
source("lib/rgate/Stub.R")

rcvStub = ReceiverStub$new()


# *** FONCTIONS ***

# ------------------------------------------------------------------------
# index_of_nearest <- function(x, number){
#   Finds the index of the element in x that is closest to number.
# Args:
# x: A vector of numbers
# number: A number
# Returns:
# The index of the element in x that is closest to number
# ------------------------------------------------------------------------
index_of_nearest <- function(x, number){
    return (which(abs(x-number)==min(abs(x-number))))}




# ------------------------------------------------------------------------
#value_of_nearest(c(5,2,1),6)
# value_of_nearest <- function(x, number)
#   Finds the value of the element in x that is closest to number.
# Args:
# x: A vector of numbers
# number: A number
# Returns:
# The value of the element in x that is closest to number
# ------------------------------------------------------------------------
value_of_nearest <- function(x, number){
    return (x[which(abs(x-number)==min(abs(x-number)))])}




# ------------------------------------------------------------------------
# try_combination <- function(n, S_HRUs, S_irr_Canton, tolerance)
#   Tries to find the combination of HRUs that best fits the given irrigation area.
# Args:
# n: The current number of HRUs to be added to the combination
# S_HRUs: A vector of HRUs' surface
# S_irr_Canton: The target irrigation area
# tolerance: The maximum error tolerated by the user
# Returns:
# The index of the HRUs that best fit the target irrigation area,
#   " continue " if the current combination does not work but a smaller combination might,
#   " non convergence " if the current combination does not work and neither does a smaller combination.
# ------------------------------------------------------------------------
try_combination <- function(n, S_HRUs, S_irr_Canton, tolerance){

    if (n < length(S_HRUs)){
        combi <- combn(S_HRUs, n)
    } else {
        combi <- t(t(S_HRUs))
    }
    sumcombi <- apply(combi, 2, sum)


    nearestarea <- value_of_nearest(sumcombi, S_irr_Canton)
    error_nearest <- abs(1-nearestarea/S_irr_Canton)*100.

    if (error_nearest[1] < tolerance){

        combi_selected <- index_of_nearest(sumcombi, S_irr_Canton)
        index_selected <- NULL
        for (i in 1:n){
            index_selected <- c(index_selected, which(S_HRUs==combi[, combi_selected][i]))
        }
        return (index_selected)

    } else if (min(sumcombi) > S_irr_Canton){

        if (n==1){
            return(which(sumcombi==min(sumcombi)))
        } else {
            return ("non convergence")
        }
    } else {
        return ("continue")
    }
}

# ------------------------------------------------------------------------
# main <- function(hrus_irrig_cantons_filePath, cantons_irrigues_filePath)
#   Main function of the irrigation assignment process.
# Args:
# hrus_irrig_cantons_filePath: The path to the HRUs irrigated cantons data file
# cantons_irrigues_filePath: The path to the cantons irrigated data file
# Returns:
# A file with the irrigation status of every HRU
# ------------------------------------------------------------------------
main <- function(hrus_irrig_cantons_filePath, cantons_irrigues_filePath, output_dir) {
    hrus_irrig_cantons <- read.dbf(hrus_irrig_cantons_filePath)
    cantons_irrigues <- read.dbf(cantons_irrigues_filePath)



    # mélange des lignes pour pouvoir avoir un résultat vraiment aléatoire
    hrus_irrig_cantons= hrus_irrig_cantons[sample(seq_len(nrow(hrus_irrig_cantons))), ]
    cantons_irrigues= cantons_irrigues[sample(seq_len(nrow(cantons_irrigues))), ]


    N_hru <- dim(hrus_irrig_cantons)[1]

    # creates two vector of the size of the number of currently irrigated HRUs
    irrigated <- rep(0, N_hru)
    area_error <- rep(0, N_hru)

    # creates a vector of the size of the number irrigated cantons
    canton_traite <- rep(0, dim(cantons_irrigues)[1])


    tolerances <- c(10, 30, 100)

    for(tolerance in tolerances) {
        for (numcanton in cantons_irrigues$CODE_CAN_1[which(canton_traite==0)]){

            indice_canton <- which(cantons_irrigues$CODE_CAN_1==numcanton)

            # Find the HRU of the current canton
            hrus <- hrus_irrig_cantons[which(hrus_irrig_cantons$CODE_CAN_1==numcanton), ]

            if (dim(hrus)[1]<=0){
                canton_traite[indice_canton] <- 1
            } else {
                indices <- which(hrus_irrig_cantons$CODE_CAN_1==numcanton) # trouve le(s) HRU(s) associe au canton etudie
                S_HRUs <- hrus$AREA # surface du/des HRU(s) en m2

                S_irr_Canton <- cantons_irrigues[which(cantons_irrigues$CODE_CAN_1==numcanton), ]$SAU_IRR*100. # le "*100" lie au fait que les donnees du RGA sont en ares = 100m2

                index_of_HRUs <- "continue"
                n_elements_combi <- 1
                while ((index_of_HRUs=="continue") && (n_elements_combi <= length(S_HRUs))){
                    index_of_HRUs <- try_combination(n_elements_combi, S_HRUs, S_irr_Canton, tolerance)
                    n_elements_combi <- n_elements_combi+1
                }

                if (index_of_HRUs=="non convergence" || index_of_HRUs=="continue") {
                    irrigated[indices] <- NA
                } else {
                    irrigated[indices] <- 0
                    irrigated[indices[index_of_HRUs]] <- 1
                    area_error[indices] <- (sum(S_HRUs[index_of_HRUs])/S_irr_Canton-1)*100.

                    canton_traite[indice_canton] <- 1
                }
            }
        }
    }


    irrig_type <- rep(0, N_hru)

    # il est entrain de mettre un vector dans chaque case du vector là, je me trompe ?
    irrig_type[which(irrigated >0)] <- hrus_irrig_cantons$IRRIG_TYPE[which(irrigated >0)]

    
    file = output_dir;
    write.table(cbind(hrus_irrig_cantons$CAT,hrus_irrig_cantons$AREA, irrigated, irrig_type, area_error),file ,append=F, sep="\t", row.names=FALSE, col.names=c('HRUnum', 'HRUarea', 'irrigated', 'irrig_type', 'area_error'))
}

# *** MAIN CODE ***
# -----------------

hruFile = rcvStub$getArgument("HRU_file")$value
cantonFile = rcvStub$getArgument("cantons_file")$value
output_dir = rcvStub$getArgument("output_dir")$value

# utilise des données par défauts s'il n y a rien d'autres
if(is.null(cantonFile) || is.null(hruFile)) {
    cantonFile = "/home/tlabrosse/Bureau/maestro/irrigation-R-codes/Irrigation/Shapes/Cantons_irrigues.dbf"
    hruFile = "/home/tlabrosse/Bureau/maestro/irrigation-R-codes/Irrigation/Shapes/hrus_irriguees_sur_Rhone.dbf"
}

main(hruFile,
    cantonFile,
    output_dir
)





# Annexe : creation de la table des surfaces irriguees modelisees par canton

# library(foreign) 

# thats is probably not the right file
# HRU_Aleatoir <- read.dbf('~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig/hrus_irriguees_Aleatoires.dbf')
# SHRUirr_can <- NULL
# for (un_canton in sort(unique(HRU_Aleatoir$CODE_CAN_1))){
#     SHRUirr_can <- c(SHRUirr_can, sum(HRU_Aleatoir[which(HRU_Aleatoir$CODE_CAN_1 == un_canton),]$AREA/100)) #ares
# }

# write.table(cbind(sort(unique(HRU_Aleatoir$CODE_CAN_1)),SHRUirr_can),'/home/tlabrosse/Bureau/maestro/irrigation-R-codes/resultats OUT/Bilan_HRU_Aleatoir.txt',append=F, sep="\t", row.names=FALSE, col.names=c('canton', 'HRUirrig_area'))

