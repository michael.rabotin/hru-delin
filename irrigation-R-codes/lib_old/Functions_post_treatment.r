############# Creation du fichier hrus.par pour prise en compte relief  
#####################################################################################
#Inputs :
# - hrus : le fichier parametre hrus.par charge
# - seuil : valeur de la pente a partir de laquelle on va modifier 
# - chemin_hrus : chemin dans lequel se trouve le fichier hrus.par initial
#
# Fonction :
# - On verifie qu'on a bien des hrus pour laquelle la pente est superieure au seuil fixe 
# - On s'assure qu'on n'a pas deja augmente les parametres (hgeo <= 8)
# - On ajoute le jeu montagne (hgeo + 8 , landuse + 8, soil + 4)
# - On ecrit le nouveau fichier hrus.par dans le dossier Modif
#
# Output :
# - le fichier hrus.par actualise 
#####################################################################################
Relief = function (hrus,seuil,chemin_hrus) {
dir.create(paste(chemin_hrus,'Modif/',sep=''))
cheminHRUmodif = paste(chemin_hrus,'Modif/',sep='')
nbLines = Lignes_saut(chemin_hrus,'hrus.par')
headerHRU = readLines(paste(chemin_hrus,'hrus.par',sep=''), n = nbLines)
LinesNames = which(substr(headerHRU,1,2)=="ID")
Names = read.table(paste(chemin_hrus,'hrus.par',sep=''),nr=1,skip=LinesNames-1)
indice = which((hrus[,which(Names == "slope")] > seuil) & (hrus[,which(Names=="hgeoID")] <= 8))
if (length(indice !=0)){
hrus[indice,which(Names=="hgeoID")] = hrus[indice,which(Names=="hgeoID")] + 8
hrus[indice,which(Names=="landuseID")] = hrus[indice,which(Names=="landuseID")] + 8
hrus[indice,which(Names=="soilID")] = hrus[indice,which(Names=="soilID")] + 4
} else {
print(paste('Pas de modifications : pas de pentes superieures a ',seuil,' deg ou indices deja modifies!', sep=''));flush.console()
}                                                                                       
write.table(headerHRU,paste(cheminHRUmodif,'hrus.par',sep=''),sep='\t',col.names=F,row.names=F,quote=F)
write.table(hrus,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F,row.names=F,quote=F,append=T,sep='\t')
hrus
}


#Correction des pentes des brins inferieures a seuil_min
#####################################################################################
#Inputs :
# - reach : le fichier parametre reach.par charge
# - seuil_min : valeur seuil de la pente - toute pente inferieure au seuil sera augmentee
# - chemin_reach : chemin dans lequel se trouve le fichier reach.par initial
#
# Fonction :
# - On verifie qu'on a bien des brins pour laquelle la pente est inferieure au seuil fixe 
# - On augmente la valeur des pentes des brins reperes
# - On ecrit le nouveau fichier reach.par dans le dossier Modif
#
# Output :
# - le fichier reach.par actualise 
#####################################################################################
CorrectionPenteminReach = function (reach,seuil_min,chemin_reach){
dir.create(paste(chemin_reach,'Modif/',sep=''))
cheminReachmodif = paste(chemin_reach,'Modif/',sep='')
nbLines = Lignes_saut(chemin_reach,'reach.par')
headerReach = readLines(paste(chemin_reach,'reach.par',sep=''), n = nbLines)
LinesNames = which(substr(headerReach,1,2)=="ID")
Names = read.table(paste(chemin_reach,'reach.par',sep=''),nr=1,skip=LinesNames-1)
indice = which(as.numeric(as.character(reach[,which(Names == "slope")])) < seuil_min)
if(length(indice)!=0){
reach[indice,which(Names == "slope")] <- seuil_min}
else{
print(paste('Toutes les pentes des brins sont superieures a ',seuil_min,' %!', sep=''));flush.console()
}

reach = reach[order(reach[,1]),]
write.table(headerReach,paste(cheminReachmodif,'reach.par',sep=''),sep='\t',col.names=F,row.names=F,quote=F)
write.table(reach,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
reach
}


# Correction largeurs et rugosite (en remplacement des valeurs 30 et 1 m pour la rugosite et la largeur des brins en sortie de HRU-delin et GRASS-HRU)
#####################################################################################
#Inputs :
# - reach : le fichier parametre reach.par charge
# - SurfaceDrainee : un tbleau de 2 colonnes (1ere colonne : n?brin, 2eme colonne : surface drainee a l'exutoire du brin
# - chemin_reach : chemin dans lequel se trouve le fichier reach.par initial
#
# Fonction :
# - En fonction de l'aire drainee par le brin, on  affecte une largeur et une rugosite a ce dernier
# - On ecrit le nouveau fichier reach.par dans le dossier Modif
#
# Output :
# - le fichier reach.par actualise 
#####################################################################################
CorrectionLargRug = function (reach,SurfaceDrainee,chemin_reach){
dir.create(paste(chemin_reach,'Modif/',sep=''))
cheminReachmodif = paste(chemin_reach,'Modif/',sep='')
nbLines = Lignes_saut(chemin_reach,'reach.par')
headerReach = readLines(paste(chemin_reach,'reach.par',sep=''), n = nbLines)
LinesNames = which(substr(headerReach,1,2)=="ID")
Names = read.table(paste(chemin_reach,'reach.par',sep=''),nr=1,skip=LinesNames-1)
indID = which(Names == "ID")
indRug = which(Names == "rough")
indLarg = which(Names == "width")

brin0 <- SurfaceDrainee[which(SurfaceDrainee[,2] < 100 ),1]
for (brin in brin0){
reach[which(reach[,indID] == brin),indRug] = 15
reach[which(reach[,indID] == brin),indLarg] = 10
}

brin0 <- SurfaceDrainee[which(SurfaceDrainee[,2] >= 100 &  SurfaceDrainee[,2] < 400),1]
for (brin in brin0){
reach[which(reach[,indID] == brin),indRug] = 30
reach[which(reach[,indID] == brin),indLarg] = 15
}

brin0 <- SurfaceDrainee[which(SurfaceDrainee[,2] >= 400 &  SurfaceDrainee[,2] < 1350),1]
for (brin in brin0){
reach[which(reach[,indID] == brin),indRug] = 40
reach[which(reach[,indID] == brin),indLarg] = 30
}

brin0 <- SurfaceDrainee[which(SurfaceDrainee[,2] >= 1350 &  SurfaceDrainee[,2] < 7000),1]
for (brin in brin0){
reach[which(reach[,indID] == brin),indRug] = 50
reach[which(reach[,indID] == brin),indLarg] = 50
}

brin0 <- SurfaceDrainee[which(SurfaceDrainee[,2] >= 7000 &  SurfaceDrainee[,2] < 20500),1]
for (brin in brin0){
reach[which(reach[,indID] == brin),indRug] = 60
reach[which(reach[,indID] == brin),indLarg] = 100
}

brin0 <- SurfaceDrainee[which(SurfaceDrainee[,2] >= 20500),1]
for (brin in brin0){
reach[which(reach[,indID] == brin),indRug] = 70
reach[which(reach[,indID] == brin),indLarg] = 200
}

reach = reach[order(reach[,1]),]
write.table(headerReach,paste(cheminReachmodif,'reach.par',sep=''),sep='\t',col.names=F,row.names=F,quote=F)
write.table(reach,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
reach
}


#Correction des pentes des HRUs inferieures a seuil_min
#####################################################################################
#Inputs :
# - hrus : le fichier parametre hrus.par charge
# - seuil_min : valeur seuil de la pente - toute pente inferieure au seuil sera augmentee
# - chemin_hrus : chemin dans lequel se trouve le fichier hrus.par initial
#
# Fonction :
# - On verifie qu'on a bien des HRUs pour laquelle la pente est inferieure au seuil fixe 
# - On augmente la valeur des pentes des hrus reperees
# - On ecrit le nouveau fichier hrus.par dans le dossier Modif
#
# Output :
# - le fichier hrus.par actualise 
#####################################################################################
CorrectionPenteminHRU = function (hrus,seuil_min,chemin_hrus){
dir.create(paste(chemin_hrus,'Modif/',sep=''))
cheminHRUmodif = paste(chemin_hrus,'Modif/',sep='')
nbLines = Lignes_saut(chemin_hrus,'hrus.par')
headerHRU = readLines(paste(chemin_hrus,'hrus.par',sep=''), n = nbLines)
LinesNames = which(substr(headerHRU,1,2)=="ID")
Names = read.table(paste(chemin_hrus,'hrus.par',sep=''),nr=1,skip=LinesNames-1)
indice = which(as.numeric(as.character(hrus[,which(Names == "slope")])) < seuil_min)
if(length(indice)!=0){
hrus[indice,which(Names == "slope")] <- seuil_min}
else{
print(paste('Toutes les pentes des HRUs sont superieures a ',seuil_min,' deg!', sep=''));flush.console()
}
write.table(headerHRU,paste(cheminHRUmodif,'hrus.par',sep=''),sep='\t',col.names=F,row.names=F,quote=F)
write.table(hrus,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
hrus
}


#Chargement des fichiers parametres
#####################################################################################
#Inputs :
# - chemin : le dossier dans lequel se situe le fichier desire
# - Name : le nom du fichier desire
#
# Fonction :
# - Charger le fichier parametre hrus.par ou reach.par
# 
# Output :
# - le fichier parametre desire
# Astuce :
# - Le code est capable d'identifier la ligne avec les premieres valeurs et de sauter les lignes de texte initiales
#####################################################################################
Chargement_param = function(chemin,Name){ # NTK ???? (aussi dans utilitaires :3)
k=0
obj = NULL
obj2 = NULL
obj3 = NULL
while(length(na.omit(obj))==0 | length(na.omit(obj2))==0 | length(na.omit(obj3))==0){
obj = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k,colClasses="character"))[1]
obj2 = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k+1,colClasses="character"))[1]
obj3 = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k+2,colClasses="character"))[1]
k=k+1}
nbLines = k - 1
read.table(paste(chemin,Name,sep=''),skip=nbLines)
}

#Fonction pour calculer le nombre de lignes a sauter en lecture du fichier paramatre
#####################################################################################
#Inputs :
# - chemin : le dossier dans lequel se situe le fichier desire
# - Name : le nom du fichier desire
#
# Fonction :
# - Identifier le nombre de lignes a sauter en lecture
# Output :
# - le nombre de lignes a ignorer
#####################################################################################
Lignes_saut = function(chemin,Name){ #NTK
k=0
obj = NULL
obj2 = NULL
obj3 = NULL
while(length(na.omit(obj))==0 | length(na.omit(obj2))==0 | length(na.omit(obj3))==0){
obj = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k,colClasses="character"))[1]
obj2 = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k+1,colClasses="character"))[1]
obj3 = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k+2,colClasses="character"))[1]
k=k+1}
nbLines = k - 1
nbLines
}


# Ajout des informations sur les barrages dans le fichier reach.par
#####################################################################################
#Inputs :
# - chemin_reach : le dossier dans lequel se situe le fichier reach.par original
# - reach : le fichier parametre reach.par charge
# - liste_barrage : la liste des barrages (issu du fichier csv) - necessite d'avoir une colonne avec le nom du barrage (intitulee 'Nom') la capacite max (en Mm3), le volume initial (en Mm3) et le brin sur lequel s'applique le barrage 
# - Bar_colSmax : colonne comportant les capacites maximales des barrages
# - Bar_colV0 : colonne comportant les volumes initiaux des barrages (au 01/01/1985)
# - Bar_colBrinSortie : colonne comportant le n? du brin de sortie du barrage (brin directement a l'aval du barrage)
#
# Fonction :
# - Une fois la liste de barrages chargee, on affecte a chaque brin present dans la colonne BrinSortie la capacite max et le volume initial du barrage correspondant
# 
#Output :
# - le fichier reach.par actualise
#####################################################################################
Ajout_barrage_reach = function (chemin_reach,reach,liste_barrage,Bar_colSmax,Bar_colV0,Bar_colBrinSortie){
cheminReachmodif = paste(chemin_reach,'Modif/',sep='')
nbLines = Lignes_saut(chemin_reach,'reach.par')
headerReach = readLines(paste(chemin_reach,'reach.par',sep=''), n = nbLines)
LinesNames = which(substr(headerReach,1,2)=="ID")
Names = read.table(paste(chemin_reach,'reach.par',sep=''),nr=1,skip=LinesNames-1)
Names = cbind(Names,'Smax','V0')
Min = read.table(paste(chemin_reach,'reach.par',sep=''),nr=1,skip=LinesNames)
Min = cbind(Min,0,0)
Max = read.table(paste(chemin_reach,'reach.par',sep=''),nr=1,skip=LinesNames+1)
Max = cbind(Max,9999999,9999999)
Unit =  read.table(paste(chemin_reach,'reach.par',sep=''),nr=1,skip=LinesNames+2) 	
Unit = cbind(Unit,'Mm3','Mm3')
reach = cbind(reach,0,0)
for (i in c(1:dim(liste_barrage)[1])){
X = which(reach[,1]==liste_barrage[i,Bar_colBrinSortie])
if (length(X) != 0){
reach[X,(dim(reach)[2]-1)] = liste_barrage[i,Bar_colSmax]
reach[X,(dim(reach)[2])] = liste_barrage[i,Bar_colV0]
} else {
print(paste("attention, pas de brin correspondant au brin mentionne pour le barrage ", as.character(liste_barrage[i,'Nom']),sep=""));flush.console()}
}
write.table (Names,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=F)
write.table (Min,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (Max,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (Unit,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (reach,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
reach
}


# Modification des paramatres dans hrus.par pour la Saone et la Dombes
#####################################################################################
#Inputs :
# - hrus : le fichier parametre hrus.par charge
# - chemin_hrus : le dossier dans lequel se situe le fichier reach.par original
# - reach : le fichier parametre reach.par charge
# - BrinSaoneAval : brin correspondant a l'exutoire de la Saone jusqu'ou nous souhaitons apporter la modification sur les parametres sols, geologie et landuse
# - Dombes_Chalaronne : brin exutoire de la Chalaronne, a sa confluence avec la Saone
# - Dombes_Veyle : brin exutoire de la Veyle, a sa confluence avec la Saone
#
# Fonction :
# - Identifier les hrus ayant les parametres a modifier (2 et 10 pour le landuseSaone, 4 et 12 pour le landuseDombes, 1 et 5 pour le soilSaone) et effectuer la modification
#
# Output :
# - le fichier hrus.par actualise 
#####################################################################################
Traitement_Saone = function(hrus,chemin_hrus,reach,BrinSaoneAval,Dombes_Chalaronne,Dombes_Veyle){
cheminHRUmodif = paste(chemin_hrus,'Modif/',sep='')
nbLines = Lignes_saut(cheminHRUmodif,'hrus.par')
headerHRU = readLines(paste(cheminHRUmodif,'hrus.par',sep=''), n = nbLines)
LinesNames = which(substr(headerHRU,1,2)=="ID")
Names = read.table(paste(cheminHRUmodif,'hrus.par',sep=''),nr=1,skip=LinesNames-1)

indID = which(Names=="ID")
indGeol = which(Names=="hgeoID")
indLand = which(Names=="landuseID")
indSoil = which(Names=="soilID")
indSub = which(Names=="subbasin")
#GeolSaoneMod = hrus[which(hrus[,indGeol] == 8 | hrus[,indGeol] == 16),indID]
LanduseSaoneMod = hrus[which(hrus[,indLand] == 2 | hrus[,indLand] == 10),indID]
LanduseDombesMod = hrus[which(hrus[,indLand] == 4 | hrus[,indLand] == 12),indID]
SoilSaoneMod = hrus[which(hrus[,indSoil] == 1 | hrus[,indSoil] == 5),indID]
brins_saone = Topologie(BrinSaoneAval,reach) 
brins_chala = Topologie(Dombes_Chalaronne,reach) 
brins_veyle = Topologie(Dombes_Veyle,reach) 
ind = NULL
Total_hru_Saone = NULL   
for (k in brins_saone){
Total_hru_Saone = c (Total_hru_Saone,hrus[hrus[,indSub] == k,indID])}

Total_hru_Chala = NULL   
for (k in brins_chala){
Total_hru_Chala = c (Total_hru_Chala,hrus[hrus[,indSub] == k,indID])}
       
Total_hru_Veyle = NULL   
for (k in brins_veyle){
Total_hru_Veyle = c (Total_hru_Veyle,hrus[hrus[,indSub] == k,indID])}
                           
for (k in Total_hru_Saone){
#if(length(which(k == GeolSaoneMod)) != 0) {hrus[which(k == hrus[,indID]),indGeol] <- 17}
#if(length(which(k == SoilSaoneMod)) != 0) {hrus[which(k == hrus[,indID]),indSoil] <- 9}
if(length(which(k == LanduseSaoneMod)) != 0) {hrus[which(k == hrus[,indID]),indLand] <- 17}
}

for (k in Total_hru_Chala){
if(length(which(k == SoilSaoneMod)) != 0) {hrus[which(k == hrus[,indID]),indSoil] <- 10}
if(length(which(k == LanduseDombesMod)) != 0) {hrus[which(k == hrus[,indID]),indLand] <- 18}
}

for (k in Total_hru_Veyle){
if(length(which(k == SoilSaoneMod)) != 0) {hrus[which(k == hrus[,indID]),indSoil] <- 10}
if(length(which(k == LanduseDombesMod)) != 0) {hrus[which(k == hrus[,indID]),indLand] <- 18}
}
write.table(headerHRU,paste(cheminHRUmodif,'hrus.par',sep=''),sep='\t',col.names=F,row.names=F,quote=F,append=F)
write.table(hrus,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F,row.names=F,quote=F,append=T,sep='\t')
hrus
}


# Mise en place de la parametrisation pour l'irrigation
#####################################################################################
#Inputs :
# - hrus : le fichier parametre hrus.par charge
# - chemin_hrus : le dossier dans lequel se situe le fichier reach.par original
# - HRUs_shp : le fichier cartographique des HRUs charge (produit de HRU-delin ou GRASS-HRU
# - Cantons_shp : le fichier cartographique des cantons irrigues charge
# - ColCultDom : Colonne dans le fichier des cantons irrigues donnant la culture dominante (attention, il est necessaire que les noms des cultures soient identiques a celles du tableau irrigation_table.csv pour la suite des traitements)
#
# Fonction :
# - Correction eventuelle de la colonne irrigated (sortie de HRU-delin) en cas de 1 et de landuse different des landuses agricoles
# - Identification des HRUs agricoles et irriguees
# - Pour chaque HRU irriguee, on croise avec les cantons irrigues, et on repere le canton le plus represente dans l'intersection
# - On enregistre n?HRU irriguee, Culture dominante et Canton correspondant dans HRUs_culture_test.csv
#
# Output :
# - le fichier hrus.par actualise 
#####################################################################################
CorrectionParamIrrig = function (hrus,chemin_hrus,HRUs_shp,Cantons_shp,ColCultDom){
dir.create(paste(chemin_hrus,'Modif/',sep=''))
cheminHRUmodif = paste(chemin_hrus,'Modif/',sep='')
nbLines = Lignes_saut(chemin_hrus,'hrus.par')
headerHRU = readLines(paste(chemin_hrus,'hrus.par',sep=''), n = nbLines)
LinesNames = which(substr(headerHRU,1,2)=="ID")
Names = read.table(paste(chemin_hrus,'hrus.par',sep=''),nr=1,skip=LinesNames-1)
indLand = which(Names == 'landuseID')
indIrr =  which(Names == 'irrigated')
if(length(indIrr) != 0){
indice = which(hrus[,indLand] != 4 & hrus[,indIrr] == 1 & hrus[,indLand] < 19)
if(length(indice) !=0){
hrus[indice,indIrr] = 0
}else {print("pas de hru ayant irrigated = 1 et landuse != 4 dans votre fichier hrus.par");flush.console()}
hrus_irrig = hrus[which(hrus[,indIrr] == 1),1]
write.table(t(c('HRU','Culture','Canton')),'C:/Users/tilmant/Desktop/Final/BILAN/Irrigation/HRUs_culture_test.csv',sep=';',quote=F,append=F,col.names=F,row.names=F)
for (k in c(1:length(hrus_irrig))){
BV2 = HRUs_shp[which(HRUs_shp$cat == hrus_irrig[k]),]
proj4string(BV2)<- proj4string(Cantons_shp)
BILAN=NULL
inters<- list()
states<- list()
pos<- which(!is.na(over(Cantons_shp,BV2)))
for (j in 1 : length(pos)) {
states[[j]]<- Cantons_shp[pos[j], 1]
inters[[j]]<- gIntersection(states[[j]],BV2)
if (class (inters[[j]]) == 'SpatialCollections'){inters[[j]]<- gIntersection(states[[j]],BV2)@polyobj}
BILAN = rbind(BILAN,c(Cantons_shp[pos[j],][[1]],area(inters[[j]])))
}
Cant <- BILAN[which(BILAN[,2] == max(BILAN[,2])),1]
write.table(t(c(hrus_irrig[k],as.character(Cantons_shp[[ColCultDom]][which(Cantons_shp@data [,1] == Cant)]),Cant)),'C:/Users/tilmant/Desktop/Final/BILAN/Irrigation/HRUs_culture_test.csv',sep=';',quote=F,append=T,col.names=F,row.names=F)
}
}else {
print("pas de colonne 'irrigated' dans votre fichier hrus.par");flush.console()
}
write.table(headerHRU,paste(cheminHRUmodif,'hrus.par',sep=''),sep='\t',col.names=F,row.names=F,quote=F,append=F)
write.table(hrus,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F,row.names=F,quote=F,append=T,sep='\t')
hrus
}

#Chargement des couches sig
#####################################################################################
#Inputs :
# - chemin : chemin complet du fichier a lire
#
# Fonction :
# - Identification du nom de la couche
# - lecture du fichier sig
#
# Output :
# - la couche desiree
#####################################################################################
Chargement_couche = function(chemin){
indice = NULL
for (i in c(1:nchar(chemin))){
Test = substr(chemin,i,i)
if(Test =="/"){indice = c(indice,i)}
}
readOGR (dsn=substr(chemin,1,indice[length(indice)]-1), layer=substr(chemin,indice[length(indice)]+1,nchar(chemin)-4))
}

#Fonction pour recreer la topologie a partir d'un brin donne
# The point of this function is to find all of the reachable nodes from a given node in a graph.
# The point of the function is to take in a vector of reachable nodes from a given node, and to return a vector of all nodes that can be reached from the original node.
#####################################################################################
#Inputs :
# - brin : brin pour lequel on veut la topologie 
# - reach : le fichier parametre reach.par charge
#
# Fonction :
# - Remontee depuis le brin choisi jusqu'a l'amont du bassin
#
# Output :
# - la liste des brins en amont du brin choisi
#####################################################################################
Topologie = function (brin,reach) { # NTK
IDs <- NULL
Brin0 <- brin
for (indice in c(1:1000)){
assign(paste('Brin',indice,sep=''),NULL)}
k =0
while (length(get(paste('Brin',k,sep='')))!=0){
for (i in c(1:length(get(paste('Brin',k,sep=''))))){
assign(paste('Brin',k+1,sep=''),c(get(paste('Brin',k+1,sep='')),reach[which(reach[,2]== get(paste('Brin',k,sep=''))[i]),1]))
}
k = k+1
}
Total  <- brin
for (l in c(1:k)){
Total <- unique(c(Total,get(paste('Brin',l,sep=''))))
}
Total
}

#Modification des sous-bassins pour les fichiers de HRU-delin (pour avoir brin exutoire = sous-bassin
#####################################################################################
#Inputs :
# - chemin_hrus : chemin dans lequel se trouve le fichier hrus.par initial
# - hrus : le fichier parametre hrus.par charge
#
# Fonction :
# - Recherche des hrus connectees aux brins
# - Remontee topologique des HRUs
# - Remplacement du code subbasin du fichier hrus.par pour coller a la nouvelle numerotation des brins (etape indispensable pour le fonctionnement du module d'irrigation)
#
# Output :
# - le fichier hrus.par actualise
#####################################################################################
Correction_subbasin = function (chemin_hrus,hrus){
cheminHRUmodif = paste(chemin_hrus,'Modif/',sep='')
nbLines = Lignes_saut(cheminHRUmodif,'hrus.par')
headerHRU = readLines(paste(cheminHRUmodif,'hrus.par',sep=''), n = nbLines)
LinesNames = which(substr(headerHRU,1,2)=="ID")
Names = read.table(paste(cheminHRUmodif,'hrus.par',sep=''),nr=1,skip=LinesNames-1)
indToReach = which(Names=="to_reach")
indToPoly = which(Names=="to_poly")
indSub = which(Names=="subbasin")
indID = which(Names=="ID")
HRUScon = hrus[which(hrus[,indToReach] !=0),indID]
for (k in HRUScon){
total = k
hrus_tot = NULL
hrus_tot = c(hrus_tot,hrus[which(hrus[,indToPoly]==k),indID])
if(length(hrus_tot) != 0){
newhrus2 = hrus_tot      
total = c(total,newhrus2)
while(length(newhrus2) !=0){
newhrus2 = NULL
for (i in hrus_tot){
newhrus = NULL
newhrus = hrus[which(hrus[,indToPoly]==i),indID]
newhrus2 = c(newhrus2,newhrus)
}
hrus_tot = newhrus2
total = c(total,newhrus2)
}}
for(m in total){hrus[which(hrus[,indID] ==m),indSub] <- hrus[which(hrus[,indID] ==k),indToReach]}
}
write.table(headerHRU,paste(cheminHRUmodif,'hrus.par',sep=''),sep='\t',col.names=F,row.names=F,quote=F,append=F)
write.table(hrus,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F,row.names=F,quote=F,append=T,sep='\t')
hrus
}



#S'assurer qu'il y a bien un brin affecte a chaque station (necessaire pour la Topologie)
#####################################################################################
#Inputs :
# - Correspondance : le fichier charge des correspondances brins/stations
# - chemin_hrus : chemin dans lequel se trouve le fichier hrus.par initial
# - hrus : le fichier parametre hrus.par charge
# - Corres_ID_Stations : l'objet faisant le lien entre ID et code station issu de la couche SIG des stations
#
# Fonction :
# - Verification que tous les watersheds presents dans hrus.par correspondent a une station presente dans le fichier de correspondance (necessaire pour pouvoir calculer l'aire des bassins)
#
# Output :
# - Message d'erreur en cas de manque - station a ajouter a la  main dans le fichier csv de correspondance
#####################################################################################
verification_correspondance = function (Correspondance,chemin_hrus,hrus,Corres_ID_Stations){
nbLines = Lignes_saut(chemin_hrus,'hrus.par')
headerHRU = readLines(paste(chemin_hrus,'hrus.par',sep=''), n = nbLines)
LinesNames = which(substr(headerHRU,1,2)=="ID")
Names = read.table(paste(chemin_hrus,'hrus.par',sep=''),nr=1,skip=LinesNames-1)
ID = as.numeric(Corres_ID_Stations[,1])
ID_hrus = unique(hrus[,which(Names=="watershed")])
indice = NULL
for (m in ID_hrus){
indice = c(indice,which(Corres_ID_Stations[,1] == m))
}
Stations_necessaires = Corres_ID_Stations[indice,2]
Bilan = NULL
for (k in Stations_necessaires){
if(length(which(as.character(Correspondance[,1]) == k)) == 0){Bilan = paste(Bilan,' - ',k,sep='')}
}
if(length(Bilan)!=0){
print(paste('Attention, il manque les stations ',Bilan,' dans le fichier Correspondance. Merci de les ajouter',sep=''));flush.console()
stop("Stations manquantes")}
}




#Ajout des parametres d'irrigation
#####################################################################################
#Inputs :
# - hrus : le fichier parametre hrus.par charge
# - chemin_hrus : chemin dans lequel se trouve le fichier hrus.par initial
# - irrigation_table : issu du fichier csv, relation landuseID,culture,type d'irrigation
# - hrus_irrig : la liste des hrus irriguees avec leurs cultures dominantes (issue du croisement des sig hrus/cantons irrigues
#
# Fonction :
# - Ajout de la colonne irrig_type (1 : Asp, 2 : GaG, 3 : Grav)
#
# Output :
# - le fichier hrus.par actualise
#####################################################################################
Irrigation_param = function (hrus,chemin_hrus,irrigation_table,hrus_irrig){
cheminHRUmodif = paste(chemin_hrus,'Modif/',sep='')
nbLines = Lignes_saut(cheminHRUmodif,'hrus.par')
headerHRU = readLines(paste(cheminHRUmodif,'hrus.par',sep=''), n = nbLines)
LinesNames = which(substr(headerHRU,1,2)=="ID")
Names = read.table(paste(cheminHRUmodif,'hrus.par',sep=''),nr=1,skip=LinesNames-1)
Names = cbind(Names,'irrigated','irrig_type')
Min = read.table(paste(cheminHRUmodif,'hrus.par',sep=''),nr=1,skip=LinesNames)
Min = cbind(Min,0,0)
Max = read.table(paste(cheminHRUmodif,'hrus.par',sep=''),nr=1,skip=LinesNames+1)
Max = cbind(Max,999,999)
Unit =  read.table(paste(cheminHRUmodif,'hrus.par',sep=''),nr=1,skip=LinesNames+2) 	
Unit = cbind(Unit,'n/a','n/a')

indLanduse = which(Names == "landuseID")
hrus <- cbind(hrus,0,0)
for (hru in hrus_irrig[,1]){
hrus[which(hrus[,1] == hru),(dim(hrus)[2]-1)] <- 1
indCult = irrigation_table[which(as.character(irrigation_table[,2]) == as.character(hrus_irrig[which(hrus_irrig[,1]== hru),2])),1]
irrig_type = as.character(irrigation_table[which(as.character(irrigation_table[,2]) == as.character(hrus_irrig[which(hrus_irrig[,1]== hru),2])),3])
if(irrig_type == "GaG"){indIrr = 2} else {if(irrig_type == "Asp"){indIrr = 1} else { if(irrig_type == "Grav"){indIrr = 3} else {print("type inconnu");flush.console()}}}
hrus[which(hrus[,1] == hru),indLanduse] = indCult
hrus[which(hrus[,1] == hru),dim(hrus)[2]] <- indIrr
}
write.table (Names,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=F)
write.table (Min,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (Max,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (Unit,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (hrus,paste(cheminHRUmodif,'hrus.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
hrus
}




#Regionalisation du TA
#####################################################################################
#Inputs :
# - chemin_reach : chemin dans lequel se trouve le fichier reach.par initial
# - reach : le fichier parametre reach.par charge
# - SurfaceDrainee : surface drainee pour chaque brin (objet issu de la fonction ou d'un fichier csv)
# - BrinSaoneAval : brin a partir duquel nous allons fixer les TA pour la Saone
# - BrinLemanAval : brin a partir duquel nous allons fixer tous les TA a 1 (sortie du Leman)
#
# Fonction :
# - Identifier les brins par surface drainee (4 classes : <=100, 100 > S >= 300 , 300 > S >= 25000 , S > 25000)
# - Recuperation des brins amont de la Saone
# - Recuperation des brins amont du Rhone (sortie du Leman) 
# - Regionalisation du TA selon la surface drainee et la position geographique (Saone, Leman, reste BV)
#
# Output :
# - le fichier reach.par actualise
#####################################################################################
TA = function(chemin_reach,reach,SurfaceDrainee,BrinSaoneAval,BrinLemanAval){
cheminReachmodif = paste(chemin_reach,'Modif/',sep='')
nbLines = Lignes_saut(cheminReachmodif,'reach.par')
headerReach = readLines(paste(cheminReachmodif,'reach.par',sep=''), n = nbLines)
LinesNames = which(substr(headerReach,1,2)=="ID")
Names = read.table(paste(cheminReachmodif,'reach.par',sep=''),nr=1,skip=LinesNames-1)
Names = cbind(Names,'TA')
Min = read.table(paste(cheminReachmodif,'reach.par',sep=''),nr=1,skip=LinesNames)
Min = cbind(Min,0)
Max = read.table(paste(cheminReachmodif,'reach.par',sep=''),nr=1,skip=LinesNames+1)
Max = cbind(Max,99)
Unit =  read.table(paste(cheminReachmodif,'reach.par',sep=''),nr=1,skip=LinesNames+2) 	
Unit = cbind(Unit,'n/a')
reach = cbind(reach,0.5)
brin0 <- SurfaceDrainee[which(SurfaceDrainee[,2] <= 100),1]
brin100 <- SurfaceDrainee[which(SurfaceDrainee[,2] > 100 & SurfaceDrainee[,2] <= 300),1]
brin300 <- SurfaceDrainee[which(SurfaceDrainee[,2] > 300 & SurfaceDrainee[,2] <= 25000),1]
brin25000 <- SurfaceDrainee[which(SurfaceDrainee[,2] > 25000),1]
reaches_saone <- Topologie (BrinSaoneAval,reach)
reaches_leman <- Topologie (BrinLemanAval,reach)

for (reach2 in brin0){
reach[which(reach[,1]==reach2),dim(reach)[2]] = 0.5
if (length(which(reaches_saone == reach2)) != 0){reach[which(reach[,1]==reach2),dim(reach)[2]] = 1}
if (length(which(reaches_leman == reach2)) != 0){reach[which(reach[,1]==reach2),dim(reach)[2]] = 1}
}

for (reach2 in brin100){
reach[which(reach[,1]==reach2),dim(reach)[2]] = 1.5
if (length(which(reaches_saone == reach2)) != 0){reach[which(reach[,1]==reach2),dim(reach)[2]] = 2}
if (length(which(reaches_leman == reach2)) != 0){reach[which(reach[,1]==reach2),dim(reach)[2]] = 1}
}
for (reach2 in brin300){
reach[which(reach[,1]==reach2),dim(reach)[2]] = 2
if (length(which(reaches_saone == reach2)) != 0){reach[which(reach[,1]==reach2),dim(reach)[2]] = 2}
if (length(which(reaches_leman == reach2)) != 0){reach[which(reach[,1]==reach2),dim(reach)[2]] = 1}
}
for (reach2 in brin25000){
reach[which(reach[,1]==reach2),dim(reach)[2]] = 10
if (length(which(reaches_saone == reach2))!= 0){reach[which(reach[,1]==reach2),dim(reach)[2]] = 10}
if (length(which(reaches_leman == reach2))!= 0){reach[which(reach[,1]==reach2),dim(reach)[2]] = 1}
}
write.table (Names,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=F)
write.table (Min,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (Max,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (Unit,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
write.table (reach,paste(cheminReachmodif,'reach.par',sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
reach
}


#Calcul de l'aire drainee a chaque brin
#####################################################################################
#Inputs :
# - reach : le fichier parametre reach.par charge
# - hrus : le fichier parametre hrus.par charge
# - chemin_hrus : chemin dans lequel se trouve le fichier hrus.par initial
#
# Fonction :
# - Recuperation des brins de reach
# - Si on est au dernier brin (exutoire du BV  => to_reach = 0), on somme la surface des HRUs ayant ce brin pour subbasin)
# - Pour un brin donne, on remonte vers ses "to_reach" :
#   - si le brin to_reach n'a jamais etudie, on calcule la somme des HRUs qui sont connectees a ce brin
#   - si le brin to_reach a deja ete etudie, on passe au suivant
# - Une fois que tous les brins amont sont etudies, on rassemble tous les brins amont 
# - On somme toutes les aires calculees pour les differents brins et on convertit la somme en km2
# - On stocke l'aire drainee et le brin correspondant dans un fichier csv
#
# Output :
# - un objet SurfaceDrainee avec col 1 = n? brin et col 2 = surface drainee en km2
# - le fichier Aire_drainee_brins.csv'
#####################################################################################
CalculAireDraineeBrin = function (reach,hrus,chemin_hrus, hrufilename){
nbLines = Lignes_saut(chemin_hrus,hrufilename)
headerHRU = readLines(paste(chemin_hrus,hrufilename,sep=''), n = nbLines)
LinesNames = which(substr(headerHRU,1,2)=="ID")
Names = read.table(paste(chemin_hrus,hrufilename,sep=''),nr=1,skip=LinesNames-1)
Total_surf <- NULL
indSurf = which(Names=="area")
indID = which(Names=="ID")
Aire_brins = matrix(c(0,0),1,2)
for (brin_choisi in reach[,which(Names == "ID")]){                                  
if(reach[brin_choisi== reach[,1],2] == 0) {Aire_brins = rbind (Aire_brins,c(brin_choisi,sum(hrus[hrus[,which(Names=="subbasin")] == brin_choisi,indSurf])))}
serie <- NULL
serie2 <- NULL
serie3 <- NULL  
Co = 1
X0 <- reach[which(reach[,2] == brin_choisi),1]
if (length(X0) != 0){
Prec = X0
while(length(Prec)!=0){
for (k in c(1:length(Prec))){
newbrins = reach[reach[,2] == Prec[k],1]
serie <- c(serie,newbrins)
Aire = 0
for (n in c(1:length(newbrins))){
if (length(newbrins) !=0 & length(which(newbrins[n]==Aire_brins[,1])) ==0){
Aire = sum(hrus[hrus[,which(Names=="subbasin")] == newbrins[n],indSurf])
Aire_brins = rbind(Aire_brins,c(newbrins[n],Aire))
}
}
}
assign(paste("X",Co,sep=""),serie)
serie <- NULL
Co = Co + 1
Prec = get(paste("X",Co-1,sep=""))
}
for (m in c(0:(Co-1))){
serie2 <- c(serie2,get(paste("X",m,sep="")))
assign(paste("X",m,sep=""),NULL)}
}
serie3 = unique(c(brin_choisi,serie2))   
serie3 = serie3[length(serie3):1]
Temp = NULL
for (n in c(1:length(serie3))){
Aire = Aire_brins[which(Aire_brins[,1]==serie3[n]),2]
Temp = c(Temp,Aire)
}
Total_surf <- rbind(Total_surf,c(brin_choisi, sum(Temp)/1000000))
print(brin_choisi);flush.console()                      
}
colnames(Total_surf) <- c('Brin','Surface drainee(en km2)')
write.table(Total_surf,paste(chemin_hrus,'Aire_drainee_brins.csv',sep=''),sep=';',dec='.',quote=F,row.names=F,col.names=T)
Total_surf
}


########################################################################################
########################################################################################
########################################################################################
###### Code pour ecrire le modele J2000-Rhone a partir des differentes entrees #########
########################################################################################
########################################################################################

##############################################################################################
##############################################################################################
###### Partie du code qui ajoute les boucles Init et FilterInit ##############################
##############################################################################################
# Parametres :
# - Nom_Boucle : code de la station etudiee
# - Nom_Modele : Nom du modele (le nom qui sera ecrit quand vous lancerez le modele avec JAMS (attention, ce n'est pas le nom du fichier de sortie!)
# - output_file : Fichier de sortie
# - Dossier_Temp : Dossier ou sera enregistre le modele final (ainsi que des boucles intermediaires)
# - Water2 : la liste des stations en amont de la station etudiee
# - Corres_ID_Stations : 2 colonnes : 1)ID , 2)code des stations
#
##############################################################################################
Boucle_Init = function (Nom_Boucle,Nom_Modele,output_file,Dossier_Temp,Water2,Corres_ID_Stations){
write.table(paste('        <contextcomponent class="jams.components.conditional.FilteredSpatialContext" enabled="true" name="',Nom_Boucle,'Init" version="1.1_0">
            <var name="attributeName" value="watershed"/>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)

write.table(t(Water2),paste(Dossier_Temp,'Boucle_temp.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
liste_bv <- read.table(paste(Dossier_Temp,'Boucle_temp.txt',sep=''),sep='\t')
file.remove(paste(Dossier_Temp,'Boucle_temp.txt',sep=''))

write.table(paste('            <var name="attributeValues" value="',liste_bv[1,1],'"/>
            <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
            <component class="org.unijena.j2k.aggregate.SumAggregator" enabled="true" name="WatershedAreaCalculator" version="1.0_0">
                <var attribute="area" context="',Nom_Boucle,'Init" name="value"/>
                <var attribute="',Nom_Boucle,'area" context="',Nom_Modele,'" name="sum"/>
            </component>
        </contextcomponent>

        <contextcomponent class="jams.components.conditional.FilteredSpatialContext" enabled="true" name="',Nom_Boucle,'FilterInit" version="1.1_0">
            <var name="attributeName" value="watershed"/>
            <var name="attributeValues" value="',liste_bv[1,1],'"/>
            <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
            <component class="org.unijena.j2k.CalcAreaWeight" enabled="true" name="CalcAreaWeight" version="1.0_0">
                <var attribute="area" context="',Nom_Boucle,'FilterInit" name="entityArea"/>
                <var attribute="',Nom_Boucle,'areaweight" context="',Nom_Boucle,'FilterInit" name="areaWeight"/>
                <var attribute="',Nom_Boucle,'area" context="',Nom_Modele,'" name="catchmentArea"/>
            </component>
        </contextcomponent>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
}




##############################################################################################
##############################################################################################
###### Recuperation des watersheds dependant d'une station donnee ############################
##############################################################################################
# Parametres :
# - Nom_Boucle : Code de la station etudiee
# - Correspondance : Correspondance entre code station et brin ou obtenir le debit
# - reach : le fichier parametre reach.par charge
# - Corres_ID_Stations : 2 colonnes : 1)ID , 2)code des stations
#
##############################################################################################
Water = function(Nom_Boucle,Correspondance,reach,Corres_ID_Stations){
Brins = Correspondance[which(Correspondance[,1]== Nom_Boucle),2]
Watersheds = NULL
for (brin in Brins) {
IDs <- NULL
Brin0 <- brin
for (indice in c(1:1000)){
assign(paste('Brin',indice,sep=''),NULL)}
k =0
while (length(get(paste('Brin',k,sep='')))!=0){
for (i in c(1:length(get(paste('Brin',k,sep=''))))){
assign(paste('Brin',k+1,sep=''),c(get(paste('Brin',k+1,sep='')),reach[which(reach[,2]== get(paste('Brin',k,sep=''))[i]),1]))
}
k = k+1
}
Total  <- NULL
for (l in c(1:k)){
Total <- c(Total,get(paste('Brin',l,sep='')))
}
Watershed <- Nom_Boucle
if(length(Total) !=0){
for (m in Total){
if(length(which(m == Correspondance[,2])) != 0){
Watershed = c(Watershed,as.character(Correspondance [which(m == Correspondance[,2]),1]))
}
}
Watershed = unique(Watershed)
Watershed = Watershed[order(Watershed)]
for (w in Watershed){
IDs = c(IDs,Corres_ID_Stations[which(Corres_ID_Stations[,2] == w),1])
}
Watersheds = c(Watersheds,IDs)
} else {
Watersheds = c(Watersheds,Corres_ID_Stations[which(Corres_ID_Stations[,2] == Nom_Boucle),1])
}
}
Watersheds = unique(Watersheds)
return(Watersheds)
}



##############################################################################################
##############################################################################################
###### Partie du code qui ajoute les boucles pour les moyennes sur les bassins aux stations ##
##############################################################################################
# Parametres :
# - Nom_Boucle : code de la station etudiee
# - Nom_Modele : Nom du modele (le nom qui sera ecrit quand vous lancerez le modele avec JAMS (attention, ce n'est pas le nom du fichier de sortie!)
# - output_file : Fichier de sortie
# - Dossier_Temp : Dossier ou sera enregistre le modele final (ainsi que des boucles intermediaires)
# - Water2 : la liste des stations en amont de la station etudiee
# - Corres_ID_Stations : 2 colonnes : 1)ID , 2)code des stations
# - SortiesL : Sorties converties en mm (division par la surface)
# - Sorties : Sorties directes du modele (en mm ou sans unite)
#
##############################################################################################
Boucle_Agreg = function(Nom_Boucle,Nom_Modele,output_file,Dossier_Temp,Water2,Corres_ID_Stations,SortiesL,Sorties){
write.table(paste('        <contextcomponent class="jams.components.conditional.FilteredSpatialContext" enabled="true" name="',Nom_Boucle,'Loop" version="1.1_0">
            <var name="attributeName" value="watershed"/> ',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
write.table(t(Water2),paste(Dossier_Temp,'Boucle_temp.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
liste_bv <- read.table(paste(Dossier_Temp,'Boucle_temp.txt',sep=''),sep='\t')
file.remove(paste(Dossier_Temp,'Boucle_temp.txt',sep=''))
write.table(t(Sorties),paste(Dossier_Temp,'Boucle_temp3.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
Outputs <- read.table(paste(Dossier_Temp,'Boucle_temp3.txt',sep=''),sep='\t')   
file.remove(paste(Dossier_Temp,'Boucle_temp3.txt',sep=''))
write.table(t(SortiesL),paste(Dossier_Temp,'Boucle_temp4.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
OutputsL <- read.table(paste(Dossier_Temp,'Boucle_temp4.txt',sep=''),sep='\t')  
file.remove(paste(Dossier_Temp,'Boucle_temp4.txt',sep=''))
Sorties2 <- NULL
for (k in Sorties){
Sorties2 <- c(Sorties2,paste(Nom_Boucle,k,sep=''))
}
write.table(t(Sorties2),paste(Dossier_Temp,'Boucle_temp5.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
Outputs2 <- read.table(paste(Dossier_Temp,'Boucle_temp5.txt',sep=''),sep='\t')  
file.remove(paste(Dossier_Temp,'Boucle_temp5.txt',sep=''))
SortiesL2 <- NULL
for (k in SortiesL){
SortiesL2 <- c(SortiesL2,paste(Nom_Boucle,k,sep=''))
}
write.table(t(SortiesL2),paste(Dossier_Temp,'Boucle_temp6.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
OutputsL2 <- read.table(paste(Dossier_Temp,'Boucle_temp6.txt',sep=''),sep='\t')  
file.remove(paste(Dossier_Temp,'Boucle_temp6.txt',sep=''))
write.table(paste('            <var name="attributeValues" value="',liste_bv[1,1],'"/>
            <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
            <component class="org.unijena.j2k.aggregate.WeightedSumAggregator" enabled="true" name="WeightedSumAggregator_2" version="1.0_0">
                <var attribute="',Nom_Boucle,'area" context="',Nom_Modele,'" name="weight"/>
                <var attribute="',OutputsL[1,1],'" context="',Nom_Boucle,'Loop" name="value"/>
                <var attribute="',OutputsL2[1,1],'" context="TimeLoop" name="sum"/>
            </component>
            <component class="org.unijena.j2k.aggregate.WeightedSumAggregator" enabled="true" name="WeightedSumAggregator_3" version="1.0_0">
                <var attribute="',Nom_Boucle,'areaweight" context="',Nom_Boucle,'Loop" name="weight"/>
                <var attribute="',Outputs[1,1],'" context="',Nom_Boucle,'Loop" name="value"/>
                <var attribute="',Outputs2[1,1],'" context="TimeLoop" name="sum"/>
            </component>
        </contextcomponent>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
}

# Boucle pour ecrire les composantes du debit dans le TimeLoop (XML)
##############################################################################################
##############################################################################################
###### Ajout des composantes du debit au TimeLoop ############################################
##############################################################################################
# Parametres :
# - Num_Brin : Brin correspondant a la station etudiee
# - output_file : Fichier de sortie
# - variable :  Nom des variables ecrites dans le TimeLoop 
#
##############################################################################################
Boucle_output <- function (Num_Brin,output_file,variable){
write.table(paste('                <attribute id="',variable,'_',Num_Brin,'"/>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)                
}


# Boucle pour ecrire les variables de bassins dans le TimeLoop (XML)
##############################################################################################
##############################################################################################
###### Ajout des variables de bassins au TimeLoop ############################################
##############################################################################################
# Parametres :
# - Nom_Boucle : Code de la station etudiee
# - output_file : Fichier de sortie
# - Dossier_Temp : Dossier ou sera enregistre le modele final (ainsi que des boucles intermediaires)
# - SortiesL : Sorties converties en mm (division par la surface)
# - Sorties : Sorties directes du modele (en mm ou sans unite) 
#
##############################################################################################
Boucle_sorties <- function(Nom_Boucle,output_file,Dossier_Temp,SortiesL,Sorties){
write.table(t(Sorties),paste(Dossier_Temp,'Boucle_temp3.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
Outputs <- read.table(paste(Dossier_Temp,'Boucle_temp3.txt',sep=''))   
file.remove(paste(Dossier_Temp,'Boucle_temp3.txt',sep=''))
write.table(t(SortiesL),paste(Dossier_Temp,'Boucle_temp4.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
OutputsL <- read.table(paste(Dossier_Temp,'Boucle_temp4.txt',sep=''))  
file.remove(paste(Dossier_Temp,'Boucle_temp4.txt',sep=''))
Sorties2 <- NULL
for (k in Sorties){
Sorties2 <- c(Sorties2,paste(Nom_Boucle,k,sep=''))
}
SortiesL2 <- NULL
for (k in SortiesL){
SortiesL2 <- c(SortiesL2,paste(Nom_Boucle,k,sep=''))
}
for (z in c(Sorties2,SortiesL2)){
write.table(paste('                <attribute id="',z,'"/>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)                
}
}


##############################################################################################
##############################################################################################
###### Boucle pour ecrire les composantes du debit dans TimeLoop depuis ReachLoop(.dat) ######
##############################################################################################
# Parametres :
# - variable : Nom des variables ecrites dans le TimeLoop 
# - Correspondance : Correspondance entre code station et brin ou obtenir le debit
# - Stations : liste des stations retenues
# - liste_br : liste des brins correspondant aux stations(suivant l'ordre des stations)
# - output_file : Fichier de sortie
# - indice_var : indice dans la boucle sur les variables 
#                                     
##############################################################################################
SwitchContext <- function(variable,Correspondance,Stations,liste_br,output_file,indice_var){
write.table(paste('            <contextcomponent class="jams.components.conditional.SwitchContext" enabled="true" name="',variable,'" version="1.0_1">
                <var name="values" value="',liste_br[1,1],'"/>
                <var attribute="ID" context="ReachLoop" name="attribute"/>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)      
for (i in Stations){
Nom_Boucle = i
k=0
while (k !=length(which(Correspondance[,1]==Nom_Boucle))){
Num_Brin <- as.character(Correspondance[which(Correspondance[,1]==Nom_Boucle),2][k+1])
write.table(paste('                <component class="org.unijena.j2k.aggregate.WeightedSumAggregator" enabled="true" name="',Nom_Boucle,'_',(10*(indice_var-1))+(k+1),'" version="1.0_0">
                    <var attribute="',variable,'" context="ReachLoop" name="value"/>
                    <var attribute="',variable,'_',Num_Brin,'" context="TimeLoop" name="sum"/>
                </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)      
k = k+1
}
}
write.table('            </contextcomponent>',output_file,col.names=F,row.names=F,quote=F,append=T)  
}



##############################################################################################
##############################################################################################
###### Mise en place du catchmentReseter (remettre les variables a 0 dans J2000) #############
##############################################################################################
# Parametres :
# - Model_Outputs : Autres variables pour lesquelles on calcule la moyenne sur tout le bassin (dont l'unite est differente du litre)
# - Model_OutputsL : Autres variables pour lesquelles on calcule la moyenne sur tout le bassin (dont l'unite est le litre)
# - Dossier_Temp : Dossier ou sera enregistre le modele final (ainsi que des boucles intermediaires)
# - Barrage : si 'oui', on ajoute les variables issues du module de barrages
#
###############################################################################################
catchResetfunction <- function (Model_Outputs,Model_OutputsL,Dossier_Temp,Barrage){
Sorties2 <- NULL
for (i in Stations){
Nom_Boucle = i
for (k in Sorties){
Sorties2 <- c(Sorties2,paste(Nom_Boucle,k,sep=''))
}
}
SortiesL2 <- NULL
for (i in Stations){
Nom_Boucle = i
for (k in SortiesL){
SortiesL2 <- c(SortiesL2,paste(Nom_Boucle,k,sep=''))
}
}        
Flow <- NULL
for (i in Stations){
Nom_Boucle = i
k=0
while (k !=length(which(Correspondance[,1]==Nom_Boucle))){
Num_Brin <- as.character(Correspondance[which(Correspondance[,1]==Nom_Boucle),2][k+1])
for (variable in variables){
Flow <- c(Flow,paste(variable,'_',Num_Brin,sep=''))
}
k=k+1
}
}
Barrages_var = NULL
if (Barrage == 'oui'){
for (k in as.character(liste_deriv[,Der_colNom])){
Barrages_var = c(Barrages_var, paste(k,'FO',sep=''))
Barrages_var = c(Barrages_var, paste(k,'_tvRD1',sep=''),paste(k,'_tvRD2',sep=''),paste(k,'_tvRG1',sep=''),paste(k,'_tvRG2',sep=''))
}
for (k in as.character(liste_barrage[,Bar_colNom])){
Barrages_var = c(Barrages_var, paste(k,'_actFO',sep=''))
Barrages_var = c(Barrages_var, paste(k,'_Storage',sep=''))
}
}
write.table(t(c(Model_Outputs,Model_OutputsL,Sorties2,SortiesL2,Flow,Barrages_var)),paste(Dossier_Temp,'Boucle_temp6.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
Catch_Reset <- read.table(paste(Dossier_Temp,'Boucle_temp6.txt',sep=''),sep='\t')  
file.remove(paste(Dossier_Temp,'Boucle_temp6.txt',sep=''))
Catch_Reset
}

##############################################################################################
##############################################################################################
###### Partie du code qui est responsable de l'application de l'irrigation ###################
##############################################################################################
# Parametres :
# - output_file : Fichier de sortie
#
##############################################################################################
IrrigationModule1 = function (output_file){
write.table('<component class="irrigation.IrrigationInit" enabled="true" name="IrrigationInit" version="1.0_0">
                <var attribute="irrigationStart" context="HRULoop" name="start"/>
                <var attribute="irrigationEnd" context="HRULoop" name="end"/>
                <var attribute="time" context="J2K_rhone" name="time"/>
                <var attribute="irrigationActive" context="HRULoop" name="irrigated"/>
            </component>
            <contextcomponent class="jams.components.conditional.SwitchContext" enabled="true" name="IrrigationContext_1" version="1.0_1">
                <var name="values" value="0;1"/>
                <var attribute="irrigationActive" context="HRULoop" name="attribute"/>
                <component class="irrigation.VariableInit" enabled="true" name="VariableInit_1" version="1.0_0">
                    <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                    <var attribute="irrigationTotal" context="HRULoop" name="irrigationTotal"/>
                </component>
                <contextcomponent class="jams.components.conditional.SwitchContext" enabled="true" name="IrrigationApplication" version="1.0_1">
                    <var name="values" value="1;2;3"/>
                    <var attribute="irrig_type" context="HRULoop" name="attribute"/>
                    <component class="irrigation.IrrigationApplicationAspersion" enabled="true" name="IrrigationApplicationAspersion" version="1.0_0">
                        <var attribute="area" context="HRULoop" name="area"/>
                        <var attribute="precip" context="HRULoop" name="precip"/>
                        <var attribute="irrigationWater" context="HRULoop" name="irrigationWater"/>
                        <var attribute="irrigationTotal" context="HRULoop" name="irrigationTotal"/>
                    </component>
                    <component class="irrigation.IrrigationApplicationDrip" enabled="true" name="IrrigationApplicationDrip" version="1.0_0">
                        <var attribute="actMPS" context="HRULoop" name="actMPS"/>
                        <var attribute="maxMPS" context="HRULoop" name="maxMPS"/>
                        <var attribute="irrigationWater" context="HRULoop" name="irrigationWater"/>
                        <var attribute="irrigationTotal" context="HRULoop" name="irrigationTotal"/>
                    </component>
                    <component class="irrigation.IrrigationApplicationSurface" enabled="true" name="IrrigationApplicationSurface" version="1.0_0">
                        <var attribute="netRain" context="HRULoop" name="netRain"/>
                        <var attribute="irrigationWater" context="HRULoop" name="irrigationWater"/>
                        <var attribute="irrigationTotal" context="HRULoop" name="irrigationTotal"/>
                    </component>
                </contextcomponent>
            </contextcomponent>',output_file,col.names=F,row.names=F,quote=F,append=T)
}           



##############################################################################################
##############################################################################################
###### Partie du code qui est responsable du calcul de la demande en irrigation ##############
##############################################################################################
# Parametres :
# - output_file : Fichier de sortie
#
# Options :
# Le code cree 3 modules (module initial developpe par Sven Kralisch, un module avec calcul de la dose par potET - actET  et un module avec possibilite de choisir la dose max ainsi que le taux de remplissage des reservoirs MPS et LPS
# 
##############################################################################################
IrrigationModule2 = function (output_file){
write.table('            <contextcomponent class="jams.components.conditional.SwitchContext" enabled="true" name="IrrigationContext_2" version="1.0_1">
                <var name="values" value="1"/>
                <var attribute="irrigationActive" context="HRULoop" name="attribute"/>
                <contextcomponent class="jams.components.conditional.SwitchContext" enabled="true" name="IrrigationDemandContext" version="1.0_1">
                    <var name="values" value="1;2;3"/>
                    <var attribute="irrig_type" context="HRULoop" name="attribute"/>
                    <component class="irrigation.IrrigationDemand" enabled="false" name="IrrigationDemand_Asp" version="1.0_0">
                        <var attribute="satMPS" context="HRULoop" name="satMPS"/>
                        <var attribute="maxMPS" context="HRULoop" name="maxMPS"/>
                        <var attribute="satLPS" context="HRULoop" name="satLPS"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var attribute="maxLPS" context="HRULoop" name="maxLPS"/>
                        <var name="irrigationDemandCorrectionMPS" value="1.0"/>
                        <var name="irrigationDemandCorrectionLPS" value="0.0"/>
                        <var name="etDeficit" value="0.5"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                    </component>
                    <component class="irrigation.IrrigationDemand_maxDose_MPS" enabled="true" name="IrrigationDemand_maxDose_MPS_Asp" version="1.0_0">
                        <var attribute="area" context="HRULoop" name="area"/>
                        <var attribute="satMPS" context="HRULoop" name="satMPS"/>
                        <var attribute="maxMPS" context="HRULoop" name="maxMPS"/>
                        <var name="efficiency" value="0.7"/>
                        <var attribute="satLPS" context="HRULoop" name="satLPS"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var attribute="maxLPS" context="HRULoop" name="maxLPS"/>
                        <var name="satMPSexp" value="0.25"/>
                        <var name="irrigationDemandCorrectionMPS" value="1.0"/>
                        <var name="irrigationDemandCorrectionLPS" value="0.0"/>
                        <var name="etDeficit" value="0.5"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var name="satLPSexp" value="0.9"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="waterRequirements" context="HRULoop" name="waterRequirements"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                    </component>
                    <component class="irrigation.IrrigationDemand_potET" enabled="false" name="IrrigationDemand_Asp_potET" version="1.0_0">
                        <var attribute="area" context="HRULoop" name="area"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var name="irrigationDemandCorrectionET" value="0.9"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                        <var name="maxDosis" value="0"/>
                        <var name="etDeficit" value="0.5"/>
                    </component>
                    <component class="irrigation.IrrigationDemand" enabled="false" name="IrrigationDemand_Drip" version="1.0_0">
                        <var attribute="satMPS" context="HRULoop" name="satMPS"/>
                        <var attribute="maxMPS" context="HRULoop" name="maxMPS"/>
                        <var attribute="satLPS" context="HRULoop" name="satLPS"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var attribute="maxLPS" context="HRULoop" name="maxLPS"/>
                        <var name="irrigationDemandCorrectionMPS" value="1.0"/>
                        <var name="irrigationDemandCorrectionLPS" value="0.0"/>
                        <var name="etDeficit" value="0.5"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                    </component>
                    <component class="irrigation.IrrigationDemand_maxDose_MPS" enabled="true" name="IrrigationDemand_maxDose_MPS_Drip" version="1.0_0">
                        <var attribute="area" context="HRULoop" name="area"/>
                        <var attribute="satMPS" context="HRULoop" name="satMPS"/>
                        <var attribute="maxMPS" context="HRULoop" name="maxMPS"/>
                        <var name="efficiency" value="0.9"/>
                        <var attribute="satLPS" context="HRULoop" name="satLPS"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var attribute="maxLPS" context="HRULoop" name="maxLPS"/>
                        <var name="satMPSexp" value="0.25"/>
                        <var name="irrigationDemandCorrectionMPS" value="1.0"/>
                        <var name="irrigationDemandCorrectionLPS" value="0.0"/>
                        <var name="etDeficit" value="0.5"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var name="satLPSexp" value="0.9"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="waterRequirements" context="HRULoop" name="waterRequirements"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                    </component>
                    <component class="irrigation.IrrigationDemand_potET" enabled="false" name="IrrigationDemand_Drip_potET" version="1.0_0">
                        <var attribute="area" context="HRULoop" name="area"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var name="irrigationDemandCorrectionET" value="0.9"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                        <var name="maxDosis" value="0"/>
                        <var name="etDeficit" value="0.5"/>
                    </component>
                    <component class="irrigation.IrrigationDemand" enabled="false" name="IrrigationDemand_Surf" version="1.0_0">
                        <var attribute="satMPS" context="HRULoop" name="satMPS"/>
                        <var attribute="maxMPS" context="HRULoop" name="maxMPS"/>
                        <var attribute="satLPS" context="HRULoop" name="satLPS"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var attribute="maxLPS" context="HRULoop" name="maxLPS"/>
                        <var name="irrigationDemandCorrectionMPS" value="1.0"/>
                        <var name="irrigationDemandCorrectionLPS" value="1.0"/>
                        <var name="etDeficit" value="0.5"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                    </component>
                    <component class="irrigation.IrrigationDemand_maxDose_MPS" enabled="true" name="IrrigationDemand_maxDose_MPS_Surf" version="1.0_0">
                        <var attribute="area" context="HRULoop" name="area"/>
                        <var attribute="satMPS" context="HRULoop" name="satMPS"/>
                        <var attribute="maxMPS" context="HRULoop" name="maxMPS"/>
                        <var name="efficiency" value="0.5"/>
                        <var attribute="satLPS" context="HRULoop" name="satLPS"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var attribute="maxLPS" context="HRULoop" name="maxLPS"/>
                        <var name="satMPSexp" value="0.25"/>
                        <var name="irrigationDemandCorrectionMPS" value="1.0"/>
                        <var name="irrigationDemandCorrectionLPS" value="0.0"/>
                        <var name="etDeficit" value="0.5"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var name="satLPSexp" value="0.9"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="waterRequirements" context="HRULoop" name="waterRequirements"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                    </component>
                    <component class="irrigation.IrrigationDemand_potET" enabled="false" name="IrrigationDemand_Surf_potET" version="1.0_0">
                        <var attribute="area" context="HRULoop" name="area"/>
                        <var attribute="subbasin" context="HRULoop" name="subBasin"/>
                        <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                        <var attribute="actET" context="HRULoop" name="actET"/>
                        <var name="irrigationDemandCorrectionET" value="0.9"/>
                        <var attribute="hrus" context="J2K_rhone" name="hrus"/>
                        <var attribute="irrigationDemand" context="HRULoop" name="irrigationDemand"/>
                        <var attribute="potET" context="HRULoop" name="potET"/>
                        <var name="maxDosis" value="0"/>
                        <var name="etDeficit" value="0.5"/>
                    </component>
                </contextcomponent>
            </contextcomponent>',output_file,col.names=F,row.names=F,quote=F,append=T)
}

##############################################################################################
##############################################################################################
###### Partie du code qui est responsable du prelevement de l'irrigation dans la riviere #####
##############################################################################################
# Parametres :
# - output_file : Fichier de sortie
# 
##############################################################################################
IrrigationModule3 = function (output_file){
write.table('            <component class="irrigation.IrrigationWaterTransfer_act" enabled="true" name="IrrigationWaterTransfer_act" version="1.0_0">
                <var attribute="inRD1" context="ReachLoop" name="inRD1"/>
                <var attribute="inRD2" context="ReachLoop" name="inRD2"/>
                <var attribute="actRD2" context="ReachLoop" name="actRD2"/>
                <var attribute="inRG2" context="ReachLoop" name="inRG2"/>
                <var attribute="actRD1" context="ReachLoop" name="actRD1"/>
                <var attribute="totalTransfer" context="ReachLoop" name="totalTransfer"/>
                <var attribute="reaches" context="J2K_rhone" name="reaches"/>
                <var attribute="actRG2" context="ReachLoop" name="actRG2"/>
                <var attribute="actRG1" context="ReachLoop" name="actRG1"/>
                <var attribute="inRG1" context="ReachLoop" name="inRG1"/>
                <var name="actPrel" value="0.5"/>
                <var attribute="totalDemand" context="ReachLoop" name="totalDemand"/>
                <var attribute="totalInput" context="ReachLoop" name="totalInput"/>
            </component>',output_file,col.names=F,row.names=F,quote=F,append=T)
}           


##############################################################################################
##############################################################################################
###### Fonction permettant de passer la FO de ReachLoop vers TimeLoop ########################
##############################################################################################
# Parametres :
# - output_file : Fichier de sortie
# - liste_deriv : liste des derivations (avec minimum brin prelevement et brin de sortie)
# - Der_colNom : Indice de la colonne ayant le nom des derivations
# - Der_colBrinEntree : Indice de la colonne ayant le brin d'entree des derivations
# 
##############################################################################################
Derivation1 = function (output_file,liste_deriv,Der_colNom,Der_colBrinEntree){
write.table('            <contextcomponent class="jams.components.conditional.SwitchContext" enabled="true" name="FO_Trans" version="1.0_1">',output_file,col.names=F,row.names=F,quote=F,append=T)
write.table(t(liste_deriv[,Der_colBrinEntree]),paste(Dossier_Temp,'Boucle_temp.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
liste_dv <- read.table(paste(Dossier_Temp,'Boucle_temp.txt',sep=''))
file.remove(paste(Dossier_Temp,'Boucle_temp.txt',sep=''))
write.table(t(liste_deriv[,Der_colBrinEntree]),paste(Dossier_Temp,'Boucle_temp.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
write.table(paste('            <var name="values" value="',as.character(liste_dv[1,1]),'"/>
                <var attribute="ID" context="ReachLoop" name="attribute"/>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
for (k in as.character(liste_deriv[,Der_colNom])){
write.table(paste('            <component class="org.unijena.j2k.aggregate.WeightedSumAggregator" enabled="true" name="',k,'" version="1.0_0">
                    <var attribute="',k,'FO" context="TimeLoop" name="sum"/>
                    <var attribute="FO" context="ReachLoop" name="value"/>
                </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
                }
write.table('            </contextcomponent>',output_file,col.names=F,row.names=F,quote=F,append=T)
}           

##############################################################################################
##############################################################################################
###### Fonction permettant de transferer le volume d'un point a un autre #####################
##############################################################################################
# Parametres :
# - output_file : Fichier de sortie
# - liste_deriv : liste des derivations (avec minimum brin prelevement et brin de sortie)
# - Der_colNom : Indice de la colonne ayant le nom des derivations
# - Der_colBrinEntree : Indice de la colonne ayant le brin d'entree des derivations
# - Der_colBrinSortie : Indice de la colonne ayant le brin de sortie des derivations
#
##############################################################################################
Derivation2 = function (output_file,liste_deriv,Der_colNom,Der_colBrinEntree,Der_colBrinSortie){
for (k in c(1:length(as.character(liste_deriv[,Der_colNom])))){

write.table(paste('        <component class="org.unijena.j2k.routing.Reach2ReachTransfer" enabled="true" name="',as.character(liste_deriv[k,Der_colNom]),'" version="1.0_0">
            <var name="targetNames" value="inRD1;inRD2;inRG1;inRG2"/>
            <var name="sourceNames" value="actRD1;actRD2;actRG1;actRG2"/>
            <var attribute="reaches" context="J2K_rhone" name="reaches"/>
            <var name="targetReachID" value="',liste_deriv[k,Der_colBrinSortie],'"/>
            <var attribute="',as.character(liste_deriv[k,Der_colNom]),'FO" context="TimeLoop" name="upperBound"/>
            <var attribute="',as.character(liste_deriv[k,Der_colNom]),'_tvRD1;',as.character(liste_deriv[k,Der_colNom]),'_tvRD2;',as.character(liste_deriv[k,Der_colNom]),'_tvRG1;',as.character(liste_deriv[k,Der_colNom]),'_tvRG2" context="TimeLoop" name="volumes"/>
            <var name="lowerBound" value="0"/>
            <var name="sourceReachID" value="',liste_deriv[k,Der_colBrinEntree],'"/>
            <var name="fraction" value="1"/>
        </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
        }
        
}

##############################################################################################
##############################################################################################
###### Partie du code qui ajoute les FO reelles au TimeLoop ##################################
##############################################################################################
# Parametres :
# - output_file :  Fichier de sortie
# - liste_barrage : liste des ouvrages (avec minimum Smax(enMm3) la capacite max de l'ouvrage,V0(enMm3) le volume en janvier et un brin sortie)
# - Bar_colNom : Indice de la colonne ayant le nom des barrages
# - Bar_colBrinSortie : Indice de la colonne ayant le brin de sortie des barrages
#
##############################################################################################
Barrage1 = function(output_file,liste_barrage,Bar_colNom,Bar_colBrinSortie){ 

write.table(t(liste_barrage[,Bar_colBrinSortie]),paste(Dossier_Temp,'Boucle_temp.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
liste_bar <- read.table(paste(Dossier_Temp,'Boucle_temp.txt',sep=''))
file.remove(paste(Dossier_Temp,'Boucle_temp.txt',sep=''))
                                         
write.table(paste('            <contextcomponent class="jams.components.conditional.SwitchContext" enabled="false" name="FO_final" version="1.0_1">
                <var name="values" value="',as.character(liste_bar[1,1]),'"/>
                <var attribute="ID" context="ReachLoop" name="attribute"/>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
                
for (k in as.character(liste_barrage[,Bar_colNom])){

write.table(paste('            <component class="org.unijena.j2k.aggregate.WeightedSumAggregator" enabled="true" name="',k,'" version="1.0_0">
                    <var attribute="actFO" context="ReachLoop" name="value"/>
                    <var attribute="',k,'_actFO" context="TimeLoop" name="sum"/>
                </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
                }
                
write.table('            </contextcomponent>',output_file,col.names=F,row.names=F,quote=F,append=T)

}

##############################################################################################
##############################################################################################
###### Partie du code qui ajoute les stocks dans les barrages au TimeLoop ####################
##############################################################################################
# Parametres :
# - output_file :  Fichier de sortie
# - liste_barrage : liste des ouvrages (avec minimum Smax(enMm3) la capacite max de l'ouvrage,V0(enMm3) le volume en janvier et un brin sortie)
# - Bar_colNom : Indice de la colonne ayant le nom des barrages
# - Bar_colBrinSortie : Indice de la colonne ayant le brin de sortie des barrages
#
##############################################################################################
Storage = function (output_file,liste_barrage,Bar_colNom,Bar_colBrinSortie){ 
write.table(t(liste_barrage[,Bar_colBrinSortie]),paste(Dossier_Temp,'Boucle_temp.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
liste_bar <- read.table(paste(Dossier_Temp,'Boucle_temp.txt',sep=''))
file.remove(paste(Dossier_Temp,'Boucle_temp.txt',sep=''))
                                         
write.table(paste('            <contextcomponent class="jams.components.conditional.SwitchContext" enabled="false" name="Storage_final" version="1.0_1">
                <var name="values" value="',as.character(liste_bar[1,1]),'"/>
                <var attribute="ID" context="ReachLoop" name="attribute"/>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)

                
for (k in as.character(liste_barrage[,Bar_colNom])){
write.table(paste('            <component class="org.unijena.j2k.aggregate.WeightedSumAggregator" enabled="true" name="',k,'" version="1.0_0">
                    <var attribute="Storage" context="ReachLoop" name="value"/>
                    <var attribute="',k,'_Storage" context="TimeLoop" name="sum"/>
                </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
                }
write.table('            </contextcomponent>',output_file,col.names=F,row.names=F,quote=F,append=T)
}



##############################################################################################
##############################################################################################
###### Code qui ecrit le fichier final du modele J2000-Rhone #################################
##############################################################################################
# Parametres :
# - Auteur : Auteur du modele 
# - Stations : liste des stations retenues
# - Nom_Modele : Nom du modele (le nom qui sera ecrit quand vous lancerez le modele avec JAMS (attention, ce n'est pas le nom du fichier de sortie!)
# - Sorties : Sorties directes du modele (en mm ou sans unite) 
# - Dossier_Temp : Dossier ou sera enregistre le modele final (ainsi que des boucles intermediaires) 
# - SortiesL : Sorties converties en mm (division par la surface) 
# - output_file : Fichier de sortie
# - variables : Nom des composantes du debit ecrites dans le TimeLoop 
# - Model_OutputsL : Autres variables pour lesquelles on calcule la moyenne sur tout le bassin (dont l'unite est le litre)
# - Model_Outputs : Autres variables pour lesquelles on calcule la moyenne sur tout le bassin (dont l'unite est differente du litre)
# - Barrage : si 'oui', on ajoute les modules relatifs aux barrages
# - Irrigation : si 'oui', on ajoute les modules relatifs a l'irrigation
#                                
###############################################################################################
EcrituremodeleFunction <- function (Auteur,Stations,Nom_Modele,Sorties,Dossier_Temp,SortiesL,output_file,variables, Model_OutputsL,Model_Outputs,Barrage,Irrigation){
output_file = paste(Dossier_Temp,output_file,sep='')
#Liste des objets a mettre dans le catchmentReseter
Catch_Reset <- catchResetfunction (Model_Outputs,Model_OutputsL,Dossier_Temp,Barrage)
#Ecriture du modele
write.table(paste('<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<model author="',Auteur,'" date="2013-04-03 00:00" helpbaseurl="http://jams.uni-jena.de/jamswiki" name="',Nom_Modele,'" version="3.0">
    <description><![CDATA[The standard J2K hydrological model]]></description>
    <var name="workspaceDirectory" value=""/>
    <launcher>
        <group name="Main"/>
        <group name="Plots &amp; Maps">
            <subgroup name="Plots"/>
            <subgroup name="Maps"/>
        </group>
        <group name="Initialising">
            <property attribute="FCAdaptation" component="InitSoilWater" description="Multiplier for field capacity" name="Multiplier for field capacity" range="0.0;5.0" type="Double"/>
            <property attribute="ACAdaptation" component="InitSoilWater" description="Multiplier for air capacity" name="Multiplier for air capacity" range="0.0;5.0" type="Double"/>
            <property attribute="initRG1" component="InitGroundWater" description="Initial storage of RG1 relative to maximum storage" name="InitGroundWater.initRG1" range="0.0;1.0" type="Double"/>
            <property attribute="initRG2" component="InitGroundWater" description="Initial storage of RG2 relative to maximum storage" name="InitGroundWater.initRG2" range="0.0;1.0" type="Double"/>
        </group>
        <group name="Interception"/>
        <group name="Snow"/>
        <group name="SoilWater"/>
        <group name="Groundwater"/>
        <group name="ReachRouting">
            <property attribute="flowRouteTA" component="J2KProcessReachRouting" description="Flood routing coefficient" name="flowRouteTA" range="0.0;100.0" type="Double"/>
        </group>
    </launcher>
    <datastores>
        <outputdatastore context="TimeLoop" enabled="true" name="TimeLoop">
            <trace>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=F)
# Application de la boucle pour sortir les composantes du debit dans TimeLoop ###
for (i in Stations){
Nom_Boucle = i
Boucle_sorties(Nom_Boucle,output_file,Dossier_Temp,SortiesL,Sorties)
k=0
while (k !=length(which(Correspondance[,1]==Nom_Boucle))){
Num_Brin <- as.character(Correspondance[which(Correspondance[,1]==Nom_Boucle),2][k+1])
for (variable in variables){
Boucle_output(Num_Brin,output_file,variable)
}
k = k+1
}
}
# Reprise du code                                                                       
# Ecriture du HRULoop et du ReachLoop
write.table(paste('            </trace>
        </outputdatastore>
        <outputdatastore context="ReachLoop" enabled="false" name="ReachLoop">
            <trace>
			    <attribute id="reachOutRD1"/>
                <attribute id="reachOutRD2"/>
                <attribute id="reachOutRG1"/>
                <attribute id="simRunoff"/>
			</trace>
        </outputdatastore>
        <outputdatastore context="HRULoop" enabled="false" name="HRULoop">
            <trace/>
        </outputdatastore>
    </datastores>
    <preprocessors> 
        <metaprocessor class="jams.components.concurrency.ConcurrentContextProcessor" context="HRULoop" enabled="true">
            <property name="partitioner_class" value="jams.components.concurrency.EntityPartitioner"/>
            <property name="exclude_component" value="J2KProcessRouting,HRU2ReachRouting;SpatialWeightedSumAggregator1;SpatialWeightedSumAggregator2"/>
            <property name="scale_factor" value="1"/>
        </metaprocessor>
    </preprocessors>
# Parametres globaux du modele (snow_trs, snow_trans, date notamment)
    <attributelists/>
    <attribute class="jams.data.Attribute$Double" name="snow_trs" value="2"/>
    <attribute class="jams.data.Attribute$TimeInterval" name="timeInterval" value="1985-01-01 07:30 2012-12-31 07:30 6 1"/>
    <attribute class="jams.data.Attribute$Double" name="snow_trans" value="3"/>
    <attribute class="jams.data.Attribute$Integer" name="data_caching" value="2"/>
#Lecture des fichiers parametres
    <contextcomponent class="jams.model.JAMSContext" enabled="true" name="ParameterInput" version="1.0_0">
        <component class="org.unijena.j2k.io.StandardEntityReader" enabled="true" name="EntityReader" version="1.2">
            <var name="reachFileName" value="parameter/reach.par"/>
            <var name="hruFileName" value="parameter/hrus.par"/>
            <var attribute="hrus" context="',Nom_Modele,'" name="hrus"/>
            <var attribute="reaches" context="',Nom_Modele,'" name="reaches"/>
        </component>
        <component class="org.unijena.j2k.io.StandardLUReader" enabled="true" name="LUReader" version="1.1_0">
            <var name="luFileName" value="parameter/landuse.par"/>
            <var attribute="hrus" context="',Nom_Modele,'" name="hrus"/>
        </component>
        <component class="org.unijena.j2k.io.StandardSoilParaReader" enabled="true" name="STReader" version="1.0_0">
            <var name="stFileName" value="parameter/soils.par"/>
            <var attribute="hrus" context="',Nom_Modele,'" name="hrus"/>
        </component>
        <component class="org.unijena.j2k.io.StandardGroundwaterParaReader" enabled="true" name="GWReader" version="1.1_0">
            <var name="gwFileName" value="parameter/hgeo.par"/>
            <var attribute="hrus" context="',Nom_Modele,'" name="hrus"/>
        </component>
    </contextcomponent>
# Initialisation (taille des reservoirs, aire du bassin, coeff cult, LAI...)
    <contextcomponent class="jams.model.JAMSContext" enabled="true" name="Initialization" version="1.0_0">
        <contextcomponent class="jams.model.JAMSSpatialContext" enabled="true" name="CatchmentInit" version="1.0_0">
            <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
            <component class="org.unijena.j2k.aggregate.SumAggregator" enabled="true" name="AreaCalculator" version="1.0_0">
                <var attribute="area" context="CatchmentInit" name="value"/>
                <var attribute="area" context="',Nom_Modele,'" name="sum"/>
            </component>
        </contextcomponent>
        <contextcomponent class="jams.model.JAMSSpatialContext" enabled="true" name="HRUInit" version="1.0_0">
            <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
            <component class="org.unijena.j2k.CalcAreaWeight" enabled="true" name="AreaWeight" version="1.0_0">
                <var attribute="area" context="HRUInit" name="entityArea"/>
                <var attribute="areaweight" context="HRUInit" name="areaWeight"/>
                <var attribute="area" context="',Nom_Modele,'" name="catchmentArea"/>
            </component>
            <component class="tools.CalcLanduseStateVars_cropcoeff_LAI" enabled="true" name="CalcLanduseStateVars_cropcoeff_LAI" version="1.0_1">
                <var attribute="cropcoeffArray" context="HRUInit" name="cropcoeffArray"/>
                <var attribute="LAIArray" context="HRUInit" name="LAIArray"/>
                <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
            </component>
            <component class="org.unijena.j2k.soilWater.InitJ2KProcessLumpedSoilWaterStates" enabled="true" name="InitSoilWater" version="1.0_0">
                <var name="ACAdaptation" value="1.0"/>
                <var attribute="actLPS" context="HRUInit" name="actLPS"/>
                <var name="FCAdaptation" value="1.0"/>
                <var attribute="maxMPS" context="HRUInit" name="maxMPS"/>
                <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
                <var attribute="rootDepth" context="HRUInit" name="rootDepth"/>
                <var attribute="maxLPS" context="HRUInit" name="maxLPS"/>
                <var attribute="area" context="HRUInit" name="area"/>
                <var attribute="satSoil" context="HRUInit" name="satSoil"/>
                <var attribute="actMPS" context="HRUInit" name="actMPS"/>
                <var attribute="satMPS" context="HRUInit" name="satMPS"/>
                <var attribute="satLPS" context="HRUInit" name="satLPS"/>
            </component>
            <component class="org.unijena.j2k.groundwater.InitJ2KProcessGroundwater" enabled="true" name="InitGroundWater" version="1.0_0">
                <var attribute="actRG2" context="HRUInit" name="actRG2"/>
                <var attribute="actRG1" context="HRUInit" name="actRG1"/>
                <var attribute="maxRG2" context="HRUInit" name="maxRG2"/>
                <var attribute="maxRG1" context="HRUInit" name="maxRG1"/>
                <var name="initRG1" value="0.0"/>
                <var name="initRG2" value="0.0"/>
                <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
            </component>                
        </contextcomponent>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
        
####  Boucle d'initialisation des stations #######
for (i in Stations){
Nom_Boucle = i
Water2 = Water(Nom_Boucle,Correspondance,reach,Corres_ID_Stations)
Boucle_Init(Nom_Boucle,Nom_Modele,output_file,Dossier_Temp,Water2,Corres_ID_Stations)
}

#Boucle temporaire pour la creation des objets pour les moyennes de bassin
write.table(t(Model_OutputsL),paste(Dossier_Temp,'Boucle_temp6.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
Model_Outputs2L <- read.table(paste(Dossier_Temp,'Boucle_temp6.txt',sep=''))  
file.remove(paste(Dossier_Temp,'Boucle_temp6.txt',sep=''))
write.table(t(Model_Outputs),paste(Dossier_Temp,'Boucle_temp6.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
Model_Outputs2 <- read.table(paste(Dossier_Temp,'Boucle_temp6.txt',sep=''))  
file.remove(paste(Dossier_Temp,'Boucle_temp6.txt',sep=''))

#Reprise de l'ecriture
# Ecriture du catchmentResetter
write.table(paste('    </contextcomponent>
    <contextcomponent class="jams.model.JAMSTemporalContext" enabled="true" name="TimeLoop" version="1.0_1">
        <var attribute="timeInterval" context="',Nom_Modele,'" name="timeInterval"/>
        <var attribute="time" context="',Nom_Modele,'" name="current"/>
        <component class="jams.components.gui.JAMSExecInfo" enabled="true" name="ExecutionInfo" version="1.1_0"/>
        <component class="org.unijena.j2k.DoubleSetter" enabled="true" name="CatchmentResetter" version="1.0_0">
            <var name="value" value="0"/>
            <var attribute="',Catch_Reset[1,1],'" context="TimeLoop" name="attributes"/>
        </component>
        <contextcomponent class="jams.model.JAMSContext" enabled="true" name="TSInput" version="1.0_0">',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
            
# Si on a active l'option barrages, on ajoute la lecture du fichier FO.dat
if (Barrage == 'oui'){
write.table(paste('    <component class="management.TSDataStoreReader_ID" enabled="true" name="FODataReader" version="1.0_0">
                <var name="id" value="FO"/>
                <var attribute="dataArrayFO" context="J2K_rhone" name="dataArray"/>
                <var attribute="timeInterval" context="J2K_rhone" name="timeInterval"/>
                <var attribute="namesFO" context="J2K_rhone" name="names"/>
                <var attribute="dataSetNameFO" context="J2K_rhone" name="dataSetName"/>
            </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
            }
            
#Lecture des fichiers climatiques
write.table(paste('    <component class="jams.components.io.TSDataStoreReader" enabled="true" name="TmeanDataReader" version="1.2">
                <var name="id" value="tmean"/>
                <var attribute="timeInterval" context="',Nom_Modele,'" name="timeInterval"/>
                <var attribute="xCoordTmean" context="',Nom_Modele,'" name="xCoord"/>
                <var attribute="dataArrayTmean" context="',Nom_Modele,'" name="dataArray"/>
                <var attribute="regCoeffTmean" context="',Nom_Modele,'" name="regCoeff"/>
                <var attribute="elevationTmean" context="',Nom_Modele,'" name="elevation"/>
                <var attribute="dataSetNameTmean" context="',Nom_Modele,'" name="dataSetName"/>
                <var attribute="yCoordTmean" context="',Nom_Modele,'" name="yCoord"/>
            </component>
            <component class="jams.components.io.TSDataStoreReader" enabled="true" name="PrecipDataReader" version="1.2">
                <var name="id" value="rain_hybride"/>
                <var attribute="timeInterval" context="',Nom_Modele,'" name="timeInterval"/>
                <var attribute="xCoordPrecip" context="',Nom_Modele,'" name="xCoord"/>
                <var attribute="dataArrayPrecip" context="',Nom_Modele,'" name="dataArray"/>
                <var attribute="regCoeffPrecip" context="',Nom_Modele,'" name="regCoeff"/>
                <var attribute="elevationPrecip" context="',Nom_Modele,'" name="elevation"/>
                <var attribute="dataSetNamePrecip" context="',Nom_Modele,'" name="dataSetName"/>
                <var attribute="yCoordPrecip" context="',Nom_Modele,'" name="yCoord"/>
            </component>
            <component class="jams.components.io.TSDataStoreReader" enabled="true" name="RefETReader" version="1.2">
                <var name="id" value="refet"/>
                <var attribute="timeInterval" context="',Nom_Modele,'" name="timeInterval"/>
                <var attribute="xCoordRefET" context="',Nom_Modele,'" name="xCoord"/>
                <var attribute="dataArrayRefET" context="',Nom_Modele,'" name="dataArray"/>
                <var attribute="regCoeffRefET" context="',Nom_Modele,'" name="regCoeff"/>
                <var attribute="elevationRefET" context="',Nom_Modele,'" name="elevation"/>
                <var attribute="dataSetNameRefET" context="',Nom_Modele,'" name="dataSetName"/>
                <var attribute="yCoordRefET" context="',Nom_Modele,'" name="yCoord"/>
            </component>
        </contextcomponent>

#Regionalisation des LAI, coeff cultural et fichiers climatiques
        <contextcomponent class="jams.model.JAMSSpatialContext" enabled="true" name="HRULoop" version="1.0_0">
            <var attribute="hrus" context="',Nom_Modele,'" name="entities"/>
            <contextcomponent class="jams.model.JAMSContext" enabled="true" name="Regionalization" version="1.0_0">
#LAI
                <component class="tools.J2KArrayGrabber_cropcoeff_LAI" enabled="true" name="J2KArrayGrabber_cropcoeff_LAI" version="1.0_0">
                    <var attribute="actcropcoeff" context="HRULoop" name="actCropCoeff"/>
                    <var attribute="time" context="',Nom_Modele,'" name="time"/>
                    <var attribute="actSlAsCf" context="HRULoop" name="actSlAsCf"/>
                    <var attribute="cropcoeffArray" context="HRULoop" name="cropcoeffArray"/>
                    <var attribute="actLAI" context="HRULoop" name="actLAI"/>
                    <var attribute="LAIArray" context="HRULoop" name="LAIArray"/>
                    <var name="tempRes" value="d"/>
                </component>
#Tmean
                <component class="org.unijena.j2000g.lowmem.Regionalisation" enabled="true" name="TmeanLmRegionaliser" version="1.0_0">
                    <var attribute="dataArrayTmean" context="J2K_rhone" name="dataArray"/>
                    <var attribute="regCoeffTmean" context="J2K_rhone" name="regCoeff"/>
                    <var attribute="tmean" context="HRULoop" name="dataValue"/>
                    <var attribute="y" context="HRULoop" name="entityY"/>
                    <var attribute="x" context="HRULoop" name="entityX"/>
                    <var name="rsqThreshold" value="0"/>
                    <var attribute="xCoordTmean" context="J2K_rhone" name="statX"/>
                    <var attribute="tmeanWeights" context="HRULoop" name="statWeights"/>
                    <var attribute="yCoordTmean" context="J2K_rhone" name="statY"/>
                    <var attribute="elevationTmean" context="J2K_rhone" name="statElevation"/>
                    <var name="pidw" value="2"/>
                    <var attribute="tmeanOrder" context="HRULoop" name="statOrder"/>
                    <var attribute="elevation" context="HRULoop" name="entityElevation"/>
                    <var name="nidw" value="4"/>
                    <var name="elevationCorrection" value="false"/>
                </component>
#Precip
                <component class="org.unijena.j2000g.lowmem.Regionalisation" enabled="true" name="PrecipLmRegionaliser" version="1.0_0">
                    <var attribute="dataArrayPrecip" context="J2K_rhone" name="dataArray"/>
                    <var attribute="regCoeffPrecip" context="J2K_rhone" name="regCoeff"/>
                    <var attribute="precip" context="HRULoop" name="dataValue"/>
                    <var attribute="y" context="HRULoop" name="entityY"/>
                    <var attribute="x" context="HRULoop" name="entityX"/>
                    <var name="rsqThreshold" value="0"/>
                    <var attribute="xCoordPrecip" context="J2K_rhone" name="statX"/>
                    <var attribute="precipWeights" context="HRULoop" name="statWeights"/>
                    <var attribute="yCoordPrecip" context="J2K_rhone" name="statY"/>
                    <var attribute="elevationPrecip" context="J2K_rhone" name="statElevation"/>
                    <var name="pidw" value="2"/>
                    <var attribute="precipOrder" context="HRULoop" name="statOrder"/>
                    <var name="fixedMinimum" value="0"/>
                    <var attribute="elevation" context="HRULoop" name="entityElevation"/>
                    <var name="nidw" value="4"/>
                    <var name="elevationCorrection" value="false"/>
                </component>
#Ref ET
                <component class="org.unijena.j2000g.lowmem.Regionalisation" enabled="true" name="RefETLmRegionaliser" version="1.0_0">
                    <var attribute="dataArrayRefET" context="J2K_rhone" name="dataArray"/>
                    <var attribute="regCoeffRefET" context="J2K_rhone" name="regCoeff"/>
                    <var attribute="refET" context="HRULoop" name="dataValue"/>
                    <var attribute="y" context="HRULoop" name="entityY"/>
                    <var attribute="x" context="HRULoop" name="entityX"/>
                    <var name="rsqThreshold" value="0"/>
                    <var attribute="xCoordRefET" context="J2K_rhone" name="statX"/>
                    <var attribute="refetWeights" context="HRULoop" name="statWeights"/>
                    <var attribute="yCoordRefET" context="J2K_rhone" name="statY"/>
                    <var attribute="elevationRefET" context="J2K_rhone" name="statElevation"/>
                    <var name="pidw" value="2"/>
                    <var attribute="refetOrder" context="HRULoop" name="statOrder"/>
                    <var name="fixedMinimum" value="0"/>
                    <var attribute="elevation" context="HRULoop" name="entityElevation"/>
                    <var name="nidw" value="4"/>
                    <var name="elevationCorrection" value="false"/>
                </component>
            </contextcomponent>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)

# Ajout des premiers modules d'irrigation(application de l'irrigation)
if (Irrigation == 'oui'){
IrrigationModule1(output_file)
}       
            
#Reprise de l'ecriture du modele
#Recuperation du crop coeff
write.table(paste('    <component class="crop.CropCoefficient" enabled="true" name="CropCoefficient_1" version="0.1_0">
                <var attribute="potET" context="HRULoop" name="PotET"/>
                <var attribute="refET" context="HRULoop" name="RefET"/>
                <var attribute="actcropcoeff" context="HRULoop" name="CropCoeff"/>
            </component>
# Calcul de la partition pluie neige
            <component class="snow.CalcRainSnowParts_IRSTEA" enabled="true" name="CalcRainSnowParts_IRSTEA" version="1.0_0">
                <var attribute="area" context="HRULoop" name="area"/>
                <var attribute="snow_trs" context="J2K_rhone" name="snow_trs"/>
                <var attribute="rain" context="HRULoop" name="rain"/>
                <var attribute="precip" context="HRULoop" name="precip"/>
                <var attribute="snow" context="HRULoop" name="snow"/>
                <var attribute="tmean" context="HRULoop" name="tmean"/>
                <var attribute="snow_trans" context="J2K_rhone" name="snow_trans"/>
            </component>
            # Interception
            <component class="interception.J2KProcessInterception_conv_potET" enabled="true" name="J2KProcessInterception3_1" version="1.0_0">
                <var attribute="snow_trs" context="',Nom_Modele,'" name="snow_trs"/>
                <var attribute="netSnow" context="HRULoop" name="netSnow"/>
                <var name="a_snow" value="1.5"/>
                <var attribute="intercStorage" context="HRULoop" name="intercStorage"/>
                <var attribute="throughfall" context="HRULoop" name="throughfall"/>
                <var attribute="netRain" context="HRULoop" name="netRain"/>
                <var attribute="snow_trans" context="',Nom_Modele,'" name="snow_trans"/>
                <var name="a_rain" value="1"/>
                <var attribute="area" context="HRULoop" name="area"/>
                <var attribute="actET" context="HRULoop" name="actET"/>
                <var attribute="snow" context="HRULoop" name="snow"/>
                <var attribute="actLAI" context="HRULoop" name="actLAI"/>
                <var attribute="potET" context="HRULoop" name="potET"/>
                <var attribute="tmean" context="HRULoop" name="tmean"/>
                <var attribute="rain" context="HRULoop" name="rain"/>
                <var attribute="interception" context="HRULoop" name="interception"/>
            </component>
# Module de neige
            <component class="snow.J2KProcessSnow_IRSTEA" enabled="true" name="J2KProcessSnow_IRSTEA" version="1.0_0">
                <var attribute="totDens" context="HRULoop" name="totDens"/>
                <var attribute="netSnow" context="HRULoop" name="netSnow"/>
                <var name="ccf_factor" value="0.0012"/>
                <var name="t_factor" value="1.84"/>
                <var attribute="snowCover" context="HRULoop" name="snowCover"/>
                <var name="r_factor" value="0.110"/>
                <var attribute="tmean" context="HRULoop" name="meanTemp"/>
                <var attribute="netRain" context="HRULoop" name="netRain"/>
                <var attribute="drySWE" context="HRULoop" name="drySWE"/>
                <var attribute="snowColdContent" context="HRULoop" name="snowColdContent"/>
                <var name="baseTemp" value="0.1"/>
                <var attribute="time" context="J2K_rhone" name="time"/>
                <var attribute="dryDens" context="HRULoop" name="dryDens"/>
                <var attribute="area" context="HRULoop" name="area"/>
                <var attribute="snowAge" context="HRULoop" name="snowAge"/>
                <var attribute="actSlAsCf" context="HRULoop" name="actSlAsCf"/>
                <var name="snowCritDens" value="0.7"/>
                <var attribute="snowTotSWE" context="HRULoop" name="snowTotSWE"/>
                <var name="active" value="true"/>
                <var attribute="snowMelt" context="HRULoop" name="snowMelt"/>
                <var attribute="snowDepth" context="HRULoop" name="snowDepth"/>
                <var name="g_factor" value="1.739"/>
            </component>
# Calcul de l ecoulement de l eau dans le sol
            <component class="soil.J2KProcessLumpedSoilWater_Tom" enabled="true" name="J2KProcessLumpedSoilWater_Tom" version="1.1_0">
                <var attribute="inRD1" context="HRULoop" name="inRD1"/>
                <var attribute="netRain" context="HRULoop" name="netRain"/>
                <var attribute="inRD2" context="HRULoop" name="inRD2"/>
                <var name="soilImpGT50" value="0.45"/>
                <var name="soilLatVertLPS" value="1"/>
                <var name="soilImpGT90" value="0.05"/>
                <var attribute="actMPS2" context="HRULoop" name="actMPS2"/>
                <var attribute="infAfterMPS" context="HRULoop" name="infAfterMPS"/>
                <var name="soilConcRD2" value="1"/>
                <var name="soilImpGT10" value="0.85"/>
                <var name="soilDistMPSLPS" value="0"/>
                <var attribute="netSnow" context="HRULoop" name="netSnow"/>
                <var attribute="actMPS" context="HRULoop" name="actMPS"/>
                <var attribute="potET" context="HRULoop" name="potET"/>
                <var name="soilImpGT0" value="0.95"/>
                <var name="soilPolRed" value="0"/>
                <var attribute="area" context="HRULoop" name="area"/>
                <var attribute="satMPS" context="HRULoop" name="satMPS"/>
                <var attribute="outRD1" context="HRULoop" name="outRD1"/>
                <var attribute="maxMPS" context="HRULoop" name="maxMPS"/>
                <var attribute="outRD2" context="HRULoop" name="outRD2"/>
                <var attribute="MaxInfSnow" context="HRULoop" name="soilMaxInfSnow"/>
                <var name="soilImpGT60" value="0.35"/>
                <var name="soilDiffMPSLPS" value="5"/>
                <var attribute="slope" context="HRULoop" name="slope"/>
                <var attribute="infiltration" context="HRULoop" name="infiltration"/>
                <var name="soilImpGT20" value="0.75"/>
                <var attribute="MaxInfSummer" context="HRULoop" name="soilMaxInfSummer"/>
                <var name="soilOutLPS" value="5"/>
                <var attribute="maxInf2" context="HRULoop" name="maxInf2"/>
                <var name="soilImpGT30" value="0.65"/>
                <var name="soilMaxPerc" value="20"/>
                <var attribute="MobileWater2" context="HRULoop" name="MobileWater2"/>
                <var attribute="actET" context="HRULoop" name="actET"/>
                <var name="soilImpGT70" value="0.25"/>
                <var attribute="infAfterSealedGrade" context="HRULoop" name="infAfterSealedGrade"/>
                <var name="soilLinRed" value="0.9"/>
                <var attribute="snowDepth" context="HRULoop" name="snowDepth"/>
                <var attribute="actLPS" context="HRULoop" name="actLPS"/>
                <var attribute="percolation" context="HRULoop" name="percolation"/>
                <var attribute="actDPS" context="HRULoop" name="actDPS"/>
                <var name="soilMaxDPS" value="0"/>
                <var attribute="sealedGrade" context="HRULoop" name="sealedGrade"/>
                <var name="soilImpGT40" value="0.55"/>
                <var attribute="satLPS" context="HRULoop" name="satLPS"/>
                <var attribute="maxLPS" context="HRULoop" name="maxLPS"/>
                <var name="soilImpGT80" value="0.15"/>
                <var attribute="DeltaETP" context="HRULoop" name="DeltaETP"/>
                <var attribute="genRD1" context="HRULoop" name="genRD1"/>
                <var attribute="actETintc" context="HRULoop" name="actETintc"/>
                <var attribute="snowMelt" context="HRULoop" name="snowMelt"/>
                <var name="soilConcRD1" value="1"/>
                <var attribute="genRD2" context="HRULoop" name="genRD2"/>
                <var attribute="ReductionFactor" context="HRULoop" name="ReductionFactor"/>
                <var attribute="satSoil" context="HRULoop" name="satSoil"/>
                <var attribute="deltaMPS2" context="HRULoop" name="deltaMPS2"/>
                <var attribute="time" context="J2K_rhone" name="time"/>
                <var attribute="MaxInfWinter" context="HRULoop" name="soilMaxInfWinter"/>
                <var attribute="interflow" context="HRULoop" name="interflow"/>
            </component>
# Calcul de l ecoulement souterrain
            <component class="soil.J2KProcessGroundwater_satRG1" enabled="true" name="J2KProcessGroundwater_satRG1" version="1.0_2">
                <var attribute="maxMPS" context="HRULoop" name="maxSoilStorage"/>
                <var attribute="actMPS" context="HRULoop" name="actSoilStorage"/>
                <var attribute="outRD2" context="HRULoop" name="gwExcess"/>
                <var name="gwRG2Fact" value="1.0"/>
                <var attribute="outRG1" context="HRULoop" name="outRG1"/>
                <var attribute="outRG2" context="HRULoop" name="outRG2"/>
                <var attribute="maxRG2" context="HRULoop" name="maxRG2"/>
                <var attribute="maxRG1" context="HRULoop" name="maxRG1"/>
                <var name="gwRG1RG2dist" value="0"/>
                <var attribute="inRG1" context="HRULoop" name="inRG1"/>
                <var attribute="slope" context="HRULoop" name="slope"/>
                <var attribute="actRG2" context="HRULoop" name="actRG2"/>
                <var attribute="actRG1" context="HRULoop" name="actRG1"/>
                <var attribute="satRG2" context="HRULoop" name="satRG2"/>
                <var attribute="satRG1" context="HRULoop" name="satRG1"/>
                <var attribute="percolation" context="HRULoop" name="percolation"/>
                <var attribute="genRG1" context="HRULoop" name="genRG1"/>
                <var name="gwCapRise" value="0"/>
                <var name="gwRG1Fact" value="1.0"/>
                <var attribute="genRG2" context="HRULoop" name="genRG2"/>
                <var attribute="RG2_k" context="HRULoop" name="kRG2"/>
                <var attribute="RG1_k" context="HRULoop" name="kRG1"/>
                <var attribute="inRG2" context="HRULoop" name="inRG2"/>
            </component>
# TemporalSumAggregator (utilite a prouver...)
            <component class="jams.components.aggregate.TemporalSumAggregator" enabled="true" name="TemporalSumAggregator1" version="1.0_0">
                <var attribute="time" context="',Nom_Modele,'" name="time"/>
                <var attribute="area" context="HRULoop" name="weight"/>
                <var attribute="outRD1;outRD2;outRG1;outRG2" context="HRULoop" name="value"/>
                <var attribute="outRD1_avg;outRD2_avg;outRG1_avg;outRG2_avg" context="HRULoop" name="sum"/>
                <var attribute="timeInterval" context="',Nom_Modele,'" name="aggregationTimeInterval"/>
                <var name="average" value="true"/>
            </component>
# Routage de HRU a HRU
            <component class="jams.components.datatransfer.DoubleTransfer" enabled="true" name="HRU2HRURouting" version="1.0_0">
                <var attribute="outRD1;outRD2;outRG1;outRG2" context="HRULoop" name="values"/>
                <var attribute="to_poly" context="HRULoop" name="target"/>
                <var name="inNames" value="inRD1;inRD2;inRG1;inRG2"/>
            </component>
#Routage de HRU a brin            
            <component class="jams.components.datatransfer.DoubleTransfer" enabled="true" name="HRU2ReachRouting" version="1.0_0">
                <var attribute="outRD1;outRD2;outRG1;outRG2" context="HRULoop" name="values"/>
                <var attribute="to_reach" context="HRULoop" name="target"/>
                <var name="inNames" value="inRD1;inRD2;inRG1;inRG2"/>
            </component>
# Moyenne sur le bassin complet pour les variables non converties
            <component class="org.unijena.j2k.aggregate.WeightedSumAggregator" enabled="true" name="SpatialWeightedSumAggregator1" version="1.0_0">
                <var attribute="areaweight" context="HRULoop" name="weight"/>
                <var attribute="',Model_Outputs2[1,1],'" context="HRULoop" name="value"/>
                <var attribute="',Model_Outputs2[1,1],'" context="TimeLoop" name="sum"/>
            </component>
# Moyenne sur le bassin complet pour les variables a convertir
            <component class="org.unijena.j2k.aggregate.WeightedSumAggregator" enabled="true" name="SpatialWeightedSumAggregator2" version="1.0_0">
                <var attribute="area" context="',Nom_Modele,'" name="weight"/>   
                <var attribute="',Model_Outputs2L[1,1],'" context="HRULoop" name="value"/>
                <var attribute="',Model_Outputs2L[1,1],'" context="TimeLoop" name="sum"/>
            </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
#Ajout des modules d'irrigation pour le calcul de la demande
if (Irrigation == 'oui'){
IrrigationModule2 (output_file)
}

#Reprise de l'ecriture
#Boucle sur les stations pour les moyennes de bassins
write.table('        </contextcomponent>',output_file,col.names=F,row.names=F,quote=F,append=T)
        
for (i in Stations){
Nom_Boucle = i
Water2 = Water(Nom_Boucle,Correspondance,reach,Corres_ID_Stations)
Boucle_Agreg(Nom_Boucle,Nom_Modele,output_file,Dossier_Temp,Water2,Corres_ID_Stations,SortiesL,Sorties)
}
#Ecriture du ReachLoop
write.table(paste('        <contextcomponent class="jams.model.JAMSSpatialContext" enabled="true" name="ReachLoop" version="1.0_0">
            <var attribute="reaches" context="',Nom_Modele,'" name="entities"/>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
#Ajout du module d'irrigation permettant de calculer la quantite d'eau disponible pour l'irrigation
if (Irrigation == 'oui'){
IrrigationModule3 (output_file)
}
# Ajout de la regionalisation des barrages (necessaire pour extraire la valeur journaliere de la FO
if (Barrage == 'oui'){
write.table(paste('        <component class="management.Regionalisation_Dam" enabled="true" name="Regionalisation_Dam" version="1.0_0">
                <var attribute="dataArrayFO" context="J2K_rhone" name="dataArray"/>
                <var attribute="Smax" context="ReachLoop" name="Smax"/>
                <var attribute="namesFO" context="J2K_rhone" name="names"/>
                <var attribute="ID" context="ReachLoop" name="ID"/>
                <var attribute="reaches" context="J2K_rhone" name="entities"/>
                <var attribute="FO" context="ReachLoop" name="dataValue"/>
            </component>
# Module pour l application de la fonction objectif (on stocke ou on relache)
            <component class="management.DamDevice" enabled="true" name="DamDevice" version="3.0_0">
                <var attribute="Storage" context="ReachLoop" name="Storage"/>
                <var attribute="inRG1" context="ReachLoop" name="inRG1"/>
                <var attribute="time" context="J2K_rhone" name="time"/>
                <var attribute="inRD2" context="ReachLoop" name="inRD2"/>
                <var attribute="FO" context="ReachLoop" name="FO"/>
                <var attribute="inRD2" context="ReachLoop" name="inRD1"/>
                <var attribute="Smax" context="ReachLoop" name="Smax"/>
                <var attribute="V0" context="ReachLoop" name="V0"/>
                <var attribute="actFO" context="ReachLoop" name="FO_fin"/>
                <var attribute="inRG2" context="ReachLoop" name="inRG2"/>
            </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
            }
# Module de routage brin a brin
write.table(paste('        <component class="org.unijena.j2k.routing.J2KProcessReachRouting" enabled="true" name="J2KProcessReachRouting" version="1.0_1">
                <var attribute="inRD2" context="ReachLoop" name="inRD2"/>
                <var attribute="inRD1" context="ReachLoop" name="inRD1"/>
                <var attribute="catchmentRD1" context="TimeLoop" name="catchmentRD1"/>
                <var attribute="catchmentRD2" context="TimeLoop" name="catchmentRD2"/>
                <var attribute="catchmentSimRunoff" context="TimeLoop" name="catchmentSimRunoff"/>
                <var attribute="reachOutRD2" context="ReachLoop" name="outRD2"/>
                <var attribute="reachOutRD1" context="ReachLoop" name="outRD1"/>
                <var attribute="slope" context="ReachLoop" name="slope"/>
                <var attribute="actRG2" context="ReachLoop" name="actRG2"/>
                <var attribute="actRG1" context="ReachLoop" name="actRG1"/>
                <var attribute="length" context="ReachLoop" name="length"/>
                <var attribute="TA" context="ReachLoop" name="flowRouteTA"/>
                <var name="slopeAsProportion" value="false"/>
                <var attribute="simRunoff" context="ReachLoop" name="simRunoff"/>
                <var attribute="reachOutRG1" context="ReachLoop" name="outRG1"/>
                <var attribute="width" context="ReachLoop" name="width"/>
                <var attribute="reachOutRG2" context="ReachLoop" name="outRG2"/>
                <var attribute="catchmentRG2" context="TimeLoop" name="catchmentRG2"/>
                <var attribute="rough" context="ReachLoop" name="roughness"/>
                <var attribute="channelStorage" context="ReachLoop" name="channelStorage"/>
                <var attribute="reaches" context="J2K_rhone" name="entities"/>
                <var attribute="catchmentRG1" context="TimeLoop" name="catchmentRG1"/>
                <var attribute="inRG1" context="ReachLoop" name="inRG1"/>
                <var attribute="actRD2" context="ReachLoop" name="actRD2"/>
                <var attribute="actRD1" context="ReachLoop" name="actRD1"/>
                <var name="tempRes" value="d"/>
                <var attribute="inRG2" context="ReachLoop" name="inRG2"/>
            </component>
# Agregation stock en riviere
            <component class="org.unijena.j2k.aggregate.SumAggregator" enabled="true" name="ChannelStorageAggregator" version="1.0_0">
                <var attribute="channelStorage" context="ReachLoop" name="value"/>
                <var attribute="channelStorage" context="TimeLoop" name="sum"/>
            </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)

# Ajout de la boucle pour passer les composantes du debit depuis le ReachLoop vers le TimeLoop
liste_brins = NULL
for (i in Stations){
liste_brins <- c(liste_brins,as.character(Correspondance[which (Correspondance[,1] == i),2]))
}
write.table(t(liste_brins),paste(Dossier_Temp,'Boucle_temp2.txt',sep=''),col.names=F,row.names=F,quote=F,append=F,sep=';')
liste_br <- read.table(paste(Dossier_Temp,'Boucle_temp2.txt',sep=''))
file.remove(paste(Dossier_Temp,'Boucle_temp2.txt',sep=''))
for (variable in variables){
indice_var = as.numeric(which(variable==variables))
SwitchContext(variable,Correspondance,Stations,liste_br,output_file,indice_var)
}

# Ajout des derivations (identification de la FO des derivations) et sortie des variables FO reelle et stockage dans le barrage
if (Barrage =='oui'){
Derivation1 (output_file,liste_deriv,Der_colNom,Der_colBrinEntree)
Barrage1 (output_file,liste_barrage,Bar_colNom,Bar_colBrinSortie)
Storage (output_file,liste_barrage,Bar_colNom,Bar_colBrinSortie)
}
                                      
#Module pour la conversion des composantes du debit (de L/j a L/s)                                      
write.table(paste('        </contextcomponent>
        <component class="jams.components.tools.JAMSUnitConverter" enabled="true" name="SimRunoffConverter" version="1.0_0">
            <var name="outUnit" value="L/s"/>
            <var attribute="catchmentSimRunoff_qm" context="TimeLoop" name="outValue"/>
            <var name="inUnit" value="L/day"/>
            <var attribute="catchmentSimRunoff" context="TimeLoop" name="inValue"/>
        </component>
        <component class="jams.components.tools.JAMSUnitConverter" enabled="true" name="SimRD2Converter" version="1.0_0">
            <var name="outUnit" value="L/s"/>
            <var attribute="catchmentRD2_qm" context="TimeLoop" name="outValue"/>
            <var name="inUnit" value="L/day"/>
            <var attribute="catchmentRD2" context="TimeLoop" name="inValue"/>
        </component>
        <component class="jams.components.tools.JAMSUnitConverter" enabled="true" name="SimRD1Converter" version="1.0_0">
            <var name="outUnit" value="L/s"/>
            <var attribute="catchmentRD1_qm" context="TimeLoop" name="outValue"/>
            <var name="inUnit" value="L/day"/>
            <var attribute="catchmentRD1" context="TimeLoop" name="inValue"/>
        </component>
        <component class="jams.components.tools.JAMSUnitConverter" enabled="true" name="SimRG1Converter" version="1.0_0">
            <var name="outUnit" value="L/s"/>
            <var attribute="catchmentRG1_qm" context="TimeLoop" name="outValue"/>
            <var name="inUnit" value="L/day"/>
            <var attribute="catchmentRG1" context="TimeLoop" name="inValue"/>
        </component>
        <component class="jams.components.tools.JAMSUnitConverter" enabled="true" name="SimRG2Converter" version="1.0_0">
            <var name="outUnit" value="L/s"/>
            <var attribute="catchmentRG2_qm" context="TimeLoop" name="outValue"/>
            <var name="inUnit" value="L/day"/>
            <var attribute="catchmentRG2" context="TimeLoop" name="inValue"/>
        </component>',sep=''),output_file,col.names=F,row.names=F,quote=F,append=T)
        
#Application des FO pour les derivations
if(Barrage == 'oui'){
Derivation2 (output_file,liste_deriv,Der_colNom,Der_colBrinEntree,Der_colBrinSortie)
}
#Fin du fichier
write.table('    </contextcomponent>
</model>',output_file,col.names=F,row.names=F,quote=F,append=T)
}