### J2000 post-processing functions for simulation results analysis
### Base = simulations with daily time step (but could maybe work for other time steps, not tested yet)

####### INTERANNUAL MONTHLY AVERAGE ANALYSIS #########

### Function J2000MonthStats
### function that calculates monthly statistics for J2000 outputs
## (discharge, runoff contribution, mean saturation levels, evapotranspiration, runoff coeff)
## inputs:
## - multivariate zoo object from ReadTimeLoop function (daily time step, runoff in m3/s, rain and evap in mm)
## - size of catchment in m2 (for calculation of runoff coefficient)

J2000MonthStats <- function(Data, size)
{
# Extract discharge data from Data object (total runoff + runoff contributions + obsrunoff)
Q <- Data[,4:11]
# Extract rain and evapotranspiration data (Potential ET and Actual ET)
RET <- Data[,1:3]

# Calculate monthly mean values for each discharge column and saturation levels
# For average values for each month of each year
Qmonth <-aggregate(Q,format(as.Date(index(Q)), '%m'),FUN=mean)

# Calculate cumulate values for rain and evapotranspiration for each month fo each year
temp <-aggregate(RET,as.yearmon(index(RET)),FUN=sum)
# Then calculate interannual average for each month
RETmonth <-aggregate(temp,format(as.Date(index(temp)), '%m'),FUN=mean)

# Calculate runoff coefficient for each month of each year (daily time step only!!)
temp2 <-aggregate(Q$catchmentSimRunoff_qm*86400,as.yearmon(index(Q),FUN=sum))/(temp$precip*size/1000)
# Calculate average monthly stat
RCmonth <-  aggregate(temp2,format(as.Date(index(temp2)), '%m'),FUN=mean)              
# Calculate average soil saturation
Soilsat <- aggregate((Q$satLPS+Q$satMPS)/2, format(as.Date(index(Q)), '%m'),FUN=mean)

# Merge and return zoo object
Month <- merge(Qmonth,RETmonth,RCmonth,Soilsat)
Month
}

### Function J2000ReachMonthStats
### function that calculates base monthly statistics for J2000 outputs
## (discharge, runoff contributions)
## input:
## - multivariate zoo object from ReadReachExtraction function (daily time step, runoff in m3/s)

J2000ReachMonthStats <- function(Data)
{
  # Extract total runoff
  Q <- Data[,1]
  # Extract runoff contributions
  Contribs <- Data[,3:6]
  
  # Calculate monthly mean values for each discharge column
  # For average values for each month of each year
  Qmonth <-aggregate(Q,format(as.Date(index(Q)), '%m'),FUN=mean)
  # same for contributions
  Contribsmonth <-aggregate(Contribs,format(as.Date(index(Contribs)), '%m'),FUN=mean)
  
  # Merge and return zoo object
  Month <- merge(Qmonth,Contribsmonth)
  Month
}

### Function J2000MonthMax
### function that selects the max discharge of each month of each year and then calculates the average interannual value + max interannual value
## inputs:
## - univariate zoo object of simulatedRunoff (daily time step, runoff in m3/s)
J2000MonthMax<- function(Data)
{
  # Select the max discharge for each month of each year
  temp <-aggregate(Data,as.yearmon(index(Data)),FUN=max)
  # Calculate interannual mean of each month
  MonthmaxMean <-aggregate(temp,format(as.Date(index(temp)), '%m'),FUN=mean)
  # Calculate max interannual value of each month
  MonthmaxMax <-aggregate(temp,format(as.Date(index(temp)), '%m'),FUN=max)
  # Merge and return zoo object
  Month <- merge(MonthmaxMean,MonthmaxMax)
  Month
}

### Function J2000MonthThresh
### function that selects discharge data above a certain threshold
## counts lenght of events (number of days) and volume (m3) and computes monthly stats on them
## inputs:
## - univariate zoo object of simulatedRunoff from ReadTimeLoop function (daily time step, runoff in m3/s)
## - value of discharge threshold in m3/s
J2000MonthThresh <- function(Data, thresh)
{
# Select events above threshold
Events <- subset(Data, Data >= thresh)

# Calculate number of days above threshold for each month
temp <-aggregate(Events,as.yearmon(index(Events)),FUN=length)
# Calulate mean number of days per month 
Nbdays <-aggregate(temp,format(as.Date(index(temp)), '%m'),FUN=mean)

# Calculate mean discharge above threshold
Q<- aggregate(Events,format(as.Date(index(Events)), '%m'),FUN=mean)

# Calculate flow volume above threshold for each month
temp <-aggregate(Events*86400,as.yearmon(index(Events)),FUN=sum)
# aggregate
Vol <-aggregate(temp,format(as.Date(index(temp)), '%m'),FUN=mean)

# Merge and return zoo object
Month <- merge(Nbdays, Q, Vol)
#Qmonth <-aggregate(na.omit(Q),chron(as.character(as.Date(as.yearmon(index(na.omit(Q))))),format=c(dates="y-m-d")),FUN=mean)
Month
}

### Function J2000MonthUnderThresh
### function that selects and analyses monthly discharge data below a certain threshold
## counts lenght of events (number of days), mean discharge under thresh (m3/s) and volume (m3)
## inputs:
## - univariate zoo object of simulatedRunoff (daily time step, runoff in m3/s)
## - value of discharge threshold in m3/s
J2000MonthUnderThresh <- function(Data, thresh)
{
# Select events above threshold
Events <- subset(Data, Data < thresh)

# Calculate number of days under threshold for each month
temp <-aggregate(Events,as.yearmon(index(Events)),FUN=length)
# aggregate
Nbdays <-aggregate(temp,format(as.Date(index(temp)), '%m'),FUN=mean)

# Calculate mean discharge under threshold
Q<- aggregate(Events,format(as.Date(index(Events)), '%m'),FUN=mean)

# Calculate flow volume under threshold for each month
temp <-aggregate(Events*86400,as.yearmon(index(Events)),FUN=sum)
# aggregate
Vol <-aggregate(temp,format(as.Date(index(temp)), '%m'),FUN=mean)

# Merge and return zoo object
res <- merge(Nbdays, Q, Vol)

# return result
res
}


######## YEARLY AVERAGE ANALYSIS ##################

### Function J2000YearStats
### function that calculates yearly statistics for J2000 outputs
## (discharge, runoff contribution, mean saturation levels, evapotranspiration, runoff coeff)
## inputs:
## - multivariate zoo object from ReadTimeLoop function (daily time step, runoff in m3/s, rain and evap in mm)
## - size of catchment in m2 (for calculation of runoff coefficient)
J2000YearStats <- function(Data, size)
{
  # Extract discharge data from Data object (total runoff + runoff contributions)
  Q <- Data[,4:11]
  # Extract rain and evapotranspiration data
  RET <- Data[,1:3]
  
  # Calculate yearly mean values for each discharge column and saturation levels
  Qyear <-aggregate(Q,format(as.Date(index(Q)), '%Y'),FUN=mean)
  
  # Calculate yearly cumulate values for rain and evapotranspiration
  RETyear <-aggregate(RET,format(as.Date(index(RET)), '%Y'),FUN=sum)
  # Calculate runoff coefficient (daily time step only!!)
  RCyear <-aggregate(Q$catchmentSimRunoff_qm*86400,format(as.Date(index(Q)),'%Y'),FUN=sum) / (RETyear$precip*size/1000)
  
  # Calculate average soil saturation
  Soilsat <- aggregate((Q$satLPS+Q$satMPS)/2, format(as.Date(index(Q)), '%Y'),FUN=mean)
  
  # Merge and return zoo object
  Year <- merge(Qyear, RETyear, RCyear,Soilsat)
  #Qmonth <-aggregate(na.omit(Q),chron(as.character(as.Date(as.yearmon(index(na.omit(Q))))),format=c(dates="y-m-d")),FUN=mean)
  Year
}

### Function J2000ReachYearStats
### function that calculates base yearly statistics for J2000 outputs
## (discharge, runoff contributions)
## input:
## - multivariate zoo object from ReadReachExtraction function (daily time step, runoff in m3/s)

J2000ReachYearStats <- function(Data)
{
  # Extract total runoff
  Q <- Data[,1]
  # Extract runoff contributions
  Contribs <- Data[,3:6]
  
  # Calculate monthly mean values for each discharge column
  # For average values for each month of each year
  Qyear <-aggregate(Q,format(as.Date(index(Q)), '%Y'),FUN=mean)
  # same for contributions
  Contribsyear <-aggregate(Contribs,format(as.Date(index(Contribs)), '%Y'),FUN=mean)
  
  # Merge and return zoo object
  Year <- merge(Qyear,Contribsyear)
  Year
}

### Function J2000YearMax
### function that selects the max discharge of each year
## inputs:
## - univariate zoo object of simulatedRunoff (daily time step, runoff in m3/s)
J2000YearMax<- function(Data)
{
  Yearmax <-aggregate(Data,format(as.Date(index(Data)), '%Y'),FUN=max)
  Yearmax
}

### Function J2000YearThresh
### function that selects and analyses yearly and monthly discharge data above a certain threshold
## counts lenght of events (number of days) and volume (m3)
## inputs:
## - univariate zoo object of simulatedRunoff (daily time step, runoff in m3/s)
## - value of discharge threshold in m3/s
J2000YearThresh <- function(Data, thresh)
{
# Select events above threshold
Events <- subset(Data, Data >= thresh)

# Calculate yearly number of days above threshold
Nbdays <-aggregate(Events,format(as.Date(index(Events)), '%Y'),FUN=length)

# Calculate mean discharge above threshold
Q<- aggregate(Events,format(as.Date(index(Events)), '%Y'),FUN=mean)

# Calculate yearly flow volume above threshold
Vol <-aggregate(Events*86400,format(as.Date(index(Events)), '%Y'),FUN=sum)

# Merge and return zoo object
Year <- merge(Nbdays, Q, Vol)
Year
}


### Function J2000YearUnderThresh
### function that selects and analyses yearly and monthly discharge data below a certain threshold
## counts lenght of events (number of days) and volume (m3)
## inputs:
## - univariate zoo object of simulatedRunoff (daily time step, runoff in m3/s)
## - value of discharge threshold in m3/s
J2000YearUnderThresh <- function(Data, thresh)
{
# Select events under threshold
Events <- subset(Data, Data < thresh)

# Calculate yearly number of days under threshold
Nbdays <-aggregate(Events,format(as.Date(index(Events)), '%Y'),FUN=length)

# Calculate mean discharge under threshold
Q<- aggregate(Events,format(as.Date(index(Events)), '%Y'),FUN=mean)

# Calculate yearly flow volume under threshold (daily time step only)
Vol <-aggregate(Events*86400,format(as.Date(index(Events)), '%Y'),FUN=sum)

# Merge and return zoo object
Year <- merge(Nbdays, Q, Vol)
Year
}

### Function J2000QIndicators
### function that calculates a set of indicators for a given time series: mean interannual discharge, max discharge, % of low flow (under thresh)
### and relative percentages of base flow, interflow and surface runoff
## inputs:
## - univariate zoo object of simulatedRunoff (daily time step, runoff in m3/s)
## - multivariate zoo object of flow contributions (in order RD1, RD2, RG1, RG2)
## - threshold for low flow
J2000QIndicators <- function(Q, contribs, thresh)
{
  # Calculate mean discharge
  MeanQ <- mean(Q, na.rm = TRUE)
    # Calculate max discharge
  MaxQ <- max(Q, na.rm = TRUE)
  # Select events under threshold
  Events <- subset(Q, Q < thresh)
  # Calculate low flow indicator (percentage of days under thresh for the time series)
  Low <- length(Events)/length(Q)*100
    # Calculate relative contrib of RD1
  RD1 <- mean(contribs[,1], na.rm = TRUE)/MeanQ
  # Calculate relative contrib of RD2
  RD2 <- mean(contribs[,2], na.rm = TRUE)/MeanQ
  # Calculate relative contrib of RG1
  RG1 <- mean(contribs[,3], na.rm = TRUE)/MeanQ
  # Calculate relative contrib of RG2
  RG2 <- mean(contribs[,4], na.rm = TRUE)/MeanQ
  
  # Merge and return set of indicators
  Indic <- c(MeanQ, MaxQ, Low, RD1,RD2,RG1,RG2)
  Indic
}

### Function J2000balanceIndicators
### function that calculates a set of indicators for a given time series: mean soil moisture, mean PET, mean RET
## inputs:
## - bivariate zoo object of soil moisture (daily time step, MPS/LPS)
## - bivariate zoo object of evapotranspiration (PET actET in order)
J2000balanceIndicators <- function(moist, ET)
{
  # Calculate mean soil moisture
  Meanmoist <- mean((moist[,1]+moist[,2])/2)
  # Calculate mean annual ET
  # Calculate yearly cumulate values evapotranspiration
  ETyear <-aggregate(ET,format(as.Date(index(ET)), '%Y'),FUN=sum)
  MeanPET <- mean(ETyear[,1])
  MeanactET <- mean(ETyear[,2])
  
  # Merge and return set of indicators
  Indic <- c(Meanmoist, MeanPET, MeanactET)
  Indic
}

