# Fonctions calculant les prélèvements moyens interannuels par canton sur les données AERMC.
# *****************************************************************************************

# Unités : dans Chronique_PrelevRMC_Cantons.txt, les prélèvements sont en x 1000 m3/yr.

# 81 : GRAVITAIRE. 82 : NON-GRAVITAIRE. 85 (for yr >=2008): CANAUX.
# on va surtout s'intéresser dans un 1er temps au NON-GRAV (82) sur 1987-2007.

# *****************************************************************************************
library(maptools)
library(rgeos)
library(sp)
library(raster)
library(xts)
library(zoo)

# *****************************************************************************************
# -------------------------------Prelev82_1987_2007-------------------------------
# Prelev82_1987_2007 <- function()
#      Calculates the mean annual water withdrawal across all cantons over 1987-2007
# Args:
#     None
# Returns:
#      A dataframe containing the canton and the corresponding mean annual water withdrawal
# -----------------------------------------------------------------------------
Prelev82_1987_2007=function(){ # m3/yr NTC
  Prelev <- read.table('~/Documents/MDR/irrigation/Chronique_PrelevRMC_Cantons.txt',header=T)
  
  Prelev82=NULL
  for (cant in Prelev$Canton[order(unique(Prelev$Canton))]){
    Prelev82_ann = Prelev[which(Prelev[,1] == cant),5] * 1000 #(m3)
    Prelev_date= as.Date(as.character(Prelev[which(Prelev[,1] == cant),2]), format="%Y")
    Prelev82_ann = xts(Prelev82_ann, Prelev_date)
    Prelev82 = rbind(Prelev82,mean(Prelev82_ann["1987/2007"])) # prélèvement annuel moyen sur 1987-2007
  }
  Prelev82data=cbind(Prelev$Canton[order(unique(Prelev$Canton))], Prelev82)
  colnames(Prelev82data)=c('canton','Prelev82')
  return(Prelev82data)
}

# *****************************************************************************************

Prelev8182_1987_2007=function(){ # m3/yr NTC
  Prelev <- read.table('~/Documents/MDR/irrigation/Chronique_PrelevRMC_Cantons.txt',header=T)
 
  Prelev8182=NULL
  for (cant in Prelev$Canton[order(unique(Prelev$Canton))]){
    Prelev82_ann = Prelev[which(Prelev[,1] == cant),5] * 1000 #(m3)
    Prelev81_ann = Prelev[which(Prelev[,1] == cant),4] * 1000 #(m3)
    Prelev_date= as.Date(as.character(Prelev[which(Prelev[,1] == cant),2]), format="%Y")
    
    Prelev82_ann = xts(Prelev82_ann, Prelev_date)
    Prelev81_ann = xts(Prelev81_ann, Prelev_date)
    
    Prelev8182 = rbind(Prelev8182,mean(Prelev81_ann["1987/2007"])+mean(Prelev82_ann["1987/2007"])) # prélèvement annuel moyen sur 1987-2007
  }
  Prelev8182data=cbind(Prelev$Canton[order(unique(Prelev$Canton))], Prelev8182)
  colnames(Prelev8182data)=c('canton','Prelev8182')
  
  return(Prelev8182data)
}

# *****************************************************************************************

Prelev8182_2008_2012=function(){ # m3/yr NTC
  Prelev <- read.table('~/Documents/MDR/irrigation/Chronique_PrelevRMC_Cantons.txt',header=T)
  
  Prelev8182=NULL
  for (cant in Prelev$Canton[order(unique(Prelev$Canton))]){
    Prelev82_ann = Prelev[which(Prelev[,1] == cant),5] * 1000 #(m3)
    Prelev81_ann = Prelev[which(Prelev[,1] == cant),4] * 1000 #(m3)
    Prelev_date= as.Date(as.character(Prelev[which(Prelev[,1] == cant),2]), format="%Y")
    
    Prelev82_ann = xts(Prelev82_ann, Prelev_date)
    Prelev81_ann = xts(Prelev81_ann, Prelev_date)
    
    Prelev8182 = rbind(Prelev8182,mean(Prelev81_ann["2008/2012"])+mean(Prelev82_ann["2008/2012"])) # prélèvement annuel moyen sur 2008 - 2012
  }
  Prelev8182data=cbind(Prelev$Canton[order(unique(Prelev$Canton))], Prelev8182)
  colnames(Prelev8182data)=c('canton','Prelev8182')
  
  return(Prelev8182data)
}



# 2. Sur quels cantons simule-t-on l'irrigation ?
# ************************************************
# CantonsIrr <- readShapeSpatial('~/DATA/SIG_MDR/irrigation/cantons_irrigues_3.shp', proj4string=CRS ("+init=epsg:2154"),verbose=FALSE,repair=FALSE,IDvar=NULL,force_ring=FALSE,delete_null_obj=FALSE, retrieve_ABS_null=FALSE)
# NumCantonsIrr=CantonsIrr$CODE_CAN_1
# 
# Prelev82_noscantons=Prelev82data[Prelev82data[,1]%in%NumCantonsIrr,]
# write.table(Prelev82_noscantons,'~/Documents/MDR/irrigation/Prelev82_1987_2007_M3_noscantonsirrigues.txt',append=F, sep="\t", row.names=FALSE, col.names=c('Canton', 'Prelev82_m3_87_2007')) # en m3



# 3. CANTONS AERMC vs NOS CANTONS IRRIGUES
# ******************************************
Prelev <- read.table('~/Documents/MDR/irrigation/Chronique_PrelevRMC_Cantons.txt',header=T)
allcantons=Prelev$Canton[order(unique(Prelev$Canton))]

# nos cantons - 104
Cantons_Rhone <- c(101,117,118,119,120,140,518,717,722,724,1333,2602,2604,2607,2611,2613,2615,2616,2619,2621,2623,2625,2626,2628,2629,2632,2634,3006,3009, 3016, 3023,3026,3802,3807,3808,3815,3819,3822,3824,3825,3830,3837,3846,3853,4213,4233,6907,6924,6931,6937,6938,6944,6945,6948,6949,7405,8405,8406,8409,8413,8415,8416,8418,8423)
Cantons_Durance <- c(410,413,414,416,419,420,421,427,429,430,505,509,512,515,516,522,523,524,1307, 1309,1312,1326,1327,1331,8319,8408,8411)
Cantons_Saone <- c(102,126,135,2103,2114,2134,2138,3909,6905,6910,6925,7116,7151)

un_canton=c(Cantons_Rhone,Cantons_Durance,Cantons_Saone)
un_canton <- un_canton[order(un_canton)]

irrigated= allcantons %in% un_canton +rep(0,length(allcantons))
write.table(irrigated,'~/Documents/MDR/irrigation/irrigated.txt',append=F, sep="\t", row.names=FALSE, col.names=FALSE)

