library(lubridate)




#####################################################################################
#                               ANALYSE SORTIES J2000 
#####################################################################################

#~* look for    NIU -> not in use
#~* or for      NTK -> need to keep
#~* maybe even  NTC -> need to check

# actually don't look for NIU, there's way too much, just keep the one with either NTK or NTC

#~* PROGRAMMER: Isabelle GOUTTEVIN (Irstea Lyon)
#~------------------------------------------------------------------------------
#~* CREATED/MODIFIED: Created 26-08-2015
#~------------------------------------------------------------------------------

# LAUNCH_SIMU(xx, model, params%xx)
# lance une simu en batch
#~------------------------------------------------------------------------------
# NIU
launch_simu=function(dir, model ,parameters=NULL){
  # INPUTS : 
  #     dir : /home/isabelle.gouttevin/JAMS/submodels/Arve_loc
  #     model : j2k_tmin_tmax_mail_wildcard.jam
  #     parameters= c(1,2) // vector.
  MODEL=paste(dir,'/', model,sep="")
  OPTIONS='-Xms150m -Xmx4096m -XX:MaxPermSize=128m -Dsun.java2d.d3d=false -Djava.library.path=../../bin/win64'
  MAINCLASS='/home/isabelle.gouttevin/JAMS/jams/jams-bin/jams-starter.jar'
  OPTIONS2=' -n -c /home/isabelle.gouttevin/JAMS/jams/default.jap '
  
  PARAM=NULL ; for (l in c(1:length(parameters))){if (l < length(parameters)){PARAM=paste(PARAM,parameters[l],';',sep='')} else {PARAM=paste(PARAM,parameters[l],sep='')}}
  if (!is.null(parameters)){
    system(paste('cd ', dir, ' ; java ', OPTIONS,' -jar ',MAINCLASS,OPTIONS2,' -m ', MODEL,' -p "', PARAM,'"',sep=''))
  }
  else {
    system(paste('cd ', dir,' ; java ', OPTIONS,' -jar ',MAINCLASS,OPTIONS2,' -m ', MODEL ,sep=''))
  }
}

# NIU
# TREATSIMU 
# ---------------------------------------------------------------------------
#~* Converts a simulation by J2000 (Timeloop.dat) into a zoo time-serie 
#   featuring the DISCHARGES at the desired stations (StationsOI )= 
#   Stations Of Interest
# 
#~* IN : simu = ReadTimeLoopDaily('../../Timeloop.dat')
#~*    StationsOI = c("X3000010","....") # code station BanqueHydro/MDR
#~*
#~* OUT : xts object contenant les débits (m3/s) journaliers aux StationsOI
#~*
#~* PARAMETER (chemin à renseigner) : 
#~* 	- la table "Correspondance" entre les stations et les brins du modèle
#~*	- le Debut et fin de simulation
#~-----------------------------------------------------------------------------
treatsimu=function(simu, StationsOI){
  library(xts)
  window = '1987-01-01/2012-12-31'
  Correspondance = read.table ('~/Documents/MDR/Correspondance_brins_stations_rhone.csv',sep=";",dec=".") # correspondance entre brins du reach.par et code des stations (station, brin, x)
  
  Reaches=Correspondance[!is.na(match(Correspondance[,1],StationsOI)),2]
  Stations=Correspondance[!is.na(match(Correspondance[,1],StationsOI)),1]
  mm=match(Correspondance[,1],StationsOI) # NA or index of StationOI (station 1, station 2, etc..) wihere there is a match
  Order=mm[!is.na(mm)]
  Cumul=table(Order) # For each match, (station 1, station 2, etc...), how many reaches ? 
  Names <- names(simu)
  indexes=match(paste('simRunoff_',Reaches,sep=''),Names) # indices de la colomne des reaches d'intérêt dans la simu
  discharges=simu[ ,indexes]/1000/24/3600 # m3/s
  
  SIM0 = rep(NULL, length(simu[,1]))
  for (i in 1:length(StationsOI)){
    if(Cumul[i]==1){
      SIM_tmp=as.vector(discharges[,which(Order==i)])
    }else{
      SIM_tmp=as.vector(apply(discharges[,which(Order==i)],1,sum))
    }
    SIM=cbind(SIM0,SIM_tmp)
    SIM0=SIM
  }
  SIM=xts(SIM,time(simu))
  return(SIM[window])
}

# NIU
# pr simus sur submodels 
# ----------------------
treatsimu_exutoire=function(simu){
  library(xts)
  window = '1987-01-01/2012-12-31'
  Names <- names(simu)
  indexe=match(paste('catchmentSimRunoff',sep=''),Names) # indices de la colomne des reaches d'intérêt dans la simu
  if(is.na(indexe)) indexe=match(paste('simRunoff',sep=''),Names) #nom dans le ReachLoop
  if(is.na(indexe)) indexe=match(paste('reachStation',sep=''),Names)
  SIM=as.numeric(simu[ ,indexe])/1000/24/3600 # m3/s
  SIM=xts(SIM,time(simu))
  colnames(SIM)=c("Q")
  return(SIM[window])
}

# ?? NIU
# -------
treatsimu_window=function(simu, StationsOI, window){
  library(xts)
  Correspondance = read.table ('~/Documents/MDR/Correspondance_brins_stations_rhone.csv',sep=";",dec=".") # correspondance entre brins du reach.par et code des stations (station, brin, x)
  
  Reaches=Correspondance[!is.na(match(Correspondance[,1],StationsOI)),2]
  Stations=Correspondance[!is.na(match(Correspondance[,1],StationsOI)),1]
  mm=match(Correspondance[,1],StationsOI) # NA or index of StationOI (station 1, station 2, etc..) wihere there is a match
  Order=mm[!is.na(mm)]
  Cumul=table(Order) # For each match, (station 1, station 2, etc...), how many reaches ? 
  Names <- names(simu)
  indexes=match(paste('simRunoff_',Reaches,sep=''),Names) # indices de la colomne des reaches d'intérêt dans la simu
  discharges=simu[ ,indexes]/1000/24/3600 # m3/s
  
  SIM0 = rep(NULL, length(simu[,1]))
  for (i in 1:length(StationsOI)){
    if(Cumul[i]==1){
      SIM_tmp=as.vector(discharges[,which(Order==i)])
    }else{
      SIM_tmp=as.vector(apply(discharges[,which(Order==i)],1,sum))
    }
    SIM=cbind(SIM0,SIM_tmp)
    SIM0=SIM
  }
  SIM=xts(SIM,time(simu))
  names(SIM)=StationsOI
  return(SIM[window])
}

# TREATOBS  NIU
# ---------------------------------------------------------------------------
#~* Converts the observed discharge at Stations Of Interest (StationsOI)
#~*  into a zoo time-serie 
#~* IN : StationsOI = c("X3000010","....") # code station BanqueHydro/MDR
#~*     OBS discharges in L/s
#~*
#~* OUT : xts object contenant les débits (m3/s) journaliers aux StationsOI
#~*
#~* PARAMETER (chemin à renseigner) : 
#~*   le chemin et le nom des fichiers contenant les obs 
#~*     (format : $ {Station}_OBS_ls.txt)
# ---------------------------------------------------------------------------
treatobs=function(StationsOI){
  library(xts)  
  deb = '1987-01-01'
  fin = '2012-12-31'
  OBS0=NULL
  for ( i in StationsOI){
    
    OBS_tmp <- as.vector(read.table(paste('~/DATA/HYDRO/Obs_New/',i,'_OBS_ls.txt',sep=''),sep='\t',skip=1)[,4]/1000) # m3/s
    OBS=cbind(OBS0,OBS_tmp)
    OBS0=OBS
  }

  OBS=xts(OBS,seq(as.POSIXct(deb, format="%Y-%m-%d"),as.POSIXct(fin, format="%Y-%m-%d"),by="1 d")
  )
  return(OBS)
}

# TREATOBS_inedites NIU
#~-------------------------------------------------------------------------------
#~*  For "inedites stations" : REF : Le Gros, C., Sauquet, E., Lang, M., Achard, A.-L., Leblois, E., Bitton, B., 2015. 
#~*        Les Annuaires Hydrologiques de la Société Hydrotechnique De France : Une source d’information patrimoniale pour la connaissance de l’hydrologie en France. La Houille Blanche, 4, 66-77 
#~*        doi: 10.1051/lhb/20150048.
#~-------------------------------------------------------------------------------
treatobs_inedite=function(Station){
  
  obsfile=grep(Station,readLines(pipe('ls ~/DATA/MDR_stations_inédites/observations/*')), value=TRUE)
  OBS = as.vector(read.table(obsfile,sep=';',skip=3, fill=TRUE)[,4]/1000) # m3/s
  OBS = OBS[1:length(OBS)-1]
  
  time=as.Date(as.character(read.table(obsfile,sep=';',skip=3, fill=TRUE)[,3]), format="%Y%m%d")
  time=time[1:length(time)-1]  
  
  OBS=zoo(OBS,time)
  return(OBS)
}

# SELECT_OPTIMUM NIU
# ----------------------------------------------------------------------------
# sélection du BEST dans un optimizer.dat
# - bv "Arve_loc"
# - optim_outputdir : "Optim_asp"
# - effcriteria_name : J2K_optim___main_eff_kge_normalized'
# - parameter_name : 'snow.J2KProcessSnow_IRSTEA_tmin_tmax___t_factor' 
# ----------------------------------------------------------------------------
select_optimum=function(bv, optim_outputdir,effcriteria_name,parameter_name){ # here optimal val for tf
  optim=read.table(paste('~/JAMS/submodels/',bv,'/output/',optim_outputdir,'/optimizer.dat', sep=''), skip=10)
  colnames(optim)=scan(paste('~/JAMS/submodels/',bv,'/output/',optim_outputdir,'/optimizer.dat', sep=''), skip=5, nlines=1, what='character')
  ind=which(optim[,effcriteria_name] == min(optim[,effcriteria_name]))[1] # select only one minimum
  tf=round(optim[ind,parameter_name],2)
  return(tf)
}


# GRAPHIQUES
# ------------------------------------------------------------------------------

# trace_graphes
# ------------------------------------------------------------------------------
#~* Creates the beautiful analyse-graph for discharge simulations at a station:
#~* 		LEFT side : monthly discharge over 1987-2012
#~*			    daily discharge over a window
#~*		  RIGHT side : mean annual cycle of modelled/observed discharge
#~*
#~* IN : - StationsOI = c("X3000010","....") # code station BanqueHydro/MDR
#~*	 - NamesOI =c("Durance Cadarache", "..")
#~*	 - SIM : zoo object with the simulated discharges (Ncols=length(StationsOI))
#~*	 - OBS : zoo object with the observed discharges (Ncols=length(StationsOI))
#~*	 - pdfname : ex : "mypdf.pdf"
#~*	 - graphefile_name : where to save that pdf ?
#~*	 - fullts : if true, the details of daily discherges are plotted after the main recap plot
#~*
#~* OUT : mypdf.pdf
#~*
#~* DEPENDANCES (chemin à renseigner) : 
#~* 	aggregateZoo_functions.r
# ------------------------------------------------------------------------------
# NIU
trace_graphes=function(StationsOI, NamesOI, SIM, OBS, pdfname=NULL, graphefile_name=NULL,fullts=NULL){
  source('lib/aggregateZoo_functions.r')
  library(lubridate)  
  library(xts)
  library(grid)
  
  if(!is.null(pdfname) && !is.null(graphefile_name)){pdf(paste(graphefile_name,pdfname,sep=''),paper="special",width=12,height=8)}
  
  SIM=SIM["1987-01-01/2012-12-31"]
  
  OBSm=aggregateZoo(OBS,'my',mean) 
  SIMm=aggregateZoo(SIM,'my',mean) 
  
  OBScycle=aggregateZoo(OBS, "m", mean)
  SIMcycle=aggregateZoo(SIM, "m", mean)
  
  for (i in 1:length(StationsOI)){
    
    layout(matrix(c(1:3,4,4,4),3,2))
    par (pty="m") # Paramètre pour avoir un graphe de taille max
    
    # 1. Tracé des débits mensuels
    plot(SIMm[,i],type='l',ylim=c(0,max(na.omit(SIMm[,i]),na.omit(OBSm[,i]))),main=NamesOI[i],col=2, xlab='Date',ylab='Q(m3/s)', cex.main=2)
    lines(OBSm[,i],col=1)
    legend("topleft",legend=c("OBS", "SIM"),y.intersp = 1, lty= 1,bty="n",col = c(1,2),xpd=NA,cex=0.8)
    
    # 2. Tracé d'une fenêtre de débits journaliers (originale François : 8037:9131 == 2009-01-01:2011-12-31)
    par (pty="m") 
    date1=as.POSIXct("1987-01-01")
    date2=as.POSIXct("1990-12-31")
    #date1=as.POSIXct("2005-01-01")
    #date2=as.POSIXct("2008-12-31")
    
    index1=which(time(SIM)==date1)
    index2=which(time(SIM)==date2)
    plot(SIM[index1:index2,i],type='l',main='Journalier 1987-1990',ylim=c(0,max(SIM[,i],na.omit(OBS[,i]))), xlab='Date',ylab='Q(m3/s)', xaxt="n")
    lines(SIM[index1:index2,i],col=2)
    lines(OBS[index1:index2,i],col=1)
    legend("topleft",legend=c("OBS", "SIM"),y.intersp = 1, lty= 1,bty="n",col = c(1,2),xpd=NA,cex=0.8)
    
    axis(1, at=seq(date1,date2, by="month"), labels=month(seq(date1,date2, by="month")))
    mtext(c(year(seq(date1,date2, by="year"))), 1, at=seq(date1,date2, by="year"), line=1.7, cex=0.8)
    
    # 2.2.
    par (pty="m") 
    date1=as.POSIXct("2009-01-01")
    date2=as.POSIXct("2012-12-31")
    
    index1=which(time(SIM)==date1)
    index2=which(time(SIM)==date2)
    plot(SIM[index1:index2,i],type='l',main='Journalier 2009-2012',ylim=c(0,max(SIM[,i],na.omit(OBS[,i]))), xlab='Date',ylab='Q(m3/s)', xaxt="n")
    lines(SIM[index1:index2,i],col=2)
    lines(OBS[index1:index2,i],col=1)
    legend("topleft",legend=c("OBS", "SIM"),y.intersp = 1, lty= 1,bty="n",col = c(1,2),xpd=NA,cex=0.8)
    
    axis(1, at=seq(date1,date2, by="month"), labels=month(seq(date1,date2, by="month")))
    mtext(c(year(seq(date1,date2, by="year"))), 1, at=seq(date1,date2, by="year"), line=1.7, cex=0.8)
    
    
    # 3. Tracé du cycle mensuel moyen des débits
    
    MAX<- max(na.omit(OBScycle[,i]),na.omit(SIMcycle[,i])) # hauteur max du graphe
    par (pty="m")
    plot(SIMcycle[,i],main = "Moyenne mensuelle interannuelle des débits",ylim=c(0, MAX*1.1),font.lab=2,type="l",col=2,xaxt = "n",xlab="Months",ylab="Q(m3/s)")
    lines(OBScycle[,i], col=1)
    ticks<-as.numeric(time(OBScycle))
    labs <- substr(month.abb,1,1)
    Axis(side = 1, at = ticks, labels = labs)
    legend("topleft",legend=c("OBS", "SIM"),y.intersp = 1, lty= 1,bty="n",col = c(1,2),xpd=NA,cex=0.8)
    #plot.new() #new page
    
    if(fullts==TRUE){
      #grid.newpage()
      Nbyears=round(dim(SIM)[1]/365)
      Nbrow=round(Nbyears/3/2)+1*(Nbyears/3/2>round(Nbyears/3/2))
      Nbpages=round(Nbrow/3)+1*(Nbrow/3>round(Nbrow/3))
      mystartyr = year(SIM[1])
      for (page in 1:Nbpages){
        layout(matrix(c(1:6),3,2))
        par (pty="m")
        yrstart=mystartyr+(page-1)*6*3 # pr la page
        yrend=min(mystartyr+(page)*6*3-1,year(SIM[length(SIM)]))#pr la page
        
        for (myyrbeg in seq(yrstart,yrend,by=3)){ # chaque plot en lighe de 3 ans
          
          date1=as.POSIXct(paste(myyrbeg,'-01-01',sep=""))
          date2=as.POSIXct(paste(min(myyrbeg+2,yrend),'-12-31',sep=""))
          
          index1=which(time(SIM)==date1)
          index2=which(time(SIM)==date2)
          plot(SIM[index1:index2,i],type='l',main=paste('Journalier ',myyrbeg,'-',min(myyrbeg+2,yrend),sep=""),ylim=c(0,max(SIM[,i],na.omit(OBS[,i]))), xlab='Date',ylab='Q(m3/s)', xaxt="n")
          lines(SIM[index1:index2,i],col=2)
          lines(OBS[index1:index2,i],col=1)
          legend("topleft",legend=c("OBS", "SIM"),y.intersp = 1, lty= 1,bty="n",col = c(1,2),xpd=NA,cex=0.8)
          
          axis(1, at=seq(date1,date2, by="month"), labels=month(seq(date1,date2, by="month")))
          mtext(c(year(seq(date1,date2, by="year"))), 1, at=seq(date1,date2, by="year"), line=1.7, cex=0.8)
          # par(new = T)
          # plot(70-Rainmean, col='blue',type='l' axes=F, xlab=NA, ylab=NA)
          # axis(side = 4)
          # mtext(side = 4, line = 3, 'Rain(mm/d)')
        }
      }
    }
  }
  
  if(!is.null(pdfname) && !is.null(graphefile_name)){graphics.off()} 
}

# trace_graphes_2 : superposer 2 simus
# ---------------------------------------------------------------------------
trace_graphes_2=function(StationsOI, NamesOI, SIM, OBS, sim2, pdfname, graphefile_name, simuname, simuname2){
  if (exists("simuname") & exists("simuname2")){
    NameSim=simuname
    NameSim2=simuname2
  } else {
    NameSim="SIM-REF"
    NameSim2="SIM-test" 
  }
  
  source('lib/aggregateZoo_functions.r')
  library(lubridate)  
  library(xts)
  pdf(paste(graphefile_name,pdfname,sep=''),paper="special",width=12,height=8)
  
  OBSm=aggregateZoo(OBS,'my',mean) 
  SIMm=aggregateZoo(SIM,'my',mean) 
  sim2m=aggregateZoo(sim2,'my',mean)
  
  OBScycle=aggregateZoo(OBS, "m", mean)
  SIMcycle=aggregateZoo(SIM, "m", mean)
  sim2cycle=aggregateZoo(sim2, "m", mean)
  
  for (i in 1:length(StationsOI)){
    
    layout(matrix(c(1:3,4,4,4),3,2))
    par (pty="m") # Paramètre pour avoir un graphe de taille max
    
    # 1. Tracé des débits mensuels
    plot(SIMm[,i],type='l',ylim=c(0,max(SIMm[,i],sim2m[,i],na.omit(OBSm[,i]))),main=NamesOI[i],color=2, xlab='Date',ylab='Q(m3/s)', cex.main=2)
    lines(sim2m[,i],col=6)
    lines(OBSm[,i],col=1)
    
    legend("topleft",legend=c("OBS", NameSim, NameSim2),y.intersp = 1, lty= 1,bty="n",col = c(1,2,6),xpd=NA,cex=0.8)
    
    # 2. Tracé d'une fenêtre de débits journaliers (originale François : 8037:9131 == 2009-01-01:2011-12-31)
    par (pty="m") 
    date1=as.POSIXct("2001-01-01")
    date2=as.POSIXct("2003-12-31")
    
    index1=which(time(OBS)==date1)
    index2=which(time(OBS)==date2)
    plot(SIM[index1:index2,i],type='l',main='Journalier 1987-1990',ylim=c(0,max(SIM[,i],sim2[,i],na.omit(OBS[,i]))),color=2, xlab='Date',ylab='Q(m3/s)', xaxt="n")
    lines(sim2[index1:index2,i],col=6)
    lines(OBS[index1:index2,i],col=1)
    
    legend("topleft",legend=c("OBS", NameSim, NameSim2),y.intersp = 1, lty= 1,bty="n",col = c(1,2,6),xpd=NA,cex=0.8)
    
    axis(1, at=seq(date1,date2, by="month"), labels=month(seq(date1,date2, by="month")))
    mtext(c(year(seq(date1,date2, by="year"))), 1, at=seq(date1,date2, by="year"), line=1.7, cex=0.8)
    
    # 2.2.
    par (pty="m") 
    date1=as.POSIXct("2009-01-01")
    date2=as.POSIXct("2012-12-31")
    
    index1=which(time(OBS)==date1)
    index2=which(time(OBS)==date2)
    plot(SIM[index1:index2,i],type='l',main='Journalier 2009-2012',ylim=c(0,max(SIM[,i],sim2[,i],na.omit(OBS[,i]))),color=2, xlab='Date',ylab='Q(m3/s)', xaxt="n")
    lines(sim2[index1:index2,i],col=6)
    lines(OBS[index1:index2,i],col=1)
    
    legend("topleft",legend=c("OBS",NameSim, NameSim2),y.intersp = 1, lty= 1,bty="n",col = c(1,2,6),xpd=NA,cex=0.8)
    
    axis(1, at=seq(date1,date2, by="month"), labels=month(seq(date1,date2, by="month")))
    mtext(c(year(seq(date1,date2, by="year"))), 1, at=seq(date1,date2, by="year"), line=1.7, cex=0.8)
    
    
    # 3. Tracé du cycle mensuel moyen des débits
    
    MAX<- max(na.omit(OBScycle[,i]),SIMcycle[,i], sim2cycle[,i]) # hauteur max du graphe
    par (pty="m")
    plot(SIMcycle[,i],main = "Moyenne mensuelle interannuelle des débits",ylim=c(0, MAX*1.1),font.lab=2,
         type="l",color=2,xaxt = "n",xlab="Months",ylab="Q(m3/s)")
    lines(sim2cycle[,i], col=6)
    lines(OBScycle[,i], col=1)
    
    ticks<-as.numeric(time(OBScycle))
    labs <- substr(month.abb,1,1)
    Axis(side = 1, at = ticks, labels = labs)
    legend("topleft",legend=c("OBS", NameSim, NameSim2),y.intersp = 1, lty= 1,bty="n",col = c(1,2,6),xpd=NA,cex=0.8)
    #plot.new() #new page
  }
  graphics.off() 
}

# tracegrapheBV NIU
# ----------------------------------------------------------------------------
# CAVEAT : fonctionne uniquement avec 8 BV AEN pr l'instant
# test : tracegrapheBV('~/JAMS/submodels/Arve_loc/output/current/','Arve_loc','~/WORK/AEN/results/graphes/','Arve_global_aasp3.5.pdf')
tracegrapheBV=function(simrep,bv,pdfrep,pdfname){
  source('lib/MDR_trace_graphes_Etienne.r')
  source('lib/MDR_utilitaires.r')
  source("lib/readwrite_functions_J2000.R")
  simu = ReadTimeLoopXTS(simrep,'TimeLoop.dat')
  SIM=treatsimu_exutoire(simu)
  
  if(bv=="Arve_loc"){
    # - Arve à Chamonix Mont Blanc
    StationsOI=c("V0002010")
    NamesOI=c("L Arve a Chamonix-Mont-Blanc [Pont des Favrands]")
  }
  if(bv=="Arvan_Amont_loc"){
    # - Arvant Amont ? Saint-Jean-D'Arves
    StationsOI=c("W1055020")
    NamesOI=c("L Arvan a Saint-Jean-d Arves [La Villette]")
  }
  if(bv=="Durance_Val_Pres_loc"){
    # La Durance e Val-des-Pres [Les Alberts]
    StationsOI=c("X0010010")
    NamesOI=c("La Durance e Val-des-Pres [Les Alberts]")
  }
  if(bv=="Archiane_loc"){
    # - L Archiane ? Treschenu-Creyers [Men?e]
    StationsOI=c("V4226010")
    NamesOI=c("L Archiane ? Treschenu-Creyers [Men?e]")
    meltperiod=seq(1,120)
  }
  if(bv=="Averole_loc"){
    # - L Averole a Bessans [Averole]
    StationsOI=c("W1006010")
    NamesOI=c("L Averole a Bessans [Averole]")
  }
  if(bv=="Issole_loc"){
    # - L Issole ? Saint-Andr?-les-Alpes [Mourefrey]
    StationsOI=c("X2114010")
    NamesOI=c("L Issole ? Saint-Andr?-les-Alpes [Mourefrey]")
    meltperiod=seq(1,120)
  }
  if(bv=="Borne_loc"){
    # - Le Borne ? Saint-Jean-de-Sixt
    StationsOI=c("V0205420")
    NamesOI=c("Le Borne ? Saint-Jean-de-Sixt")
  }
  if(bv=="Doron_loc"){
    # - Le Doron de Bozel ? la Perri?re [Vignotan]
    StationsOI=c("W0224010")
    NamesOI=c("Le Doron de Bozel ? la Perri?re [Vignotan]")
  }
  
  OBS=treatobs(StationsOI)
  trace_graphes(StationsOI, NamesOI, SIM, OBS, pdfname, pdfrep)
}


# Trace Graphe AVANT - APRES NIU
# ----------------------------------------------------------------------------
tracegraphe_avant_apres=function(REF, SIM, OBS, bv, pdfrep,pdfname,colAPRES=NULL){
  if (is.null(colAPRES)){colAPRES='coral'}
  source('lib/aggregateZoo_functions.r')
  library(lubridate)  
  library(xts)
  pdf(paste(pdfrep,pdfname,sep=''),paper="special",width=12,height=8)
  
  OBSm=aggregateZoo(OBS,'my',mean) 
  SIMm=aggregateZoo(SIM,'my',mean) 
  REFm=aggregateZoo(REF,'my',mean) 
  
  OBScycle=aggregateZoo(OBS, "m", mean)
  SIMcycle=aggregateZoo(SIM, "m", mean)
  REFcycle=aggregateZoo(REF, "m", mean)
  
  layout(matrix(c(1,3,4,2,5,5),3,2))
  par (pty="m") # Paramètre pour avoir un graphe de taille max
  
  # 1. débits mensuels
  plot(OBSm,type='l',ylim=c(0,max(na.omit(REFm),na.omit(OBSm))),main=bv, xlab='Date',ylab='Q(m3/s)', cex.main=2)
  lines(REFm,col=2)
  legend("topleft",legend=c("OBS", "SIM Ref"),y.intersp = 1, lty= 1,bty="n",col = c(1,2),xpd=NA,cex=0.8)
  mtext("débits mensuels REF-MDR",3,line=0.2,cex=.7)
  
  plot(OBSm,type='l',ylim=c(0,max(na.omit(REFm),na.omit(OBSm))),main="", xlab='Date',ylab='Q(m3/s)', cex.main=2)
  lines(SIMm,col=colAPRES)
  legend("topleft",legend=c("OBS", "SIM"),y.intersp = 1, lty= 1,bty="n",col = c(1,colAPRES),xpd=NA,cex=0.8)
  mtext("débits mensuels Action NEIGE",3,line=0.2,cex=.7)
  
  # 2. Tracé d'une fenêtre de débits journaliers
  par (pty="m") 
  date1=as.POSIXct("2000-01-01")
  date2=as.POSIXct("2003-12-31")
  
  index1=which(time(OBS)==date1)
  index2=which(time(OBS)==date2)
  
  plot(OBS[index1:index2],type='l',main='',ylim=c(0,max(REF[index1:index2],na.omit(OBS[index1:index2]))), xlab='Date',ylab='Q(m3/s)', xaxt="n")
  lines(REF[index1:index2],col=2)
  legend("topleft",legend=c("OBS", "SIM Ref"),y.intersp = 1, lty= 1,bty="n",col = c(1,2),xpd=NA,cex=0.8)
  axis(1, at=seq(date1,date2, by="month"), labels=month(seq(date1,date2, by="month")))
  mtext(c(year(seq(date1,date2, by="year"))), 1, at=seq(date1,date2, by="year"), line=1.7, cex=0.8)
  mtext("débits journaliers REF-MDR",3,line=0.2,cex=.7)
  
  plot(OBS[index1:index2],type='l',main='',ylim=c(0,max(REF[index1:index2],na.omit(OBS[index1:index2]))), xlab='Date',ylab='Q(m3/s)', xaxt="n")
  lines(SIM[index1:index2],col=colAPRES)
  legend("topleft",legend=c("OBS", "SIM"),y.intersp = 1, lty= 1,bty="n",col = c(1,colAPRES),xpd=NA,cex=0.8)
  axis(1, at=seq(date1,date2, by="month"), labels=month(seq(date1,date2, by="month")))
  mtext(c(year(seq(date1,date2, by="year"))), 1, at=seq(date1,date2, by="year"), line=1.7, cex=0.8)
  mtext("débits journaliers Action Neige",3,line=0.2,cex=.7)
  
  # 3. Cycles
  MAX<- max(na.omit(OBScycle),na.omit(SIMcycle),na.omit(REFcycle)) # hauteur max du graphe
  par (pty="m")
  plot(OBScycle,main = "Moyenne mensuelle interannuelle des débits",ylim=c(0, MAX*1.1),font.lab=2,
       type="l",xaxt = "n",xlab="Months",ylab="Q(m3/s)")
  lines(REFcycle, col=2)
  lines(SIMcycle, col=colAPRES)
  ticks<-as.numeric(time(OBScycle))
  labs <- substr(month.abb,1,1)
  Axis(side = 1, at = ticks, labels = labs)
  legend("topleft",legend=c("OBS", "SIM Ref", "SIM"),y.intersp = 1, lty= 1,bty="n",col = c(1,2,colAPRES),xpd=NA,cex=0.8)
  dev.off()
}

# REGRESSIONS SIM-OBS et OBS-SIM pdt melt
# ----------------------------------------------------------------------------
trace_SIM_OBS_reg_meltperiod=function(OBS,simREF,meltperiod){
  layout(matrix(c(1,2),1,2))
  XX=as.numeric(OBS[which(yday(simREF)%in%meltperiod),])
  YY=as.numeric(simREF[which(yday(simREF)%in%meltperiod),])
  
  plot(XX,YY, xlim=c(0,80),ylim=c(0,80),xlab='OBS',ylab='SIM', main='lin reg SIM vs OBS in meltperiod')
  reg=lm(YY~XX)
  abline(0,1, col='red')
  abline(reg)
  text(15,80, labels=paste('slope=', round(coef(reg)[2],2), '\n intercept=',round(coef(reg)[1],2), sep=""), pos=1)
  
  reg2=lm(XX~YY)
  plot(YY, XX, xlim=c(0,80),ylim=c(0,80),xlab='SIM',ylab='OBS', main='lin reg OBS vs SIM in meltperiod')
  abline(0,1, col='red')
  abline(reg2)
  text(15,80, labels=paste('slope=', round(coef(reg2)[2],2), '\n intercept=',round(coef(reg2)[1],2), sep=""), pos=1)
}


# crit_stats
# ------------------------------------------------------------------
source("lib/readwrite_functions_J2000.R")
Sys.setenv(TZ='UTC')

#KGE classique par jour

KGEday <- function(sim,OBS)
{
  library(hydroGOF)
  KGEd=KGE(as.numeric(sim), as.numeric(OBS))
  KGEd
}

#KGE par sur tous les mois de la p?riode

KGEmonth <- function(sim,OBS)
{
  library(hydroGOF)
  KGEm=KGE(as.numeric(aggregateZoo(sim,'my',mean)),as.numeric(aggregateZoo(OBS,'my',mean)),na.rm=T)
  KGEm
}

#KGE sur le cycle par mois

KGEcycle <- function(sim,OBS)
{
  library(hydroGOF)
  KGEc=KGE(as.numeric(aggregateZoo(sim,'m',mean)),as.numeric(aggregateZoo(OBS,'m',mean)))
  KGEc
}

# KGE seulement sur les jours de fonte unique # jours avec moins de 1 mm de precip lorsqu'il fait plus de 1?C
# 8-03-2017 :: introduction de meltperiod

KGEwithoutRainyDays <- function(sim,OBS,inputfolder,rainfilename,tempfilename,meltperiod)
{
  sim=sim[which(yday(sim)%in%meltperiod)]
  OBS=OBS[which(yday(OBS)%in%meltperiod)]
  Rain=ReadJ2000_Input(inputfolder,rainfilename)
  Temp=ReadJ2000_Input(inputfolder,tempfilename)
  
  Rainmean=rowMeans(Rain)
  Tempmean=rowMeans(Temp)
  
  RainyDays=which((Rainmean>1 & Tempmean>1))
  RainyDaysp1=RainyDays+1
  RainyDaysp2=RainyDays+2
  
  period=index(Rain)
  
  periodwRainyDays=period[-c(RainyDays,RainyDaysp1,RainyDaysp2)]
  
  simwRD=sim[periodwRainyDays,]
  OBSwRD=OBS[periodwRainyDays,]
  
  KGEwRD=KGE(simwRD,OBSwRD)
  KGEwRD
}

# KGE seulement sur les jours de pluie durant la p?riode de fonte # jours avec plus de 1 mm de precip lorsqu'il fait plus de 1?C

KGERainyDays <- function(sim,OBS,inputfolder,rainfilename,tempfilename,meltperiod)
{
  sim=sim[which(yday(sim)%in%meltperiod)]
  OBS=OBS[which(yday(OBS)%in%meltperiod)]
  
  Rain=ReadJ2000_Input(inputfolder,rainfilename)
  Temp=ReadJ2000_Input(inputfolder,tempfilename)
  
  Rainmean=apply(Rain, 1, 'mean')
  Tempmean=apply(Temp, 1, 'mean')
  
  RainyDays=which((Rainmean>1 & Tempmean>1))
  
  period=time(Rain)
  
  periodRainyDays=period[RainyDays]
  simRD=sim[periodRainyDays,]
  OBSRD=OBS[periodRainyDays,]
  
  KGERD=KGE(as.numeric(simRD), as.numeric(OBSRD))
  KGERD
}

# KGE seulement sur les jours de fonte unique # jours avec moins de 1 mm de precip lorsqu'il fait plus de 1?C # avec un d?bit sup?rieur ? 1,5 le d?bit moyen

KGEwithoutRainyDayswithstrongDischarge <- function(sim,OBS,inputfolder,rainfilename,tempfilename,meltperiod)
{
  
  sim=sim[which(yday(sim)%in%meltperiod)]
  OBS=OBS[which(yday(OBS)%in%meltperiod)]
  
  sD=mean(OBS,na.rm=T)*1.5
  
  Rain=ReadJ2000_Input(inputfolder,rainfilename)
  Temp=ReadJ2000_Input(inputfolder,tempfilename)
  
  Rainmean=rowMeans(Rain)
  Tempmean=rowMeans(Temp)
  
  RainyDays=which((Rainmean>1 & Tempmean>1))
  RainyDaysp1=RainyDays+1
  RainyDaysp2=RainyDays+2
  
  period=index(Rain)
  
  periodwRainyDays=period[-c(RainyDays, RainyDaysp1, RainyDaysp2) ]
  simwRD=sim[periodwRainyDays,]
  OBSwRD=OBS[periodwRainyDays,]
  
  simwRDsD=simwRD[which(OBSwRD>sD)]
  OBSwRDsD=OBSwRD[which(OBSwRD>sD)]
  
  KGEwRDsD=KGE(as.numeric(simwRDsD), as.numeric(OBSwRDsD))
  KGEwRDsD
}

# calc_stats
# - inclut KGE_rainy_days donc besoin de rainf et T°C files
# CAVEATS : ne fonctionne que pr 8 BV AEN
# CAVEATS : codage des noms et aires de BV en dur (pr calcul des biais absolu en mm/m2)

calc_stats=function(simREF,OBS,inputfolder,rainfilename,tempfilename, bv, meltperiod, exp=NULL){
  library(hydroGOF)
  mystat=NULL
  NSEd=NSE(simREF,OBS)
  KGEd=KGEday(simREF,OBS)
  KGEm=KGEmonth(simREF,OBS)
  KGEc=KGEcycle(simREF,OBS)
  KGEwRD=KGEwithoutRainyDays(simREF,OBS,inputfolder,rainfilename,tempfilename,meltperiod) # memtpriod arvan ori : 04->07
  KGEwRDsD=KGEwithoutRainyDayswithstrongDischarge(simREF,OBS,inputfolder,rainfilename,tempfilename,meltperiod)
  KGERD=KGERainyDays(simREF,OBS,inputfolder,rainfilename,tempfilename,meltperiod)
  KGEmelt=KGEday(simREF[which(yday(simREF)%in%meltperiod),],OBS[which(yday(OBS)%in%meltperiod),])
  pBiais=pbias(simREF,OBS) # %
  pBiais_melt=pbias(simREF[which(yday(simREF)%in%meltperiod),],OBS[which(yday(OBS)%in%meltperiod),]) # %
  
  # Surfaces des BV en m2
  
  area=c(192280000,60840000,200360000,36560000,38720000,13932000,73120000,340640000)
  names(area)=c("Arve_loc","Arvan_Amont_loc","Durance_Val_Pres_loc","Archiane_loc","Averole_loc","Issole_loc","Borne_loc","Doron_loc")
  
  #aBiais=(mean(simREF)-mean(OBS,na.rm=T))*3600*24*365.25/as.numeric(area[bv])*1000 # m/yr...-> IG : mm/yr. 7/11/2016
  # AU SECOURS !!! Maxime n'avait pas décompté les missing-val dans ses sim...
  aBiais=me(simREF,OBS)*3600*24*365.25/as.numeric(area[bv])*1000 # mm(/m2)/yr
  aBiais_melt=me(simREF[which(yday(simREF)%in%meltperiod),],OBS[which(yday(OBS)%in%meltperiod),])*3600*24*length(meltperiod)/as.numeric(area[bv])*1000 # mm(/m2)/yr
  
  KGEmoy=mean(c(KGEwRD,KGEwRDsD))
  
  pdischarge_meltperiod_sim=mean(simREF[which(yday(simREF)%in%meltperiod),])*length(meltperiod)/mean(simREF)/365.25*100 # %
  
  mystat=rbind(mystat,c(KGEd,KGEm,KGEc,KGEwRD,KGEwRDsD,KGERD,KGEmelt,pBiais,aBiais,pBiais_melt, aBiais_melt,pdischarge_meltperiod_sim, KGEmoy,NSEd))
  
  colnames(mystat)=c("KGEd","KGEm","KGEc","KGEwRD","KGEwRDsD","KGERD","KGEmelt","pBiais","aBiais","pBiais_melt","aBiais_melt","pdischarge_meltperiod_sim","KGEmoy",'NSEd')
  rownames(mystat)=paste(bv,exp,sep='') 
  mystat
}

# calc_stats_simple
# CAVEATS : ne fonctionne que pr 8 BV AEN
calc_stats_simple=function(simREF,OBS,bv, meltperiod, exp=NULL){
  library(hydroGOF)
  mystat=NULL
  NSEd=NSE(simREF,OBS)
  KGEd=KGEday(simREF,OBS)
  KGEm=KGEmonth(simREF,OBS)
  KGEc=KGEcycle(simREF,OBS)
  
  KGEmelt=KGEday(simREF[which(yday(simREF)%in%meltperiod),],OBS[which(yday(OBS)%in%meltperiod),])
  pBiais=pbias(simREF,OBS) # %
  pBiais_melt=pbias(simREF[which(yday(simREF)%in%meltperiod),],OBS[which(yday(OBS)%in%meltperiod),]) # %
  
  pdischarge_meltperiod_sim=mean(simREF[which(yday(simREF)%in%meltperiod),])*length(meltperiod)/mean(simREF)/365.25*100 # %
  
  mystat=rbind(mystat,c(KGEd,KGEm,KGEc,KGEmelt,pBiais,pBiais_melt,pdischarge_meltperiod_sim,NSEd))
  
  colnames(mystat)=c("KGEd","KGEm","KGEc","KGEmelt","pBiais","pBiais_melt","pdischarge_meltperiod_sim",'NSEd')
  rownames(mystat)=paste(bv,exp,sep='') 
  mystat
}

# compute_critstat sur liste de BV et d'expériences
# --------------------------------------------------------------
# - tous les bassins doivent avoir les expériences mentionnées.
# - inclut KGE_rainy_days donc besoin de rainf et T°C files
# CAVEATS : ne fonctionne que pr 8 BV AEN
# CAVEATS : codage des noms et aires de BV en dur (pr calcul des biais absolu en mm/m2)
#
#   Critères : "KGEd","KGEm","KGEc","KGEwRD","KGEwRDsD","KGERD","KGEmelt","pBiais","aBiais","pBiais_melt","aBiais_melt","pdischarge_meltperiod_sim","KGEmoy"
#
#   shortnames qualifie 'exp' (ex : "SPZ_mail"); il permet de donner un nom plus court que 'exp' pour les légendes...
compute_critstat=function(statfile,experiencelist,pptlist,tmeanlist, bvlist, shortnames=NULL){
  
  source("lib/MDR_utilitaires.r")
  source("lib/readwrite_functions_J2000.R")
  Sys.setenv(TZ='UTC')
  library(zoo)
  library(chron)
  library(hydroGOF)
  library(maptools)
  library(gdata)
  library(lubridate)
  library(fmsb)
  
  
  stat=NULL
  myrownames=NULL
  for (bv in bvlist ){
    for (exp in experiencelist){
      i=which(experiencelist==exp)
      ppt=pptlist[i]
      tt=tmeanlist[i]
      
      inputfolder=paste('~/JAMS/submodels/',bv,'/input/local/',sep='')
      rainfilename=paste('rain_',ppt,'.dat',sep='')
      tempfilename=paste('tmean_',tt,'.dat',sep='')
      
      simufile=paste('~/JAMS/submodels/',bv,'/output/',exp,'/',sep='')
      filename='TimeLoop.dat'
      meltperiod=seq(91,213) # DOY, from R2D2 : 1st april - 31 july. 
      # Original try : encompassing ~ half the year.seq(15,198)
      # Exceptions : Issole and Archiane (1st jan -> 1st May)
      
      if(bv=="Arve_loc"){
        # - Arve à Chamonix Mont Blanc
        StationsOI=c("V0002010")
        NamesOI=c("L Arve a Chamonix-Mont-Blanc [Pont des Favrands]")
      }
      if(bv=="Arvan_Amont_loc"){
        # - Arvant Amont ? Saint-Jean-D'Arves
        StationsOI=c("W1055020")
        NamesOI=c("L Arvan a Saint-Jean-d Arves [La Villette]")
      }
      if(bv=="Durance_Val_Pres_loc"){
        # La Durance e Val-des-Pres [Les Alberts]
        StationsOI=c("X0010010")
        NamesOI=c("La Durance e Val-des-Pres [Les Alberts]")
      }
      if(bv=="Archiane_loc"){
        # - L Archiane ? Treschenu-Creyers [Men?e]
        StationsOI=c("V4226010")
        NamesOI=c("L Archiane ? Treschenu-Creyers [Men?e]")
        meltperiod=seq(1,120)
      }
      if(bv=="Averole_loc"){
        # - L Averole a Bessans [Averole]
        StationsOI=c("W1006010")
        NamesOI=c("L Averole a Bessans [Averole]")
      }
      if(bv=="Issole_loc"){
        # - L Issole ? Saint-Andr?-les-Alpes [Mourefrey]
        StationsOI=c("X2114010")
        NamesOI=c("L Issole ? Saint-Andr?-les-Alpes [Mourefrey]")
        meltperiod=seq(1,120)
      }
      if(bv=="Borne_loc"){
        # - Le Borne ? Saint-Jean-de-Sixt
        StationsOI=c("V0205420")
        NamesOI=c("Le Borne ? Saint-Jean-de-Sixt")
      }
      if(bv=="Doron_loc"){
        # - Le Doron de Bozel ? la Perri?re [Vignotan]
        StationsOI=c("W0224010")
        NamesOI=c("Le Doron de Bozel ? la Perri?re [Vignotan]")
      }
      deb = '1987-01-01'
      fin = '2012-12-31'
      OBS=treatobs(StationsOI)
      REF=ReadTimeLoopXTS(simufile,filename)       
      simREF=treatsimu_exutoire(REF)
      
      mystat=calc_stats(simREF,OBS,inputfolder,rainfilename,tempfilename, bv,meltperiod, shortnames[i])
      tf=as.numeric(system(paste('grep t_factor ~/JAMS/submodels/',bv,'/output/',exp,'/model.jmp | cut -d"=" -f2',sep=""),intern=TRUE))
      
      a_asp=as.numeric(system(paste('grep a_asp ~/JAMS/submodels/',bv,'/output/',exp,'/model.jmp | cut -d"=" -f2',sep=""),intern=TRUE))
      if (length(a_asp)==0) {a_asp=0}
      
      Amp=as.numeric(system(paste('grep Amplitude ~/JAMS/submodels/',bv,'/output/',exp,'/model.jmp | cut -d"=" -f2',sep=""),intern=TRUE))
      if (length(Amp)==0) {Amp=0}
      
      mystat=cbind(mystat, tf, a_asp, Amp)
      
      stat=rbind(stat,mystat)
    }
  }
  write.csv(stat,statfile)
  return(stat)
}

# radarcharts
# ---------------------------------------------------------------------------
plot_radarchart=function(stat, expnames=NULL, legend=NULL, title=NULL){ # stat matrix, rows=exp with names, col=statcriteria, legend=boolean: Y or N
  radarcrit=cbind(1-abs(stat[,"pBiais"]/100.), stat[,"KGEd"], stat[,"KGEm"], stat[,"KGEc"], stat[,"KGEwRD"], stat[,"KGEwRDsD"], stat[,"KGERD"], stat[,"KGEmelt"], 1-abs(stat[,"pBiais_melt"]/100.))
  radarcrit=apply(radarcrit,c(1,2), function(x) if(x<=0) 0 else x)
  colnames(radarcrit)=c('Bilan', 'KGEd', 'KGEm', 'KGEc', "KGEwRD","KGEwRDsD","KGERD","KGEmelt", 'Bilan Fonte')
  rownames(radarcrit)=rownames(stat)
  radarcrit=as.data.frame(radarcrit)
  radarcrit=rbind(rep(1,8),rep(0,8), radarcrit)
  
  if(!is.null(legend)){layout(matrix(c(1:2),1,2))}
  
  if(dim(stat)[1]==3){
    colors_border=c( rgb(0.7,0.5,0.1,0.9), rgb(0.8,0.2,0.5,0.9) ,  rgb(0.2,0.5,0.5,0.9))
    colors_in=c( rgb(0.7,0.5,0.1,0.4), rgb(0.8,0.2,0.5,0.4) , rgb(0.2,0.5,0.5,0.4) )
    colors_border=c(rgb(0.2,0.5,0.5,0.9), 'deepskyblue', 'blue')
    colors_in=c(rgb(1,1,1,0),rgb(1,1,1,0),rgb(1,1,1,0))
    radarchart(radarcrit, plwd=1, plty=1, cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,1,0.25), cglwd=0.8, axistype=1, cex=0.3, calcex=.6, pcol=colors_border, pfcol=colors_in, vlcex=.7, title=title)
    if(!is.null(legend)){
      plot.new()
      if(!is.null(expnames)){
        legend(x=0.1, y=0.7, legend = expnames, bty = "n", pch=20 , col=colors_border, cex=1, pt.cex=4)} else{
          legend(x=0.1, y=0.7, legend = rownames(stat), bty = "n", pch=20 , col=colors_border, cex=1, pt.cex=4)
        }
    }
  } else {
    
    radarchart(radarcrit, plwd=1, plty=1, cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,1,0.25), cglwd=0.8, axistype=1, cex=0.5, calcex=.6, vlcex=.7,title=title)
    plot.new()
    if(!is.null(expnames)){
      legend(x=0.1, y=0.7, legend = expnames, bty = "n", pch=20 , col=c(1:dim(stat)[1]), cex=1, pt.cex=4)} else{
        legend(x=0.1, y=0.7, legend = rownames(stat), bty = "n", pch=20 , col=c(1:dim(stat)[1]), cex=1., pt.cex=4)
      }
  }
}

# ceux du rapport AEN
plot_radarchart_simple=function(stat, expnames=NULL, legend=NULL, title=NULL){ # stat matrix, rows=exp with names, col=statcriteria, legend=boolean: Y or N
  radarcrit=cbind(1-abs(stat[,"pBiais"]/100.), stat[,"KGEd"], stat[,"KGEm"], stat[,"KGEc"], stat[,"KGEmelt"], 1-abs(stat[,"pBiais_melt"]/100.))
  radarcrit=apply(radarcrit,c(1,2), function(x) if(x<=0) 0 else x)
  colnames(radarcrit)=c('Bilan', 'KGEd', 'KGEm', 'KGEc',"KGEmelt", 'Bilan Fonte')
  rownames(radarcrit)=rownames(stat)
  radarcrit=as.data.frame(radarcrit)
  radarcrit=rbind(rep(1,8),rep(0,8), radarcrit)
  
  #if(!is.null(legend)){layout(matrix(c(1:2),1,2))}
  
  if(dim(stat)[1]==3){
    colors_border=c( rgb(0.7,0.5,0.1,0.9), rgb(0.8,0.2,0.5,0.9) ,  rgb(0.2,0.5,0.5,0.9))
    colors_in=c( rgb(0.7,0.5,0.1,0.4), rgb(0.8,0.2,0.5,0.4) , rgb(0.2,0.5,0.5,0.4) )
    #colors_border=c(rgb(0.2,0.5,0.5,0.9), 'deepskyblue', 'blue')
    #colors_in=c(rgb(1,1,1,0),rgb(1,1,1,0),rgb(1,1,1,0))
    radarchart(radarcrit, plwd=2, plty=1, cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,1,0.25), cglwd=2, axistype=1, cex=3, calcex=.6, pcol=colors_border, pfcol=colors_in, vlcex=1.1, title=title, cex.title=2)
    # if(!is.null(legend)){
    #   plot.new()
    #   if(!is.null(expnames)){
    #     legend(x=0.1, y=0.7, legend = expnames, bty = "n", pch=20 , col=colors_border, cex=1, pt.cex=4)} else{
    #       legend(x=0.1, y=0.7, legend = rownames(stat), bty = "n", pch=20 , col=colors_border, cex=1, pt.cex=4)
    #     }
    # }
  } else {
    
    radarchart(radarcrit, plwd=1, plty=1, cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,1,0.25), cglwd=1.2, axistype=1, cex=3, calcex=.6, vlcex=1.1,title=title)
    #plot.new()
    # if(!is.null(expnames)){
    #   legend(x=0.1, y=0.7, legend = expnames, bty = "n", pch=20 , col=c(1:dim(stat)[1]), cex=1, pt.cex=4)} else{
    #     legend(x=0.1, y=0.7, legend = rownames(stat), bty = "n", pch=20 , col=c(1:dim(stat)[1]), cex=1., pt.cex=4)
    #   }
  }
}



# write_dbf_performancefile
#~-------------------------------------------------------------------------------
# Ecrit le .dbf des NSE et MB (Mean Bias) du modèle aux stations MDR retenues
# qui sont dans ~/DATA/SIG_MDR/Perf_modele/Stations_Rhone_retenues_Sim_Ref.dbf
#~-------------------------------------------------------------------------------
write_dbf_performancefile=function(StationsOI, simREF, OBS, performancefile){
  library(foreign)
  
  dbf=read.dbf('~/DATA/SIG_MDR/Perf_modele/Stations_Rhone_retenues_Sim_Ref.dbf')
  
  NSE=NULL
  MB=NULL
  for (i in seq(1,length(StationsOI),1)){
      NSE_tmp=Nash(simREF[,i], OBS[,i])
      MB_tmp=Bias(simREF[,i], OBS[,i])
      NSE=c(NSE,NSE_tmp)
      MB=c(MB,MB_tmp)
  }
  myorder=match(dbf$CODE,StationsOI)
  mydbf=data.frame(dbf$CODE,dbf$NOM, dbf$S_BH, NSE[myorder], MB[myorder])
  names(mydbf)=c('CODE','NOM','S_BH','NSE','MB')
  write.dbf(mydbf,paste(performancefile, '.dbf', sep=""))
  
  for (ext in c('.prj', '.qml', '.shp', '.qpj', '.shp', '.shx')){
    system(paste('cp ~/DATA/SIG_MDR/Perf_modele/Stations_Rhone_retenues_Sim_Ref', ext,' ',performancefile,ext, sep=""))
    
  }

}


#####################################################################################
#           CHECK BILAN HYDRO J2000 
#####################################################################################

#~* PROGRAMMER: Isabelle GOUTTEVIN (Irstea Lyon) 
#~******************************************************************************
#~* CREATED/MODIFIED: Created 10-02-2016
#~******************************************************************************


# fonctionnementHydro
# -----------------------------------------------------------------------------
#~* IN : - sim (le produit d'un ReadTimeLoopDaily)
#~*      - StationsOI (une seule)
#~*      - Reaches (un seul)
#~*
#~* OUT : - variables (zoo object, cycle mensuel) pr comprendre le fonctionnement hydro du bassin
#~*                   =c('precip','actET','snowTotSWE','reachOutRD1','reachOutRG1','reachOutRG1') 
#~*                   =[varOI_h1, ..., varOI_r3]
#~*                    t1              t1
#~*                   ...             ...
#~*                   t12             t12
#~*         units     =(mm/d, mm/d, mm/d, mm, mm/d, mm/d, mm/d)
# -----------------------------------------------------------------------------
# définition des variables d'intérêt
# -h : sur hru ou bassin
# -r : sur reach
fonctionnementHydro=function(sim,StationsOI,Reaches){
  varOI_h=c('precip','actET','snowTotSWE') # hrus (mm,mm,mm)
  varOI_r=c('reachOutRD1','reachOutRD2','reachOutRG1', 'simRunoff') # reaches (L/d, L/d, L/d)
  coeff_normalisation=c(1,1,1,1./Area, 1./Area,1./Area, 1./Area ) # (all : mm/d or mm)
  
  # cycle annuel sur ces variables
  #----------------------------------- 
  # - result : variables=[varOI_h1, ..., varOI_r3]
  #                       t1              t1
  #                       ...             ...
  #                       t12             t12
  variables=zoo(NULL)
  for (var in varOI_h){
    vareff=paste(StationsOI,var,sep="")
    indexe=match(vareff,names(sim))
    chronique=sim[,indexe]
    annualcycle=aggregateZoo(chronique, "m", mean)
    variables=merge(variables,annualcycle)
  }
  for (var in varOI_r){
    vareff=paste(var,'_',Reaches, sep="")
    indexe=match(vareff,names(sim))
    chronique=sim[,indexe]
    annualcycle=aggregateZoo(chronique, "m", mean)
    variables=merge(variables,annualcycle)
  }
  
  # normalisation
  for (i in (1:dim(variables)[2])){variables[,i]=variables[,i]*coeff_normalisation[i]}
  
  names(variables)=c(varOI_h,varOI_r)
  
  return(variables)
}

# checkBilanHRU
# ------------------------------------------------------------------------------
#~* vérifie le bilan hydrologique d'une HRU
#~*
#~* IN :  - hruloopdir: '/home/..../' where HRULoop.dat is located
#~*       - numhru: numéro de la HRU dont on veut le bilan
#~*
#~* OUT : (in)-(out) [mm/m2/yr]. si >0 : le bassin "crée" de l'eau (ou n'en évacue pas assez)
#~*                              si <0 : le bassin perd de l'eau
#~*
#~* CAVEAT1 : valable seulement en hydrologie naturelle
#~* CAVEAT2 : valable entre une fin et un début de simu où les réservoirs (sol, SWE) ont été initialisés à 0.
#~* CAVEAT3 (à vérifier) : que se passe-t-il si un reach traverse la HRU ?
# ------------------------------------------------------------------------------
checkBilanHRU=function(hruloopdir, numhru){
  
  hruloop=ReadLoopDaily(hruloopdir,'HRULoop.dat', FALSE)
  myhru=hruloop$Data[which(hruloop$Data[,1]==numhru),]
  myhru=xts(myhru[, c('outRD1','precip','snowTotSWE','actLPS','actMPS', 'actRG1', 'actRG2','actET','outRD2','outRG1','outRG2')],as.POSIXct(hruloop$dates,format='$Y-%m-%d'))
  myarea=unique(hruloop$Data[which(hruloop$Data[,1]==numhru),'area'])
  N=length(myhru[,1])
  
  IN=sum(myhru$precip)
  OUT=sum(myhru$actET+myhru$outRD1+myhru$outRD2+myhru$outRG1)/myarea
  deltaSWE=myhru$snowTotSWE[N]/myarea
  res_soil=(myhru$actRG1+myhru$actRG2+myhru$actMPS+myhru$actLPS)/myarea
  deltaSoilRes=res_soil[N]
  
  Bilan=IN-OUT-deltaSWE-deltaSoilRes  #(mm)
  return(as.numeric(Bilan))
}

# checkBilanSubmodel 
# ------------------------------------------------------------------------
checkBilanSubmodel=function(dir){
  
  tloop=ReadTimeLoopIG(dir,'TimeLoop.dat') # 24 variables
  hruloop=ReadLoopDaily(dir,'HRULoop.dat', FALSE)
  
  ids=unique(hruloop$Data[,'ID'])
  N=length(ids)
  areas=hruloop$Data[,'area'][1:N]
  myarea=sum(areas) # m2
  
  window='1990-01-01/2012-12-31' # 5 year-spin-up
  date1='1990-01-01'
  date2='2012-12-31'
  
  var_station=c('precip','actET','actMPS','actLPS','actRG1','intercStorage','snowTotSWE')
  var_reach=c('reachStation', 'channelStorage')
  
  for (var in var_station){
    assign(var,tloop[window,var])
  }
  for (var in var_reach){
    assign(var,tloop[window,var]/myarea)  # transfo en mm/m2
  }
  
  Storage=actMPS+actLPS+actRG1+channelStorage+intercStorage+snowTotSWE # mm/m2
  names(Storage)='Storage'
  DeltaStorage=as.numeric(Storage[date2])-as.numeric(Storage[date1]) # RQ : pas de Storage[1] car c'est déja le t-step1 ET les Storage sont nuls au début de la simu
  
  Err_Bilan=sum(precip)-sum(reachStation)-sum(actET)-DeltaStorage #mm/m2
  
  out=c(sum(precip),sum(actET),sum(reachStation),DeltaStorage, Err_Bilan ,actMPS[date2],actLPS[date2],actRG1[date2],channelStorage[date2],intercStorage[date2],snowTotSWE[date2])
  names(out)=c('P (mm/m2)','actET (mm/m2)','Q(mm/m2)', 'DeltaStorage(mm/m2)', 'Err_Bilan (mm/m2)', 'actMPS', 'actLPS','actRG1','channelStorage','intercStorage','snowTotSWE')
  
  Nbyears=year(date2)-year(date1)+1
  out=out/Nbyears
  
  return(out)
}

# checkBilanLoop (hruLoop) 
# -------------------------------------------------------------------------
checkBilanLoop=function(dir, station, brin, hrusinloop){
  
  tloop=ReadTimeLoopDaily(dir,'TimeLoop.dat') # 24 variables
  hruloop=ReadLoopDaily(dir,'HRULoop.dat', FALSE)
  
  hruloop1=hruloop$Data[which(hruloop$Data[,'ID']%in%hrusinloop),]
  hruloop2=unique(hruloop1)
  myarea=sum(hruloop2[,'area']) 
  
  var_station=c('precip','actET','actMPS','actLPS','actRG1','intercStorage','snowTotSWE')
  var_reach=c('simRunoff', 'channelStorage')
  
  for (var in var_station){
    varname=paste(station,var,sep="")
    assign(var,tloop[,varname])
  }
  for (var in var_reach){
    varname=paste(var,'_',brin,sep="")
    assign(var,tloop[,varname]/myarea)  # transfo en mm/m2
  }
  
  tend=dim(tloop)[1] #-1 temporaire
  Storage=actMPS+actLPS+actRG1+channelStorage+intercStorage+snowTotSWE # mm/m2
  names(Storage)='Storage'
  DeltaStorage=as.numeric(Storage[tend]) # RQ : pas de Storage[1] car c'est déja le t-step1 ET les Storage sont nuls au début de la simu
  
  Err_Bilan=sum(precip)-sum(simRunoff)-sum(actET)-DeltaStorage #mm/m2
  
  out=c(sum(precip),sum(actET),sum(simRunoff),DeltaStorage, Err_Bilan ,actMPS[tend],actLPS[tend],actRG1[tend],channelStorage[tend],intercStorage[tend],snowTotSWE[tend])
  names(out)=c('P (mm/m2)','actET (mm/m2)','Q(mm/m2)', 'DeltaStorage(mm/m2)', 'Err_Bilan (mm/m2)', 'actMPS', 'actLPS','actRG1','channelStorage','intercStorage','snowTotSWE')
  
  return(out)
}

#####################################################################################
#           MODIF de FICHIERS de PARAM' de J2000
#####################################################################################

# écriture d'un fichier de params modifié en utilisant le header de l'ancien -- NTC
# ----------------------------------------------------------------
write_new_paramfile=function(oldfile, newvalues ,newfile){
  # récupération du header
  nbLines = Lignes_saut1(oldfile)  
  header = readLines(oldfile, n = nbLines)
  # écriture
  write.table(header,newfile,sep='\t',col.names=F,row.names=F,quote=F)
  write.table(newvalues,newfile,col.names=F,row.names=F,quote=F,append=T,sep='\t')
}


# Nombre de lignes du header d'un fichier de paramètre de J2000
# ----------------------------------------------------------------
Lignes_saut1 = function(file){
  k <- 0
  obj <- NULL; obj2 <- NULL; obj3 <- NULL
  while (length(na.omit(obj)) == 0 | length(na.omit(obj2)) == 0 | length(na.omit(obj3)) == 0) {
    
    obj <- as.numeric(read.table(file, nrow = 1, skip = k, colClasses = "character"))[1]
    obj2 <- as.numeric(read.table(file, nrow = 1, skip = k + 1, colClasses = "character"))[1]
    obj3 <- as.numeric(read.table(file, nrow = 1, skip = k + 2, colClasses = "character"))[1]
    
    k <- k + 1
  }
  return(k - 1)
}


# **** ajout d'un paramètre supplémentaire au reach.par
# ----------------------------------------------------------------
# -- NTK
add_param = function(inputdir,oldreachfile,newreachfile,newparamName,newparamVal,newparamUnit){

  nbLines = Lignes_saut(inputdir,oldreachfile)
  headerReach = readLines(paste(inputdir,oldreachfile,sep=''), n = nbLines)
  LinesNames = which(substr(headerReach,1,2)=="ID")
  Names = read.table(paste(inputdir,oldreachfile,sep=''),nr=1,skip=LinesNames-1)
  Names = cbind(Names,newparamName)
  Min = read.table(paste(inputdir,oldreachfile,sep=''),nr=1,skip=LinesNames)
  Min = cbind(Min,0,0)
  Max = read.table(paste(inputdir,oldreachfile,sep=''),nr=1,skip=LinesNames+1)
  Max = cbind(Max,9999999,9999999)
  Unit =  read.table(paste(inputdir,oldreachfile,sep=''),nr=1,skip=LinesNames+2)   
  Unit = cbind(Unit,newparamUnit)
  reach=Chargement_param(inputdir,oldreachfile)
  reach = cbind(reach,newparamVal)
  
  write.table (Names,paste(inputdir,newreachfile,sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=F)
  write.table (Min,paste(inputdir,newreachfile,sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
  write.table (Max,paste(inputdir,newreachfile,sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
  write.table (Unit,paste(inputdir,newreachfile,sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
  write.table (reach,paste(inputdir,newreachfile,sep=''),col.names=F, row.names=F,quote=F,sep='\t',append=T)
}

# chargement d'un fichier hru.par ou reach.par
# ------------------------------------------------------------
#Inputs :
# - chemin : le dossier dans lequel se situe le fichier desire
# - Name : le nom du fichier desire
#Output : les paramètres (tableau) avec les bons noms de colonne
#
# Astuce :
# - Le code est capable d'identifier la ligne avec les premieres valeurs et de sauter les lignes de texte initiales
# Caveats : ça risque de ne plus marcher pour des fichiers avec moins de 3 lignes de données.
# ----------------------------------------------------------
# -- NTK
Chargement_param = function(chemin,Name){
  k=0
  obj = NULL
  obj2 = NULL
  obj3 = NULL
  while(length(na.omit(obj))==0 | length(na.omit(obj2))==0 | length(na.omit(obj3))==0){
    obj = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k,colClasses="character"))[1]
    obj2 = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k+1,colClasses="character"))[1]
    obj3 = as.numeric(read.table(paste(chemin,Name,sep=''),nrow=1,skip=k+2,colClasses="character"))[1]
    k=k+1}
  nbLines = k - 1
  data=read.table(paste(chemin,Name,sep=''),skip=nbLines)
  mycolnames=apply(read.table(paste(chemin,Name,sep=''),nrow=1)[1,],1,as.character)
  colnames(data)=mycolnames
  return(data)
}


#####################################################################################
#           IRRIGATION / AGRICULTURE
#####################################################################################

# LU id -> culture
# ------------------- NTC
# vecteur des cultures dominantes à partir du vecteur des landuseID
luid2cult=function(vect_luid){
  cultures=c('Vigne', 'Mais', 'Tournesol', 'Blé dur', 'Maraichage', 'PdT', 'Vergers', 'Prairies', 'Protéagineux', 'Riz', "Jachère","Divers", "Industrielles")
  numJ2000_cultures=c(19:31)
  res=apply(as.matrix(vect_luid),2,function(X){cultures[match(X,numJ2000_cultures)]})
  as.vector(res)
}

# Culture -> LU id
# -------------------
# l'inverse : vecteur LUID dominant à partir du vecteur des cultures
cult2luid=function(vect_cult){
  cultures=c('Vigne', 'Mais', 'Tournesol', 'Blé dur', 'Maraichage', 'PdT', 'Vergers', 'Prairies', 'Protéagineux', 'Riz', "Jachère","Divers", "Industrielles")
  numJ2000_cultures=c(19:31)
  res=apply(as.matrix(vect_cult),2,function(X){numJ2000_cultures[match(X,cultures)]})
  as.vector(res)
}