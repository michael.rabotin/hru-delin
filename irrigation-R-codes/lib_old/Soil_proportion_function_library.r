Soil_proportion_function <- function(param_folder,Catchment,Name_subCatch,watershed,rewrite){

############################################################################
#  param_folder : folder where the parameter files are
#
#   parameter files (hgeo, landuse and soils) need 2 columns : one with the index used in hrus.par and a second with the description of the parameter (format : parameter.csv with sep =";") !
#  Put the hrus.par file too in yout folder!
#
#  Catchment : Name of the catchment
#  Name_subCatch : if you have sub-catchment, name of one sub-catchment, else NULL
#  watershed : watershed corresponding to the sub-catchment if you have one, else NULL
#  rewrite : if TRUE, the code is creating a new file
############################################################################
                        
                                 
#Read landuse description
param_landuse <- read.table(paste(param_folder,'landuse.csv',sep=''),sep=';')
#Read soils description
param_soil <- read.table(paste(param_folder,'soils.csv',sep=''),sep=';')
#Read hgeo description
param_hgeo <- read.table(paste(param_folder,'hgeo.csv',sep=''),sep=';')

#Read HRU.par
hrus2 <- read.table(paste(param_folder,'hrus.par',sep=''),col.names=read.table(paste(param_folder,'hrus.par',sep=''),skip=1,nr=1,colClasses="character"),skip=5)


if (length(Name_subCatch) != 0){
if (length(watershed) != 0){
#Extraction of the hrus of the sub-catchment
hrus <- NULL
NbWatershed <- length(watershed)
nb <- 0
while(nb != NbWatershed){
nb <- nb + 1
hrus <- rbind(hrus,hrus2[(hrus2$watershed == watershed[nb]),])
}
}
} else {hrus <- hrus2}


#Rewrite the file if necessary
if(rewrite){write.table(NULL,paste(param_folder,"Proportion_",Catchment,".csv",sep=""),col.names=F,row.names=F,quote=F,sep=';',append=F)}


slope <- sum(hrus[,2]*hrus[,4])/sum(hrus[2]*1)

hgeo <- (hrus$hgeoID)
hgeo2 <- NULL
for (i in c (1:length(unique (hgeo)))){
hgeo3 <- NULL
hgeo3 <- c(as.character(param_hgeo[param_hgeo[,1]==unique(hgeo)[i],2]),round(sum((hrus[hrus$hgeoID == unique(hgeo)[i],2])*1)/sum(hrus[,2]*1),2))
hgeo2 <- rbind(hgeo2,hgeo3)
}
hgeo2 <- hgeo2[order(as.numeric(hgeo2[,2]),decreasing=TRUE),]





soil <- (hrus$soilID)
soil2 <- NULL
for (i in c (1:length(unique (soil)))){
soil3 <- NULL
soil3 <- c(as.character(param_soil[param_soil[,1]==unique(soil)[i],2]),round(sum((hrus[hrus$soilID == unique(soil)[i],2])*1)/sum(hrus[,2]*1),2)) 
soil2 <- rbind(soil2,soil3)
}
soil2 <- soil2[order(as.numeric(soil2[,2]),decreasing=TRUE),]


landuse <- (hrus$landuseID)
landuse2 <- NULL
for (i in c (1:length(unique (landuse)))){
landuse3 <- NULL
landuse3 <- c(as.character(param_landuse[param_landuse[,1]==unique(landuse)[i],2]),round(sum((hrus[hrus$landuseID == unique(landuse)[i],2])*1)/sum(hrus[,2]*1),2))
landuse2 <- rbind(landuse2,landuse3)
}
landuse2 <- landuse2[order(as.numeric(landuse2[,2]),decreasing=TRUE),]

Recap <- cbind(rbind(cbind('hgeo',if(length(dim(hgeo2))==0){t(hgeo2)}else{hgeo2}),cbind('soil',if(length(dim(soil2))==0){t(soil2)}else{soil2}),cbind('landuse',if(length(dim(landuse2))==0){t(landuse2)}else{landuse2})))
colnames (Recap) <- c('param','value','Proportion')

#Write the name of the sub-catchment
write.table(rbind("---------------------------------------------------------------------",ifelse(length(Name_subCatch)!=0,Name_subCatch,Catchment),""),paste(param_folder,"Proportion_",Catchment,".csv",sep=""),col.names=F,row.names=F,quote=F,sep='\t',append=T)


write.table(Recap,paste(param_folder,"Proportion_",Catchment,".csv",sep=""),row.names=F,col.names=T,quote=F,sep=';',dec='.',append=T)

# Add the catchment area in the file
Area_Catchment <- sum(as.numeric(hrus$area))
write.table(rbind("",paste("Catchment area : ", round(Area_Catchment/1000000,0)," km2",sep="")),paste(param_folder,"Proportion_",Catchment,".csv",sep=""),col.names=F,row.names=F,quote=F,sep=';',append=T)

}

