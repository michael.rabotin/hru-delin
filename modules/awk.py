#!/usr/bin/env python3
# -*- coding: utf-8 -*-

############################################################################
#
# MODULE:       awk.py
# AUTHOR(S):    Julien Veyssier
# 
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################




# this file contains the converted old awk scripts which were not portable (to windows)

from collections import defaultdict
import os, time

# Format table N:M
# it was the uggliest code ever
def formatNm(bas, topoToHruTmpPath, topoBflTmpPath, stats):
    statDict = defaultdict(list)
    with open(topoToHruTmpPath, 'a') as ttohru:
        with open(topoBflTmpPath, 'a') as tbfl:
            # group stats by second column value
            for stat in stats:
                statSpl = stat.strip().split()
                if len(statSpl) > 2:
                    second = statSpl[2]
                    statDict[second].append(statSpl)

            # one value = one result line with accumulated values
            for sec in statDict:
                acc_sum = 0.0
                ids = []
                weight = []
                for statSpl in statDict[sec]:
                    ids.append(statSpl[4].strip(';'))
                    size = statSpl[9]
                    weight.append(float(statSpl[7]))
                    acc_sum += float(statSpl[7])

                ttohru.write('%s\t%s\t%s\t' % (sec[:-1], size, bas))
                ttohru.write(','.join(ids))
                ttohru.write(os.linesep)

                tbfl.write('%s\t%s\t' % (sec[:-1], size))
                ws = ['%.4f' % (w / acc_sum) for w in weight]
                tbfl.write(','.join(ws))
                tbfl.write(os.linesep)


# Find max rate in N:M (+ associated id)
def findMax(tToHruPath, tbflPath, outPath):
    strTime = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    with open(tToHruPath, 'r') as tth:
        with open(tbflPath, 'r') as bfl:
            with open(outPath, 'w') as out:
                out.write('# topologie_n1_tmp.par created %s by hru-delin-nextgen\n' % strTime)
                out.write('# ID,-SUBBASIN,TO_HRU\n')
                for bfll in bfl.readlines():
                    if bfll.startswith('#'):
                        continue
                    idx = 0
                    max = 0.0
                    rates = bfll.split()[2].strip(',').split(',')
                    for i, r in enumerate(rates):
                        if float(r) > max:
                            max = float(r)
                            idx = i

                    ttohl = tth.readline()
                    while ttohl.startswith('#'):
                        ttohl = tth.readline()
                    ttohSpl = ttohl.split()
                    ids = ttohSpl[3].strip(',').split(',')
                    #if int(ids[idx]) > 0:
                    out.write('%s,-%s,%s\n' % (ttohSpl[0], ttohSpl[2], ids[idx]))
                out.write('#END')


# Script to use if no cycle dissolving is chosen
def findMaxNoCycleOpt(tthruPath, tbflPath, outPath):
    strTime = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    with open(tthruPath, 'r') as tthru:
        with open(tbflPath, 'r') as tbfl:
            with open(outPath, 'w') as out:
                out.write('# topology_n1.par created %s by hru-delin next gen (cycles not removed)\n' % strTime)
                out.write('# ID\tTO_HRU\tTO_REACH\n')

                tthrul = tthru.readline()
                bfll = tbfl.readline()
                while bfll and tthrul:
                    while tthrul and tthrul.startswith('#'):
                        tthrul = tthru.readline()
                    while bfll and bfll.startswith('#'):
                        bfll = tbfl.readline()

                    if bfll and tthrul:
                        bflThird = bfll.strip().split()[2]
                        rates = [float(r) for r in bflThird.split(',')]
                        maxRate = max(rates)
                        maxIndex = rates.index(maxRate)

                        tthruSpl = tthrul.strip().split()
                        ids = tthruSpl[3].split(',')
                        if int(ids[maxIndex]) > 0:
                            out.write('%s\t%s\t0\n' % (tthruSpl[0], ids[maxIndex]))
                        else:
                            out.write('%s\t0\t%s\n' % (tthruSpl[0], ids[maxIndex]))

                        tthrul = tthru.readline()
                        bfll = tbfl.readline()

# Transform back to original HRU*TO_REACH*TO_PAR format
# in case of cycle removing
def origFormat(inPath, outPath):
    strTime = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    with open(inPath, 'r') as inF:
        with open(outPath, 'w') as outF:
            outF.write('# topology_n1.par created %s by hru-delin-nextgen\n' % strTime)
            outF.write('# ID\tTO_HRU\tTO_REACH\n')
            for l in inF.readlines():
                if not l.startswith('#'):
                    spl = l.strip().split(',')
                    if int(spl[2]) > 0:
                        outF.write('%s\t%s\t0\n' % (spl[0], spl[2]))
                    else:
                        outF.write('%s\t0\t%s\n' % (spl[0], spl[2]))
