#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_basins.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
# PURPOSE:      1. Relocates the gauges on the reaches
#               2. Calculates watersheds at the gauges
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################

# to keep python2 compatibility
from __future__ import print_function

import glob
import os
import platform
import string
import sys
import time
import types

import numpy as np

try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
# import grass.script as grass
from grass.script.utils import decode, encode
import struct
import math
import csv
import shutil

from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr

import multiprocessing
from multiprocessing import Pool, cpu_count
import pandas as pd

from utils import isint, write_log
from reach import snapping_points_to_reaches, cut_streams_at_points
from reach import updateAttributeTable, processReachStats

MY_ABS_PATH = os.path.abspath(__file__)
MY_DIR = os.path.dirname(MY_ABS_PATH)

gdal.UseExceptions()
gdal.PushErrorHandler('CPLQuietErrorHandler')

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

'''

 MAIN

'''


def main(parms_file):
    print("-----------------------------------------------------------------------------------")
    print('---------- HRU-delin Step 2-1 started ---------------------------------------------')
    print("-----------------------------------------------------------------------------------")

    print("----------- Cleaning environment --------------------------------------------------")
    driver = ogr.GetDriverByName("ESRI Shapefile")
    configFileDir = os.path.dirname(parms_file)
    # create main env
    buildGrassEnv(os.path.join(configFileDir, 'grass_db'), 'hru-delin')
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')
    # Get parameters from configuration file
    parms = ConfigParser.ConfigParser(allow_no_value=True)

    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)

    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)

    shapefileGauges = os.path.join(directory_out, 'step2_gauges_for_watersheds.shp')
    if os.path.exists(shapefileGauges):
        driver.DeleteDataSource(shapefileGauges)

    shapefileDams = os.path.join(directory_out, 'step2_dams_for_watersheds.shp')
    if os.path.exists(shapefileDams):
        driver.DeleteDataSource(shapefileDams)

    fileDams = os.path.join(directory_out, 'dams_reloc.csv')
    if os.path.exists(fileDams):
        os.remove(fileDams)

    fileGauges = os.path.join(directory_out, 'gauges_reloc.csv')
    if os.path.exists(fileGauges):
        os.remove(fileGauges)

    filePoint = os.path.join(directory_out, "list_point.csv")
    if os.path.exists(filePoint):
        os.remove(filePoint)

    """OUTPUT files
    - gauges_reloc.csv
    - dams_reloc.csv
    - step2_dams_for_watersheds.shp
    - step2_gauges_for_watersheds.shp
    """
    
    ####################
    # GRASS ENVIRONNEMENT
    ####################
    
# test parameters from configuration file
    # if auto_relocation == yes, test int value for surface_tolerance_1 and distance_tolerance_1
    if (parms.get('auto_relocation', 'to_do')) == 'yes':

        if not isint(parms.get('auto_relocation', 'surface_tolerance_1')):
            sys.exit('------------> ERROR : Surface_tolerance_1 value not provided or is not integer')
        if not isint(parms.get('auto_relocation', 'distance_tolerance_1')):
            sys.exit('------------> ERROR : Distance_tolerance_1 value not provided or is not integer')

    # test if basin min size is valid
        if not isint(parms.get('basin_min_size', 'size')):
            sys.exit('------------> ERROR : Basin min size value not provided or is not integer')
    #######
    # GAUGES
    #######
    
    # Get the shape of gauges
    gauges_file = parms.get('gauges', 'relocated_gauges')
    if gauges_file == '':
        gauges_file = os.path.join(directory_out, 'gauges_selected.shp')
    else:
        if ogr.Open(gauges_file) is None:
            sys.exit('------------> ERROR : Relocated Gauges file not found')

    gauges_in = ogr.Open(gauges_file)
    gauges_lyr = gauges_in.GetLayer()

    # Set the new shape
    gauges_reloc_name = 'step2_gauges_for_watersheds'
    gauges_reloc_file = os.path.join(directory_out, gauges_reloc_name + '.shp')
    driver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(gauges_reloc_file):
        driver.DeleteDataSource(gauges_reloc_file)
    gauges_reloc_shp = driver.CreateDataSource(gauges_reloc_file)
    gauges_reloc_lyr = gauges_reloc_shp.CopyLayer(gauges_lyr, gauges_reloc_name)
   
    # Relocation of the gauges
    if (parms.get('auto_relocation', 'to_do')) == 'yes':
        print('---------- HRU-delin Step 2-1 : Relocation of the gauges')
        
        gauges_area_col_name = parms.get('gauges', 'gauges_area_col_name')
        gauges_col_name = parms.get('gauges', 'gauges_col_name')
        snapping_points_to_reaches(parms, directory_out, gauges_col_name,
                                   gauges_area_col_name, gauges_reloc_lyr, 'gauges_reloc.csv', 'gauges')

    gauges_reloc_shp.ExecuteSQL('REPACK ' + gauges_reloc_lyr.GetName())
    gauges_reloc_shp.Destroy()

    #
    # DAMS
    #####
    
    # relocation of dams if provided
    dams_reloc_file = 0
    if str(parms.get('dams', 'to_do')) == 'yes':
        # Get the shape of dams
        dams_file = parms.get('dams', 'relocated_dams')
        if dams_file == '':
            dams_file = os.path.join(directory_out, 'dams_selected.shp')
        else:
            if ogr.Open(dams_file) is None:
                sys.exit('------------> ERROR : Relocated dams file not found')

        dams_in = ogr.Open(dams_file)
        dams_lyr = dams_in.GetLayer()

        # Set the new shape
        dams_reloc_name = 'step2_dams_for_watersheds'
        dams_reloc_file = os.path.join(directory_out, dams_reloc_name + '.shp')
        driver = ogr.GetDriverByName('ESRI Shapefile')
        if os.path.exists(dams_reloc_file):
            driver.DeleteDataSource(dams_reloc_file)
        dams_reloc_shp = driver.CreateDataSource(dams_reloc_file)
        dams_reloc_lyr = dams_reloc_shp.CopyLayer(dams_lyr, dams_reloc_name)
   
        # Relocation of the dams
        if (parms.get('auto_relocation', 'to_do')) == 'yes':
            print('---------- HRU-delin Step 2-1 : Relocation of the dams')
            
            dams_area_col_name = parms.get('dams', 'dams_area_col_name')
            dams_col_name = parms.get('dams', 'dams_col_name')
            snapping_points_to_reaches(parms, directory_out, dams_col_name,
                                       dams_area_col_name, dams_reloc_lyr, 'dams_reloc.csv', 'dams')

        dams_reloc_shp.ExecuteSQL('REPACK ' + dams_reloc_lyr.GetName())
        dams_reloc_shp.Destroy()

    # Import dem (for EPSG) and drain raster
    dem_cut = os.path.join(directory_out, 'step1_dem_cut.tif')
    dem_wk = 'dem_wk'
    grass_run_command('g.proj', flags='c', georef=dem_cut, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.in.gdal', flags='o', input=dem_cut, output=dem_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=dem_wk, stdout=DEVNULL, stderr=DEVNULL)

    drain_layer = os.path.join(directory_out, 'step1_drain.tif')
    drain_wk = 'drain_wk'
    grass_run_command('r.in.gdal', flags='o', input=drain_layer, output=drain_wk, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)

    grass_run_command('r.mapcalc', expression='basins=null()', overwrite=True, stdout=DEVNULL, stderr=DEVNULL)
    # gauges
    gaugesDs = ogr.Open(gauges_reloc_file)
    gauges_reloc_lyr = gaugesDs.GetLayer()
    nb_gauges = gauges_reloc_lyr.GetFeatureCount()
    # dams
    nb_dams = 0

    list_point = []
    if str(parms.get('dams', 'to_do')) == 'yes':
        dams_col_name = parms.get('dams', 'dams_col_name')
        damsDs = ogr.Open(dams_reloc_file)
        dams_reloc_lyr = damsDs.GetLayer()
        nb_dams = dams_reloc_lyr.GetFeatureCount()
        
        for dam in dams_reloc_lyr:
            geom = dam.GetGeometryRef()
            dam_x, dam_y = geom.GetX(), geom.GetY()
            dam_ID = int(dam.GetField(dams_col_name))
            tuple_dam = (dam_x, dam_y, dam_ID, "dams")
            list_point.append(tuple_dam) 

    nb_points = nb_gauges+nb_dams
    
    # test if nb_points still has feature
    if nb_points == 0:
        print("Error on gauges (and dams if provided) relocated layer : 0 features found ") 
        print("Maybe check basin min size parameter") 
        sys.exit()
    
    gauges_col_name = parms.get('gauges', 'gauges_col_name')
    for gauge in gauges_reloc_lyr:
        geom = gauge.GetGeometryRef()
        gauge_x, gauge_y = geom.GetX(), geom.GetY()
        gauge_ID = int(gauge.GetField(gauges_col_name))
        tuple_gauge = (gauge_x, gauge_y, gauge_ID, "gauges")
        list_point.append(tuple_gauge)
        
    # export of list_point
    df_list_point = pd.DataFrame(list_point)
    df_list_point.to_csv(os.path.join(directory_out, "list_point.csv"), index=False, header=False)
    
    # TEST EXIST FILES AND FILL FILES
    print('---------- HRU-delin Step 2-1 : Test of existing and completed files')
    # GAUGES
    
    # gauges_reloc
    gauges_reloc_path = os.path.join(directory_out, 'gauges_reloc.csv')
    if (parms.get('auto_relocation', 'to_do')) == 'yes':
        if os.stat(gauges_reloc_path).st_size == 0:
            print('--------------- gauges_reloc.csv is empty or nonexistent')
        else:
            pd_gauges_reloc = pd.read_csv(gauges_reloc_path)
            len_gauges_reloc = len(pd_gauges_reloc)
            if len_gauges_reloc > 0:
                print('--------------- gauges_reloc.csv is created and it has ', len_gauges_reloc, " lines")
            else:
                print("--------------- gauges_reloc.csv is created but it empty")

    # gauges_selected
    gauges_selected_path = os.path.join(directory_out, 'gauges_selected.shp')

    if os.stat(gauges_selected_path).st_size == 0:
        print('--------------- gauges_selected.shp is empty or nonexistent')
    else:
        datasource_gauges = ogr.Open(gauges_selected_path)
        layer_gauges = datasource_gauges.GetLayer()
        featureCount_gauges = layer_gauges.GetFeatureCount()
        if featureCount_gauges > 0:
            print('--------------- gauges_selected.shp is created and it has ', featureCount_gauges, " features")
        else:
            print("--------------- gauges_selected.shp is created but it empty")

    # DAMS
    if str(parms.get('dams', 'to_do')) == 'yes':
        # dams_reloc
        dams_reloc_path = os.path.join(directory_out, 'dams_reloc.csv')
        
        if os.stat(dams_reloc_path).st_size == 0:
            print('--------------- dams_reloc.csv is empty or nonexistent')
        else:
            pd_dams_reloc = pd.read_csv(dams_reloc_path)
            len_dams_reloc = len(pd_dams_reloc)
            if len_dams_reloc > 0:
                print('--------------- dams_reloc.csv is created and it has ', len_dams_reloc, " lines")
            else:
                print("--------------- dams_reloc.csv is created but it empty")
                
        # dams_selected
        dams_selected_path = os.path.join(directory_out, 'dams_selected.shp')
    
        if os.stat(dams_selected_path).st_size == 0:
            print('--------------- dams_selected.shp is empty or nonexistent')
        else:
            datasource_dams = ogr.Open(dams_selected_path)
            layer_dams = datasource_dams.GetLayer()
            featureCount_dams = layer_dams.GetFeatureCount()
            if featureCount_dams > 0:
                print('--------------- dams_selected.shp is created and it has ', featureCount_dams, " features")
            else:
                print("--------------- dams_selected.shp is created but it empty")

    print('---------- HRU-delin Step 2-1 ended ---------------------------------------------')



# MAIN _PARAM
if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    # check TQDM presence only if we are executed
    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'

    if len(sys.argv) > 1:
        parms_file = sys.argv[1]

    main(parms_file)

    try:
        os.system('notify-send "hru-delin-6-2 step 2-1 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
