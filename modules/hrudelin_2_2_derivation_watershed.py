#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_basins.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
# PURPOSE:      1. Relocates the gauges on the reaches
#               2. Calculates watersheds at the gauges
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################

# to keep python2 compatibility
from __future__ import print_function

import glob
import os
import platform
import string
import sys
import time
import types

import numpy as np

try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
# import grass.script as grass
from grass.script.utils import decode, encode
import struct
import math
import csv
import shutil

from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr

import multiprocessing
from multiprocessing import Pool, cpu_count

import pandas as pd
from rastertodataframe import raster_to_dataframe

from utils import isint, write_log
from reach import snapping_points_to_reaches, cut_streams_at_points
from reach import updateAttributeTable, processReachStats

MY_ABS_PATH = os.path.abspath(__file__)
MY_DIR = os.path.dirname(MY_ABS_PATH)

gdal.UseExceptions()
gdal.PushErrorHandler('CPLQuietErrorHandler')

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

'''

 MAIN

'''


def main(parms_file):
    
    """OUTPUT files
    - basins.tif
    """
    print(" ")
    print('---------- HRU-delin Step 2-2 started ---------------------------------------------')
    print("-----------------------------------------------------------------------------------")
    
    configFileDir = os.path.dirname(parms_file)
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)

    print("----------- Cleaning environment --------------------------------------------------")
    fileBasin = os.path.join(directory_out, 'basins.tif')
    if os.path.exists(fileBasin):
        os.remove(fileBasin)
        
    # open list_point.csv
    file = pd.read_csv(os.path.join(directory_out, "list_point.csv"), header=None)
    # convert to tuples
    list_point = list(file.itertuples(index=False, name=None))
    nb_points = len(list_point)
    # Set Grass environnement
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')

    # Import dem (for EPSG) and drain raster
    dem_cut = os.path.join(directory_out, 'step1_dem_cut.tif')
    dem_wk = 'dem_wk'
    grass_run_command('g.proj', flags='c', georef=dem_cut, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.in.gdal', flags='o', input=dem_cut, output=dem_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=dem_wk, stdout=DEVNULL, stderr=DEVNULL)
    
    drain_layer = os.path.join(directory_out, 'step1_drain.tif')
    drain_wk = 'drain_wk'
    grass_run_command('r.in.gdal', flags='o', input=drain_layer, output=drain_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    i = 0
    basins = 'basins'
    rasters_list = ['basins']
    max_rasters = 29

    print('---------- HRU-delin Step 2-2 : r.water.outlet')
    
    for points in list_point:
    
        grass_run_command('r.water.outlet',
                          input=drain_wk, output='basin_tmp%d' % i,
                          coordinates='%s,%s' % (points[0], points[1]),
                          overwrite='True',
                          stdout=DEVNULL, stderr=DEVNULL
                          )

        # export vector watershed for dams
        if str(parms.get('dams', 'to_do')) == 'yes' and str(parms.get('dams', 'dams_export_sbw')) == 'yes':
            if points[3] == "dams":
                dams_basin_number = 'dams_basin_tmp' + str(points[2])
                grass_run_command('r.to.vect', flags='v', input='basin_tmp%d' % i, output=dams_basin_number,
                                  type='area', overwrite='True', quiet=True)

                dams_basin_number_geojson = 'dams_basin_tmp' + str(points[2]) + '.geojson'

                grass_run_command('v.out.ogr',
                                  # flags='c',
                                  quiet=True,
                                  input=dams_basin_number, type='area', format='GeoJSON',
                                  output=os.path.join(directory_out, dams_basin_number_geojson))

        # export vector watershed for gauges
        if str(parms.get('gauges', 'gauges_export_sbw')) == 'yes':
            if points[3] == "gauges":
                gauges_basin_number = 'gauges_basin_tmp' + str(points[2])
                grass_run_command('r.to.vect', flags='v', input='basin_tmp%d' % i, output=gauges_basin_number,
                                  type='area', overwrite='True', quiet=True)

                gauges_basin_number_geojson = 'gauges_basin_tmp' + str(points[2]) + '.geojson'

                grass_run_command('v.out.ogr',
                                  # flags='c',
                                  quiet=True,
                                  input=gauges_basin_number, type='area', format='GeoJSON',
                                  output=os.path.join(directory_out, gauges_basin_number_geojson))

        rasters_list.append('basin_tmp%d' % i)
        i += 1
        
        if i % max_rasters == 0 or i == nb_points:
            grass_run_command('r.cross', input=','.join(rasters_list), output=basins, overwrite='True',
                              stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('g.remove', flags='f', type='raster', pattern='basin_tmp*',
                              stdout=DEVNULL, stderr=DEVNULL)
            rasters_list = ['basins']
    

    # with grass7 watershed ids begin at 0
    # empty areas value is already NULL, no need to set it
    grass_run_command('r.mapcalc', expression='basins=basins+1', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    # TODO remove this line
    grass_run_command('r.out.gdal', input='basins', output=os.path.join(directory_out, 'basins.tif'),
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # TEST EXIST FILES AND FILL FILES
    print('---------- HRU-delin Step 2-2 : Test of existing and completed files')
    
    # basins.tif
    tif_path = os.path.join(directory_out, 'basins.tif')
    
    if os.stat(tif_path).st_size == 0:
        print('--------------- basins.tif is empty or nonexistent')
    else:
        tif_gdal = gdal.Open(tif_path)
        tif_band = tif_gdal.GetRasterBand(1)
        (min_tif, max_tif) = tif_band.ComputeRasterMinMax(True)

        if min_tif > 0 and max_tif <= nb_points:
            print('--------------- basins.tif is created and it has', nb_points, "watersheds")
        else:
            print("--------------- basins.tif is created but it empty")

    print('---------- HRU-delin Step 2-2 ended ---------------------------------------------')


if __name__ == '__main__':

    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    # check TQDM presence only if we are executed
    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'

    if len(sys.argv) > 1:
        parms_file = sys.argv[1]

    main(parms_file)

    try:
        os.system('notify-send "hru-delin-6-2-1 step 2-2 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
