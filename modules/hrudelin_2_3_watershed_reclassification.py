#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_basins.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
# PURPOSE:      1. Relocates the gauges on the reaches
#               2. Calculates watersheds at the gauges
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################

# to keep python2 compatibility
from __future__ import print_function

import glob
import os
import platform
import string
import sys
import time
import types

import numpy as np

try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
# import grass.script as grass
from grass.script.utils import decode, encode
import struct
import math
import csv
import shutil

from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr

import multiprocessing
from multiprocessing import Pool, cpu_count

import pandas as pd
from rastertodataframe import raster_to_dataframe

from utils import isint, write_log
from reach import snapping_points_to_reaches, cut_streams_at_points
from reach import updateAttributeTable, processReachStats

MY_ABS_PATH = os.path.abspath(__file__)
MY_DIR = os.path.dirname(MY_ABS_PATH)

gdal.UseExceptions()
gdal.PushErrorHandler('CPLQuietErrorHandler')

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

'''

 MAIN

'''


def main(parms_file):
    
    """OUTPUT files
    - step2_watersheds.tif
    """
    
    print(" ")
    print('---------- HRU-delin Step 2-3 started ---------------------------------------------')
    print("-----------------------------------------------------------------------------------")
     
    configFileDir = os.path.dirname(parms_file)
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)

    print("----------- Cleaning environment --------------------------------------------------")
    fileWatershed = os.path.join(directory_out, 'step2_watersheds.tif')
    if os.path.exists(fileWatershed):
        os.remove(fileWatershed)

    # open list_point.csv
    file = pd.read_csv(os.path.join(directory_out, "list_point.csv"), header=None)
    # convert to tuples
    list_point = list(file.itertuples(index=False, name=None))
    # Set Grass environnement
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')
    
    # Import dem (for EPSG) and drain raster
    dem_cut = os.path.join(directory_out, 'step1_dem_cut.tif')
    dem_wk = 'dem_wk'
    grass_run_command('g.proj', flags='c', georef=dem_cut, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.in.gdal', flags='o', input=dem_cut, output=dem_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=dem_wk, stdout=DEVNULL, stderr=DEVNULL)
    
    drain_layer = os.path.join(directory_out, 'step1_drain.tif')
    drain_wk = 'drain_wk'
    grass_run_command('r.in.gdal', flags='o', input=drain_layer, output=drain_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    # Import basins raster
    basins_tif = os.path.join(directory_out, 'basins.tif')
    basins = "basins"

    basins_rcl = 'basins_rcl'
    
    # create a GRASS basins raster
    grass_run_command('r.in.gdal', flags='o', input=basins_tif, output=basins, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    
    pRecode = grass_feed_command('r.recode', input=basins, output=basins_rcl, overwrite='True', rules='-', quiet=True)

    for points in list_point:
        out = decode(grass_read_command('r.what', map=basins, coordinates='%s,%s' % (points[0], points[1])))
        cat_grass = out.rstrip(os.linesep).split('|')[3].strip()
        pRecode.stdin.write(encode('%s:%s:%s\n' % (cat_grass, cat_grass, points[2])))
    pRecode.stdin.close()
    pRecode.wait()
    
    grass_run_command('r.out.gdal',
                      input=basins_rcl,
                      type='UInt16',
                      output=os.path.join(directory_out, 'step2_watersheds.tif'),
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # TEST EXIST FILES AND FILL FILES
    print('---------- HRU-delin Step 2-3 : Test of existing and completed files')
    
    # basins.tif
    tif_path = os.path.join(directory_out, 'step2_watersheds.tif')
    
    if os.stat(tif_path).st_size == 0:
        print('--------------- step2_watersheds.tif is empty or nonexistent')
    else:
        tif_gdal = gdal.Open(tif_path)
        tif_band = tif_gdal.GetRasterBand(1)
        (min_tif, max_tif) = tif_band.ComputeRasterMinMax(True)

        if min_tif > 0:
            print('--------------- step2_watersheds.tif is created')
        else:
            print("--------------- step2_watersheds.tif is created but it empty")

    print('---------- HRU-delin Step 2-3 ended ---------------------------------------------')


if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    # check TQDM presence only if we are executed
    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'

    if len(sys.argv) > 1:
        parms_file = sys.argv[1]

    main(parms_file)

    try:
        os.system('notify-send "hru-delin-6-2-1 step 2-3 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
