#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_basins.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
# PURPOSE:      1. Relocates the gauges on the reaches
#               2. Calculates watersheds at the gauges
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################

# to keep python2 compatibility
from __future__ import print_function

import glob
import os
import platform
import string
import sys
import time
import types

import numpy as np

try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser

from grass.script.utils import decode, encode
import struct
import math
import csv
import shutil

from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr

import multiprocessing
from multiprocessing import Pool, cpu_count

import pandas as pd
from rastertodataframe import raster_to_dataframe

from utils import isint, write_log
from reach import snapping_points_to_reaches, cut_streams_at_points
from reach import updateAttributeTable, processReachStats

MY_ABS_PATH=os.path.abspath(__file__)
MY_DIR=os.path.dirname(MY_ABS_PATH)

gdal.UseExceptions()
gdal.PushErrorHandler('CPLQuietErrorHandler')

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

'''

 MAIN

'''


def main(parms_file, nbProc, generator=False):
    
    """OUTPUT files
    - step2_mask.tif
    """
    print(" ")
    print('---------- HRU-delin Step 2-6 started ---------------------------------------------')
    print("-----------------------------------------------------------------------------------")
    
    configFileDir = os.path.dirname(parms_file)
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)

    print("----------- Cleaning environment --------------------------------------------------")
    driver = ogr.GetDriverByName("ESRI Shapefile")

    fileSubbasins = os.path.join(directory_out, 'step2_subbasins.tif')
    if os.path.exists(fileSubbasins):
        os.remove(fileSubbasins)

    fileReach = os.path.join(directory_out, 'step2_reachraster.tif')
    if os.path.exists(fileReach):
        os.remove(fileReach)

    fileCross = os.path.join(directory_out, 'step2_cross1.tif')
    if os.path.exists(fileCross):
        os.remove(fileCross)

    fileSubbasins2 = os.path.join(directory_out, 'step2_subbasins_2.tif')
    if os.path.exists(fileSubbasins2):
        os.remove(fileSubbasins2)

    shapefileSubbasins = os.path.join(directory_out, 'step2_step1_subbasins.shp')
    if os.path.exists(shapefileSubbasins):
        driver.DeleteDataSource(shapefileSubbasins)

    shapefileWatershed = os.path.join(directory_out, 'step2_step2_watersheds.shp')
    if os.path.exists(shapefileWatershed):
        driver.DeleteDataSource(shapefileWatershed)

    shapefileStep2Subbasins = os.path.join(directory_out, 'step2_step2_subbasins.shp')
    if os.path.exists(shapefileStep2Subbasins):
        driver.DeleteDataSource(shapefileStep2Subbasins)

    shapefileStep2Subbasins2 = os.path.join(directory_out, 'step2_step2_subbasins_2.shp')
    if os.path.exists(shapefileStep2Subbasins2):
        driver.DeleteDataSource(shapefileStep2Subbasins2)

    # Set Grass environnement
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')

    tmpPath = os.path.join(configFileDir, 'tmp')

    # Import dem (for EPSG) and drain raster
    dem_cut = os.path.join(directory_out, 'step1_dem_cut.tif')
    dem_wk = 'dem_wk'
    grass_run_command('g.proj', flags='c', georef=dem_cut, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.in.gdal', flags='o', input=dem_cut, output=dem_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=dem_wk, stdout=DEVNULL, stderr=DEVNULL)
    print('---------- Importing raster \'step1_drain.tif\'')
    drain_layer = os.path.join(directory_out, 'step1_drain.tif')
    drain_wk = 'drain_wk'
    grass_run_command('r.in.gdal', flags='o', input=drain_layer, output=drain_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    print('---------- Importing raster \'step1_subbasins.tif\'')
    subbasins = os.path.join(directory_out, 'step1_subbasins.tif')
    grass_run_command('r.in.gdal', flags='o', input=subbasins, output='subbasins', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Importing raster \'step2_watersheds.tif\'')
    basins = os.path.join(directory_out, 'step2_watersheds.tif')
    grass_run_command('r.in.gdal', flags='o', input=basins, output='watersheds', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Importing raster \'step2_streams_new.tif\'')
    reachraster = os.path.join(directory_out, 'step2_streams_new.tif')
    grass_run_command('r.in.gdal', flags='o', input=reachraster, output='reachraster', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Compute cross product \'watersheds*100000+subbasins\'')
    grass_run_command('r.mapcalc', expression='cross1=watersheds*100000+subbasins', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Compute cross product cross1*reachraster')
    grass_run_command('r.mapcalc', expression='cross2=if(reachraster!=0, cross1, 0)', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)

    if generator:
        yield 20
    
    print('---------- Setting nulls in \'cross1*reachraster\' and \'reachraster\'')
    grass_run_command('r.null', map='cross2', setnull=0, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.null', map='reachraster', setnull=0, stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Saving \'step2_subbasins.tif\' (watersheds*100000+subbasins)')
    grass_run_command('r.out.gdal', input='cross1', output=os.path.join(directory_out, 'step2_subbasins.tif'),
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Computing links between (watersheds*100000+subbasins) and reach ids')
    reach_ids = decode(grass_read_command('r.stats', quiet=True, flags='nN',
                                          input='reachraster')).rstrip(os.linesep).split(os.linesep)
    reach_ids_cleaned = []
    subbasins_cleaned = []

    print('')
    # main loop

    # export rasters that are necessary for parallel environments
    rastersForWorkers = {
        'reachraster': os.path.join(tmpPath, 'step2_reachraster.tif'),
        'cross1': os.path.join(tmpPath, 'step2_cross1.tif'),
    }
    exportRasters(rastersForWorkers)

    # save main grass env which is being overriden later
    MAIN_GISRC = os.environ['GISRC']

    # build the environments and load exported rasters in each of them
    grassDbPath = os.path.join(configFileDir, 'grass_db')
    for i in range(nbProc):
        location = 'hru-delin_%s' % (i+1)
        buildGrassLocation(grassDbPath, location)
        # set projection
        # TODO test with a raster we want to pass to //
        os.environ['GISRC'] = os.path.join(grassDbPath, 'grassdata', location, '.grassrc')
        # Import dem (for EPSG) and drain raster
        dem_cut = os.path.join(directory_out, 'step1_dem_cut.tif')
        dem_wk = 'dem_wk'
        grass_run_command('g.proj', flags='c', georef=dem_cut, stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('r.in.gdal', flags='o', input=dem_cut, output=dem_wk, overwrite='True', stdout=DEVNULL,
                          stderr=DEVNULL)
        grass_run_command('g.region', flags='sp', raster=dem_wk, stdout=DEVNULL, stderr=DEVNULL)

        importRastersInEnv(rastersForWorkers, grassDbPath, location)

    nbReachs = len(reach_ids)
    if generator:
        print('Starting reach loop with %s process' % nbProc)
        with Pool(nbProc) as p:
            params = [(id, configFileDir, nbProc) for (i, id) in enumerate(reach_ids)]
            results = []
            for i, _ in enumerate(p.imap_unordered(processReachStats, params), 1):
                results.append(_)
                loopProgress = i/nbReachs*100
                globalProgress = 25 + (loopProgress/100*60)
                yield globalProgress
    else:
        # this is the interesting part, launching N processes in parallel to process basins
        # the locks are here to prevent concurrent terminal tqdm writing
        with Pool(nbProc, initializer=tqdm.set_lock, initargs=(tqdm.get_lock(),)) as p:
            params = [(id, configFileDir, nbProc) for (i, id) in enumerate(reach_ids)]
            results = list(tqdm(p.imap_unordered(processReachStats, params),
                                desc='[main process] get reach id => subbasins id [%s process] ' % nbProc,
                                total=nbReachs,
                                unit='reach',
                                bar_format=bar_format1
                                ))

    # merge results
    for r in results:
        reach_ids_cleaned.append(r[0])
        subbasins_cleaned.append(r[1])

    # restore main grass env
    os.environ['GISRC'] = MAIN_GISRC

    print('')
    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Reclassifying (watersheds*100000+subbasins)')
    pReclass = grass_feed_command('r.reclass', input='cross1', output='cross1_reclassed', rules='-', overwrite='True')
    for k in range(0, len(reach_ids_cleaned)):
        pReclass.stdin.write(encode('%s=%s\n' % (subbasins_cleaned[k], reach_ids_cleaned[k])))
    pReclass.stdin.close()
    pReclass.wait()

    if generator:
        yield 90

    print('---------- Saving \'step2_subbasins_2.tif\'')
    try:
        grass_run_command('r.out.gdal', input='cross1_reclassed',
                          output=os.path.join(directory_out, 'step2_subbasins_2.tif'),
                          overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    except:
        sys.exit('------------> ERROR : Too many reclass category; check the value of ID of gauges and/or dams')       

    print('---------- Creating vector layers from raster layers ... ')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='subbasins', output='subbasins_vector', type='area',
                      overwrite='True')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='watersheds', output='watersheds_vector', type='area',
                      overwrite='True')
    # we could clean the vector data in case isolated pixels in edges produce false small areas
    # (with no category because it comes from an isolated nodata pixel in watershed.tif)
    # rasterWsheds = gdal.Open(basins)
    # ulx, xres, xskew, uly, yskew, yres = rasterWsheds.GetGeoTransform()
    # pixelArea = abs(xres) * abs(yres)
    # grass_run_command('v.clean',
    #    #flags='v',
    #    quiet=True,
    #    input='watersheds_vector',
    #    type='area',
    #    tool='rmarea',
    #    threshold=pixelArea,
    #    output='watersheds_vector_clean',
    #    overwrite='True')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='cross1', output='cross1_vector', type='area',
                      overwrite='True')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='cross1_reclassed', output='cross1_reclassed_vector',
                      type='area', overwrite='True')

    if generator:
        yield 95

    # to make sure there is a projection in exported files (grass78 complains)
    grass_run_command('g.proj', flags='c', georef=drain_layer, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('v.out.ogr',
                      # flags='c',
                      quiet=True,
                      overwrite='True',
                      input='subbasins_vector', type='area', format='ESRI_Shapefile',
                      output=os.path.join(directory_out, 'step2_step1_subbasins.shp'))
    # we avoid c flag to skip areas with no category (result of vectorisation error)
    grass_run_command('v.out.ogr',
                      # flags='c',
                      quiet=True,
                      overwrite='True',
                      input='watersheds_vector', type='area', format='ESRI_Shapefile',
                      output=os.path.join(directory_out, 'step2_step2_watersheds.shp'))
    grass_run_command('v.out.ogr',
                      # flags='c',
                      quiet=True,
                      overwrite='True',
                      input='cross1_vector', type='area', format='ESRI_Shapefile',
                      output=os.path.join(directory_out, 'step2_step2_subbasins.shp'))
    grass_run_command('v.out.ogr',
                      # flags='c',
                      quiet=True,
                      overwrite='True',
                      input='cross1_reclassed_vector', type='area', format='ESRI_Shapefile',
                      output=os.path.join(directory_out, 'step2_step2_subbasins_2.shp'))

    # TEST EXIST FILES AND FILL FILES
    print('---------- HRU-delin Step 2-6 : Test of existing and completed files')
    
    # step2_step1_subbasins.shp
    shp_subb_path = os.path.join(directory_out, 'step2_step1_subbasins.shp')
    
    if os.stat(shp_subb_path).st_size == 0:
        print('--------------- step2_step1_subbasins.shp is empty or nonexistent')
    else:
        datasource_subb1 = ogr.Open(shp_subb_path)
        layer_datasource_subb1 = datasource_subb1.GetLayer()
        featureCount_datasource_subb1 = layer_datasource_subb1.GetFeatureCount()
        if featureCount_datasource_subb1 > 0:
            print('--------------- step2_step1_subbasins.shp is created and it has ',
                  featureCount_datasource_subb1, " features")
        else:
            print("--------------- step2_step1_subbasins.shp is created but it empty")
            
    # step2_step2_watersheds.shp
    shp_watershed_path = os.path.join(directory_out, 'step2_step2_watersheds.shp')
    
    if os.stat(shp_watershed_path).st_size == 0:
        print('--------------- step2_step2_watersheds.shp is empty or nonexistent')
    else:
        datasource_watershed2 = ogr.Open(shp_watershed_path)
        layer_datasource_watershed2 = datasource_watershed2.GetLayer()
        featureCount_datasource_watershed2 = layer_datasource_watershed2.GetFeatureCount()
        if featureCount_datasource_watershed2 > 0:
            print('--------------- step2_step2_watersheds.shp is created and it has ',
                  featureCount_datasource_watershed2, " features")
        else:
            print("--------------- step2_step2_watersheds.shp is created but it empty")
            
    # step2_step2_subbasins.shp
    shp_subb2_path = os.path.join(directory_out, 'step2_step2_subbasins.shp')
    
    if os.stat(shp_subb2_path).st_size == 0:
        print('--------------- step2_step2_subbasins.shp is empty or nonexistent')
    else:
        datasource_subb2 = ogr.Open(shp_subb2_path)
        layer_datasource_subb2 = datasource_subb2.GetLayer()
        featureCount_datasource_subb2 = layer_datasource_subb2.GetFeatureCount()
        if featureCount_datasource_subb2 > 0:
            print('--------------- step2_step2_subbasins.shp is created and it has ',
                  featureCount_datasource_subb2, " features")
        else:
            print("--------------- step2_step2_subbasins.shp is created but it empty")
            
    # step2_step2_subbasins_2.shp
    shp_subb2_2_path = os.path.join(directory_out, 'step2_step2_subbasins_2.shp')
    
    if os.stat(shp_subb2_2_path).st_size == 0:
        print('--------------- step2_step2_subbasins.shp is empty or nonexistent')
    else:
        datasource_subb2_2 = ogr.Open(shp_subb2_2_path)
        layer_datasource_subb2_2 = datasource_subb2_2.GetLayer()
        featureCount_datasource_subb2_2 = layer_datasource_subb2_2.GetFeatureCount()
        if featureCount_datasource_subb2_2 > 0:
            print('--------------- step2_step2_subbasins_2.shp is created and it has ',
                  featureCount_datasource_subb2_2, " features")
        else:
            print("--------------- step2_step2_subbasins_2.shp is created but it empty")

    print('---------- HRU-delin Step 2-6 ended    ---------------------------------------------')


if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    # check TQDM presence only if we are executed
    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'
    nbProcArg = ''
    if len(sys.argv) > 1:
        parms_file = sys.argv[1]
        if len(sys.argv) > 2:
            nbProcArg = sys.argv[2]

    # determine how many processes we can launch
    if str(nbProcArg).isnumeric() and int(nbProcArg) > 0:
        nbProc = int(nbProcArg)
    else:
        nbProc = cpu_count()

    # main is a generator, but we don't use it here
    for pc in main(parms_file, nbProc, False):
        pass

    try:
        os.system('notify-send "hru-delin-6-2-1 step 2-6 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
