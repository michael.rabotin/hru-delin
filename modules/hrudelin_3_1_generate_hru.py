#!/usr/bin/env python3
# -*- coding: utf-8 -*-

############################################################################
#
# MODULE:       hrudelin_3_1_generate_hru
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
#
# PURPOSE:      overlay of all selected layers
#               construction of HRUs
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################

# to keep python2 compatibility
from __future__ import print_function

import glob
import math
import os
import shutil
import string
import sys
import time
import types

try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser

from grass.script.utils import decode, encode

from osgeo import gdal
from osgeo import ogr

from io import StringIO
import pandas as pd

import multiprocessing
from multiprocessing import Pool, cpu_count


def isint(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


def reclass(map_in, map_out, size):
    p = grass_pipe_command('r.stats', flags='lnNc', input=map_in)
      
    pReclass = grass_feed_command('r.reclass', overwrite=True, input=map_in, output=map_out, rules='-')
    for L in p.stdout:
        lSpl = decode(L).rstrip(os.linesep).split()
        if int(lSpl[1]) <= int(size):
            pReclass.stdin.write(encode('%s = %s\n' % (lSpl[0], lSpl[0])))
    p.wait()
    pReclass.stdin.close()
    pReclass.wait()


def count_only(map_in, size):
    statLines = decode(grass_read_command('r.stats', quiet=True, flags='lnNc', input=map_in)).rstrip(os.linesep).\
        split(os.linesep)
    c = 0
   
    for L in statLines:
        lSpl = L.split()
        if int(lSpl[1]) <= int(size):
            c += 1
    return c


def getAreasUpTo(map_in, size):
    areas = []
    statLists = []
    statLines = decode(grass_read_command('r.stats', quiet=True, flags='lnNc', input=map_in)).\
        rstrip(os.linesep).split(os.linesep)
    for L in statLines:
        statLists.append(L.strip().split())
    # sort on second column
    statLists = sorted(statLists, key=lambda x: int(x[1]))
    # awk
    intSize = int(size)
    old = 0
    for sl in statLists:
        intSl1 = int(sl[1])
        if old != intSl1 and intSl1 <= intSize:
            areas.append(intSl1)
            old = intSl1

    return areas


try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

MY_ABS_PATH = os.path.abspath(__file__)
MY_DIR = os.path.dirname(MY_ABS_PATH)

gdal.UseExceptions()
gdal.PushErrorHandler('CPLQuietErrorHandler')

'''
MAIN
'''


def main(parms_file):
    os.environ['GRASS_MESSAGE_FORMAT'] = 'silent'

    timestr = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    print('---------- HRU-delin Step 3_1 generate hru started -------------------------- %s' % str(timestr))

    configFileDir = os.path.dirname(parms_file)
    # create main env
    buildGrassEnv(os.path.join(configFileDir, 'grass_db'), 'hru-delin')
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')
    # Get parameters from configuration file
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)

    print("----------- Cleaning environment --------------------------------------------------")
    fileClumps = os.path.join(directory_out, 'step3_clumps.tif')
    if os.path.exists(fileClumps):
        os.remove(fileClumps)

    fileHruTmp = os.path.join(directory_out, 'step3_hrutmp.tif')
    if os.path.exists(fileHruTmp):
        os.remove(fileHruTmp)

    # test if surface is valid
    if not isint(parms.get('hrus_min_surface', 'surface')):
        sys.exit('------------> ERROR : Hrus min surface not provided or is not integer')

    # if hgeon, landuse and soil provided, test if exist
    directory = parms.get('dir_in', 'dir')
    for data in parms.items('data'):
        data = os.path.join(directory, data[1])
        if not os.path.isfile(data):
            
            sys.exit('------------> ERROR : Input data not found')

    # get rid of mask anyway
    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)

    # Import rasters in grass envir
    print('---------- HRU-delin Step 3_1 : Importing the rasters in GRASS')

    # Import dem (for EPSG)
    dem_cut = os.path.join(directory_out, 'step1_dem_cut.tif')
    dem_wk = 'dem_wk'
    grass_run_command('g.proj', flags='c', georef=dem_cut, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.in.gdal', flags='o', input=dem_cut, output=dem_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=dem_wk, stdout=DEVNULL, stderr=DEVNULL)

    dem_recl = os.path.join(directory_out, 'step1_dem_reclass.tif')

    data_list = []
    mask_list = []
    if parms.get('layer_overlay', 'dem') == 'yes':
        print('----------------------------- Importing raster \'step1_dem_reclass.tif\'')
        dem = 'dem'
        grass_run_command('r.in.gdal', flags='o', input=dem_recl, output=dem, overwrite='True',
                          stdout=DEVNULL, stderr=DEVNULL)
        data_list.append('dem')
        mask_list.append('dem_msk')

    if parms.get('layer_overlay', 'slope') == 'yes':
        print('----------------------------- Importing raster \'step1_slope_reclass.tif\'')
        slp_recl = os.path.join(directory_out, 'step1_slope_reclass.tif')
        grass_run_command('r.in.gdal', flags='o', input=slp_recl, output='slp', overwrite='True',
                          stdout=DEVNULL, stderr=DEVNULL)
        data_list.append('slp')
        mask_list.append('slp_msk')

    if parms.get('layer_overlay', 'aspect') == 'yes':
        print('----------------------------- Importing raster \'step1_aspect_reclass.tif\'')
        asp_recl = os.path.join(directory_out, 'step1_aspect_reclass.tif')
        grass_run_command('r.in.gdal', flags='o', input=asp_recl, output='asp', overwrite='True',
                          stdout=DEVNULL, stderr=DEVNULL)
        data_list.append('asp')
        mask_list.append('asp_msk')

    # read sub-basin raster created in step 1
    print('----------------------------- Importing raster \'step2_subbasins_2.tif\'')
    # new raster I. H.
    subbasins = os.path.join(directory_out, 'step2_subbasins_2.tif')
    grass_run_command('r.in.gdal', flags='o', input=subbasins, output='subbasins', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    data_list.append('subbasins')
    mask_list.append('subbasins_msk')

    # read watershed raster created in step 2
    print('----------------------------- Importing raster \'step2_watersheds.tif\'')
    basins = os.path.join(directory_out, 'step2_watersheds.tif')
    grass_run_command('r.in.gdal', flags='o', input=basins, output='watersheds', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    data_list.append('watersheds')
    mask_list.append('watersheds_msk')

    for data in parms.items('data'):
        print('----------------------------- Importing raster \'step1_%s\'' % str(data[1]))
        name = data[0]
        data_file = (os.path.join(directory_out, 'step1_' + data[1]))
        grass_run_command('r.in.gdal', flags='o', input=data_file,
                          output=name, overwrite='True', stdout=DEVNULL, stderr=DEVNULL
                          )
        data_list.append(name)
        mask_list.append(name + '_msk')

    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)

    grass_run_command('g.region', raster='watersheds', stdout=DEVNULL, stderr=DEVNULL)

    # Import mask(= all watersheds)
    print('----------------------------- Importing mask raster \'step2_mask\'')
    mask_in = os.path.join(directory_out, 'step2_mask.tif')
    mask_wk = 'mask_wk'
    grass_run_command('r.in.gdal', flags='o', input=mask_in, output=mask_wk, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.null', map=mask_wk, null=0, setnull=1, stdout=DEVNULL, stderr=DEVNULL)
    # Cut out every layer with mask
    for layer in data_list:
        print('----------------------------- Cutting layer \''+str(layer)+'\' according to mask')
        grass_run_command('r.patch', input='mask_wk,%s' % layer, output='%s_msk' % layer,
                          overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('r.null', map='%s_msk' % layer, setnull=0, stdout=DEVNULL, stderr=DEVNULL)

    # Create a first raster of hrus = combination (overlay) of layers
    timestr = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    print('---------- HRU-delin Step 3 : Creating the cross product of the layers --- ' + str(timestr))
    print('----------------------------- Computing the raw cross product')
    # r.cross has changed a lot in grass7, combinations with at least one NULL (not all) are now absent,
    # so we remove this option and reclass those combinations to get exactly what we with grass6
    # tested and approved
    grass_run_command('r.cross',
                      input=','.join(mask_list),
                      output='hrus_tmp_wrong', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL
                      )
    # generate reclass rules
    p = grass_pipe_command('r.stats', flags='l', input='hrus_tmp_wrong')
    pReclass = grass_feed_command('r.reclass', input='hrus_tmp_wrong', output='hrus_tmp', rules='-', overwrite='True')
    c = 1
    for L in p.stdout:
        dl = decode(L).rstrip(os.linesep)
        spl = dl.split()
        cat = spl[0]
        label = ' '.join(spl[1:])
        # used to be this but does not work in grass74 because result of r.cross
        # is slightly different than grass >= 76...
        # if 'NULL' in l:
        if not dl.startswith('*') and ('NULL' in dl or 'no data' in dl or dl.strip() == '0'):
            pReclass.stdin.write(encode('%s = 0\n' % cat))
        else:
            pReclass.stdin.write(encode('%s = %s %s\n' % (cat, c, label)))
            c += 1
    p.wait()
    pReclass.stdin.close()
    pReclass.wait()

    print('----------------------------- Saving the raw cross product')
    grass_run_command('r.out.gdal',
                      input='hrus_tmp',
                      output=os.path.join(directory_out, 'step3_hrutmp.tif'),
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # Attrib a unique cat to each HRU generated
    print('----------------------------- Attributing unique IDs to HRUs')
    grass_run_command('r.clump', input='hrus_tmp', output='clumps', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.out.gdal',
                      input='clumps',
                      output=os.path.join(directory_out, 'step3_clumps.tif'),
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- HRU-delin Step 3_1 generate hru ended   -------------------------- '+str(timestr))


if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv, exportRastersFromEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_pipe_command, grass_read_command
    from progressColors import *

    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found. %s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'

    if len(sys.argv) > 1:
        parms_file = sys.argv[1]

    main(parms_file)
    try:
        os.system('notify-send "hru-delin-6-2-1 step 3_1 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv, exportRastersFromEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_pipe_command, grass_read_command
    from .progressColors import *
