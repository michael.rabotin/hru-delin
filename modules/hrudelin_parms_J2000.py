#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_parms_J2000.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
#
# PURPOSE:      calculates the topology
#               generates parameters files for J2000
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################

# to keep python2 compatibility
from __future__ import print_function

import os
import shutil
import string
import sys
import time
import types
import zipfile

# import pythonGate as pyGate
try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
# import grass.script as grass
from grass.script.utils import decode, encode
import numpy as np

from osgeo import gdal
from osgeo.gdalconst import *
from osgeo import ogr
import pandas as pd
import json
from utils import isint
import multiprocessing
from multiprocessing import Pool, cpu_count

try:
    # Python 3.
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

MY_ABS_PATH = os.path.abspath(__file__)
MY_DIR = os.path.dirname(MY_ABS_PATH)

MODULES_PATH = os.path.join(MY_DIR, '..', 'modules')

gdal.UseExceptions()
gdal.PushErrorHandler('CPLQuietErrorHandler')

# function launched in parallel


def processSubbasin(params):
    id, configFileDir, iRun, nbBasins, tmpPath, nbProc, generator = params
    # define which grass env we are using
    current = multiprocessing.current_process()
    processN = int(current._identity[0])
    # because multiprocess does not reset workers id numbers and we still use [1, N]
    processN = (processN % nbProc) + 1
    location = 'hru-delin_%s' % processN
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', location, '.grassrc')

    if not generator:
        iterable2 = tqdm(
            total=6,
            initial=0,
            desc='[process %s] basin [%s/%s] id %s overlay hrus/stream %s' %
                 (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6), pad('', 5)),
            unit='phase',
            bar_format=bar_format_yellow_2,
            position=processN,
            leave=True
        )

    grass_run_command('r.mask', raster='subbasins', maskcats=id.rstrip('\n'), overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.region', raster='MASK', stdout=DEVNULL, stderr=DEVNULL)

    # OVERLAY HRUS WITH STREAM MAP
    grass_run_command('r.patch',
                      flags='z',
                      input='str_wk,hrus', output='result_cp',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    if not generator:
        iterable2.update(1)
        iterable2.set_description('[process %s] basin [%s/%s] id %s hru borders/neighbours %s' %
                                  (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6),
                                   pad('', 0)))
    # HRU BORDERS
    grass_run_command('r.mapcalc',
                      expression='hru_border=if((result_cp==result_cp[-1,-1]&&&' +
                                 'not(isnull(result_cp[-1,-1]))&&&result_cp==result_cp[-1,0]&&&' +
                                 'not(isnull(result_cp[-1,0]))&&&result_cp==result_cp[-1,1]&&&' +
                                 'not(isnull(result_cp[-1,1]))&&&result_cp==result_cp[0,1]&&&' +
                                 'not(isnull(result_cp[0,1]))&&&result_cp==result_cp[1,1]&&&' +
                                 'not(isnull(result_cp[1,1]))&&&result_cp==result_cp[1,0]&&&' +
                                 'not(isnull(result_cp[1,0]))&&&result_cp==result_cp[1,-1]&&&' +
                                 'not(isnull(result_cp[1,-1]))&&&result_cp==result_cp[0,-1]&&&' +
                                 'not(isnull(result_cp[0,-1]))),null(),result_cp)',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # NEIGHBOUR HRU ON BORDERS
    grass_run_command('r.mapcalc',
                      expression='hru_nb=' +
                                 'if((drain==2),if((hru_border==hru_border[-1,0]),null(),hru_border[-1,0]),' +
                                 'if((drain==3),if((hru_border==hru_border[-1,-1]),null(),hru_border[-1,-1]),' +
                                 'if((drain==4),if((hru_border==hru_border[0,-1]),null(),hru_border[0,-1]),' +
                                 'if((drain==5),if((hru_border==hru_border[1,-1]),null(),hru_border[1,-1]),' +
                                 'if((drain==6),if((hru_border==hru_border[1,0]),null(),hru_border[1,0]),' +
                                 'if((drain==7),if((hru_border==hru_border[1,1]),null(),hru_border[1,1]),' +
                                 'if((drain==8),if((hru_border==hru_border[0,1]),null(),hru_border[0,1]),' +
                                 'if((drain==1),if((hru_border==hru_border[-1,1]),null(),hru_border[-1,1])))))))))',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    if not generator:
        iterable2.update(1)
        iterable2.set_description('[process %s] basin [%s/%s] id %s create overlay map %s' %
                                  (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6),
                                   pad('', 4)))
    # GET HRU AREAS
    # os.system("r.stats -cnN input=hrus | awk '{print $1 \"=\" $1\" \"$2'} |
    # r.reclass --o input=hrus output=hru_sz rules=-")
    p = grass_pipe_command('r.stats', flags='cnN', input='hrus')
    pReclass = grass_feed_command('r.reclass', overwrite='True', input='hrus', output='hru_sz', rules='-')
    for L in p.stdout:
        spl = decode(L).rstrip(os.linesep).split()
        pReclass.stdin.write(encode('%s = %s %s\n' % (spl[0], spl[0], spl[1])))
    p.wait()
    pReclass.stdin.close()
    pReclass.wait()
    # CREATE OVERLAY MAP FOR FURTHER PROCESSING
    # there is a missing category in grass7 r.cross -z result. Behaviour of -z option changed
    # so now we need to remove -z to get all combinations with NULL and then reclassify to put them all in cat 0
    grass_run_command('r.cross',
                      input='hru_border,hru_nb,hru_sz',
                      output='res1', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # generate reclass rules
    p = grass_pipe_command('r.stats', flags='l', input='res1')
    pReclass = grass_feed_command('r.reclass', input='res1', output='res2', rules='-', overwrite='True')
    c = 1
    for L in p.stdout:
        dl = decode(L).rstrip(os.linesep)
        spl = dl.split()
        res1Cat = spl[0]
        res1Label = ' '.join(spl[1:])
        # used to be this but does not work in grass74 because result of r.cross is slightly
        # different than grass >= 76...
        # if 'NULL' in l:
        if not dl.startswith('*') and ('NULL' in dl or 'no data' in dl or dl.strip() == '0'):
            pReclass.stdin.write(encode('%s = 0\n' % res1Cat))
        else:
            pReclass.stdin.write(encode('%s = %s %s\n' % (res1Cat, c, res1Label)))
            c += 1
    p.wait()
    pReclass.stdin.close()
    pReclass.wait()

    if not generator:
        iterable2.update(1)
        iterable2.set_description('[process %s] basin [%s/%s] id %s flow accum on overlay %s' %
                                  (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6),
                                   pad('', 1)))
    # FLOW ACCUMULATION ANALYSIS ON res1
    grass_run_command('r.mapcalc', expression='acc_int=int(accum)', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.statistics',
                      base='res2', cover='acc_int', method='sum', output='sum_map',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.mapcalc', expression='acc_sum=sum_map', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.null', map='acc_sum', setnull=0, stdout=DEVNULL, stderr=DEVNULL)

    if not generator:
        iterable2.update(1)
    # NEW FOR MAX ACCUMULATION (FOR BJOERN CIRCLE ELIM)
    grass_run_command('r.statistics',
                      base='hrus', cover='acc_int', method='max', output='max_map',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.mapcalc', expression='acc_max=max_map', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.null', map='acc_max', setnull=0, stdout=DEVNULL, stderr=DEVNULL)
    # END NEW

    if not generator:
        iterable2.update(1)
        iterable2.set_description('[process %s] basin [%s/%s] id %s awk processing %s' %
                                  (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6),
                                   pad('', 8)))
    # AWK PROCESSING
    # acc_max ADDED TO r.stats
    # subb100
    # TODO check wether it should be *100 or not
    # new_id = str(int(id.rstrip("\n")) * 100)
    new_id = str(
        int(id.rstrip('\n')))  # I'm just testing what is does if I remove the *100 as it is causing topology issue.
    topoToHruTmpPath = os.path.join(tmpPath, 'topologie_to_hru.par_tmp')
    topoBflTmpPath = os.path.join(tmpPath, 'topologie_bfl.par_tmp')
    # os.system("r.stats -lnN input=res2,acc_sum,acc_max | awk -v BASIN=%s -v TTOHRU=%s_proc%s -v TBFL=%s_proc%s -f %s" %
    #    (new_id, topoToHruTmpPath, processN, topoBflTmpPath, processN, AWK_cmd_1))
    # nextgen mzfk
    stats = decode(grass_read_command('r.stats', flags='lnN', input='res2,acc_sum,acc_max')).rstrip(os.linesep).split(
        os.linesep)
    topoToHruTmpPath = os.path.join(tmpPath, 'topologie_to_hru.par_tmp_proc%s' % processN)
    topoBflTmpPath = os.path.join(tmpPath, 'topologie_bfl.par_tmp_proc%s' % processN)
    formatNm(new_id, topoToHruTmpPath, topoBflTmpPath, stats)

    if not generator:
        iterable2.update(1)
        iterable2.close()


'''
 MAIN
'''


def main(parms_file, nbProc, generator=False):
    os.environ['GRASS_MESSAGE_FORMAT'] = 'silent'

    print('------------- HRU-delin parms_J2000 Started -------------')
    configFileDir = os.path.dirname(parms_file)
    # create main env
    buildGrassEnv(os.path.join(configFileDir, 'grass_db'), 'hru-delin')
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')
    # Get parameters from configuration file
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')

    # if hgeon, landuse and soil provided, test if exist
    directory = parms.get('dir_in', 'dir')
    for data in parms.items('data'):
        data = os.path.join(directory, data[1])
        if not os.path.isfile(data):
            print(data)
            sys.exit('------------> ERROR : Input data not found')

    # if irrig_rast provided, test if is valid
    # if str(parms.get('irrigation', 'irrig_rast')) != '':
    #    irr_rast_test = os.path.join(directory, str(parms.get('irrigation', 'irrig_rast')))
    #    if not os.path.isfile(irr_rast_test):
    #        sys.exit('------------> ERROR : Irrigation raster not found' ) 

    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)

    dir_results = parms.get('dir_out', 'results')
    if not os.path.isabs(dir_results):
        dir_results = os.path.join(configFileDir, dir_results)

    if not os.path.isdir(directory_out):
        sys.exit('------------> ERROR : Out Directoy is not valid')
    # set projection
    grass_run_command('g.proj', flags='c', georef=os.path.join(directory_out, 'step3_hrus.tif'), stdout=DEVNULL,
                      stderr=DEVNULL)

    # save projection for hru.par
    regionEPSG = decode(grass_read_command('g.proj', flags='g')).split()
    matchingEPSG = ''.join([s for s in regionEPSG if "EPSG" in s])

    # Import rasters in grass envir
    print('----------------------------- Importing raster \'step3_hrus.tif\'')
    hrus = 'hrus'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step3_hrus.tif'),
                      output=hrus, overwrite='True')
    print('----------------------------- Importing raster \'step1_dem_filled.tif\'')
    dem = 'dem'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step1_dem_filled.tif'),
                      output=dem, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    print('----------------------------- Importing raster \'step1_slope.tif\'')
    slope = 'slope'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step1_slope.tif'),
                      output=slope, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    print('----------------------------- Importing raster \'step1_aspect.tif\'')
    aspect = 'aspect'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step1_aspect.tif'),
                      output=aspect, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    print('----------------------------- Importing raster \'step1_aspect_reclass.tif\'')
    aspect_rcl = 'aspect_rcl'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step1_aspect_reclass.tif'),
                      output=aspect_rcl, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    print('----------------------------- Importing raster \'step1_drain.tif\'')
    drain = 'drain'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step1_drain.tif'),
                      output=drain, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    print('----------------------------- Importing raster \'step1_accum.tif\'')
    accum = 'accum'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step1_accum.tif'),
                      output=accum, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    # print('----------------------------- Importing raster \'step1_subbasins.tif\'')
    print('----------------------------- Importing raster \'step2_subbasins_2.tif\'')  # I.H.: new raster
    subbasins = 'subbasins'
    grass_run_command('r.in.gdal', flags='o',
                      # input=directory_out + '/step1_subbasins.tif',
                      input=os.path.join(directory_out, 'step2_subbasins_2.tif'),
                      output=subbasins, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    print('----------------------------- Importing raster \'step2_watersheds.tif\'')
    watersheds = 'watersheds'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step2_watersheds.tif'),
                      output=watersheds, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)

    for data in parms.items('data'):
        name = data[0]
        print(('----------------------------- Importing raster \'step1_' + data[1] + '\''))
        data_file = os.path.join(directory_out, 'step1_' + data[1])
        grass_run_command('r.in.gdal', flags='o',
                          input=data_file,
                          output=name, overwrite='True',
                          stdout=DEVNULL, stderr=DEVNULL)

    # if str(parms.get('irrigation', 'irrig_rast')) != '':
    #    irrigation = 'irrigation'
    #    grass_run_command('r.in.gdal', flags='o',
    #                    input=os.path.join(directory_out, 'step1_irrigation.tif'),
    #                    output=irrigation, overwrite='True',
    #                    stdout=DEVNULL, stderr=DEVNULL)
    #    grass_run_command('r.null', map=irrigation, null=0, stdout=DEVNULL, stderr=DEVNULL)

    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.region', raster=subbasins, stdout=DEVNULL, stderr=DEVNULL)

    # Import mask(= all watersheds)
    mask_in = os.path.join(directory_out, 'step2_mask.tif')
    mask_wk = 'mask_wk'
    grass_run_command('r.in.gdal', flags='o', input=mask_in, output=mask_wk, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    grass_run_command('r.null', map=mask_wk, null=0, setnull=1, stdout=DEVNULL, stderr=DEVNULL)

    # Set mask to all HRUs
    # optim

    # Prepare for topology construction
    streams_wk = 'streams_wk'
    grass_run_command('r.in.gdal', flags='o',
                      input=os.path.join(directory_out, 'step2_streams_new.tif'),
                      output=streams_wk, overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    # set null to limit thin operations to do
    grass_run_command('r.null', map=streams_wk, setnull=0, stdout=DEVNULL, stderr=DEVNULL)

    # Save streams raster as shapefile
    streams_thinned = 'streams_thinned'
    grass_run_command('r.thin',
                      input=streams_wk, output=streams_thinned,
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.to.vect',
                      flags='v',
                      input=streams_thinned,
                      output='streams_shp',
                      type='line',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # added by I. Horner (2018-06-19)
    streams_shape = os.path.join(dir_results, 'reach.shp')
    # added by I. Horner (2018-06-19)
    grass_run_command('v.out.ogr',
                      flags='c', input='streams_shp', type='line', format='ESRI_Shapefile',
                      output=streams_shape, stdout=DEVNULL, stderr=DEVNULL)

    # negate stream
    grass_run_command('r.mapcalc',
                      expression='str_wk=if((streams_wk>0),-streams_wk,streams_wk)',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # Subbasins loop
    print('------------- Retrieving subbasin ids -----------')
    subbasins_msk = 'subbasins_msk'
    # print('r.patch  ')
    grass_run_command('r.patch',
                      input='mask_wk,subbasins', output=subbasins_msk,
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    # print('r.null  ')
    grass_run_command('r.null', map=subbasins_msk, setnull=0, stdout=DEVNULL, stderr=DEVNULL)
    # print('r.stats  ')
    basin_ids = decode(grass_read_command('r.stats', quiet=True, flags='nN', input='subbasins_msk')).rstrip(
        os.linesep).split(os.linesep)

    print('%s subbasins were found:' % len(basin_ids))
    print('%s, %s, %s ... %s, %s, %s ' % (
        basin_ids[0].rstrip('\n') if len(basin_ids) > 0 else '???',
        basin_ids[1].rstrip('\n') if len(basin_ids) > 1 else '???',
        basin_ids[2].rstrip('\n') if len(basin_ids) > 2 else '???',
        basin_ids[-3].rstrip('\n') if len(basin_ids) > 2 else '???',
        basin_ids[-2].rstrip('\n') if len(basin_ids) > 1 else '???',
        basin_ids[-1].rstrip('\n') if len(basin_ids) > 0 else '???'
    ))
    print('------------- subbasin loop started -------------',
          time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime()))
    print('')
    # export rasters that are necessary for parallel environments
    rastersForWorkers = {
        'subbasins': os.path.join(tmpPath, 'step4_subbasins.tif'),
        'str_wk': os.path.join(tmpPath, 'step4_str_wk.tif'),
        'hrus': os.path.join(tmpPath, 'step4_hrus.tif'),
        'drain': os.path.join(tmpPath, 'step4_drain.tif'),
        'accum': os.path.join(tmpPath, 'step4_accum.tif')
    }
    exportRasters(rastersForWorkers)

    # save main grass env which is being overriden later
    MAIN_GISRC = os.environ['GISRC']

    # build the environments and load exported rasters in each of them
    grassDbPath = os.path.join(configFileDir, 'grass_db')
    for i in range(nbProc):
        location = 'hru-delin_%s' % (i + 1)
        buildGrassLocation(grassDbPath, location)
        importRastersInEnv(rastersForWorkers, grassDbPath, location)

    nbBasins = len(basin_ids)
    # the locks are here to prevent concurrent terminal tqdm writing
    # this is the interesting part, launching N processes in parallel to process basins
    if generator:
        print('Starting subbasins loop with %s process' % nbProc)
        with Pool(nbProc) as p:
            params = [(id, configFileDir, i + 1, nbBasins, tmpPath, nbProc, generator) for (i, id) in
                      enumerate(basin_ids)]
            for i, _ in enumerate(p.imap_unordered(processSubbasin, params), 1):
                yield i / nbBasins * 33
    else:
        with Pool(nbProc, initializer=tqdm.set_lock, initargs=(tqdm.get_lock(),)) as p:
            params = [(id, configFileDir, i + 1, nbBasins, tmpPath, nbProc, generator) for (i, id) in
                      enumerate(basin_ids)]
            r = list(tqdm(p.imap_unordered(processSubbasin, params),
                          desc='[main process] Loop on subbasins [%s process] ' % nbProc,
                          total=nbBasins,
                          unit='basin',
                          bar_format=bar_format1
                          ))
        print('\n' * nbProc, end='')

    # nothing to export from parallel grass environments

    # restore main grass env
    os.environ['GISRC'] = MAIN_GISRC

    # merge results from parallel processes
    # and enjoy pandas doing all at once instead of awk+sort+tail+head...
    tthFrames = []
    tbflFrames = []
    for i in range(nbProc):
        n = i + 1
        tthNPath = os.path.join(tmpPath, 'topologie_to_hru.par_tmp_proc%s' % n)
        tbflNPath = os.path.join(tmpPath, 'topologie_bfl.par_tmp_proc%s' % n)
        # they might not exist if subbasin number is less than number of process...
        if os.path.exists(tthNPath) and os.path.exists(tbflNPath):
            df = pd.read_table(tthNPath, names=('a', 'b', 'c', 'd'))
            df.dropna(inplace=True)
            df = df.astype({'a': int, 'b': int, 'c': int, 'd': str})
            tthFrames.append(df)

            df = pd.read_table(tbflNPath, names=('a', 'b', 'c'))
            df.dropna(inplace=True)
            df = df.astype({'a': int, 'b': int, 'c': str})
            tbflFrames.append(df)

    strTime = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    dfTth = pd.concat(tthFrames)
    dfTth.sort_values('a', inplace=True)
    with open(os.path.join(tmpPath, 'topologie_to_hru.par'), 'w', newline='\n') as f:
        f.write('# topology_to_hru.par created %s by hru-delin-nextgen\n' % strTime)
        f.write('# ID\tMAXACC\tSUBBASIN\tTO_HRU\n')
        dfTth.to_csv(f, header=False, index=False, sep='\t')

    dfTbfl = pd.concat(tbflFrames)
    dfTbfl.sort_values('a', inplace=True)
    with open(os.path.join(tmpPath, 'topologie_bfl.par'), 'w', newline='\n') as f:
        f.write('# topology_rates.par created %s by hru-delin-nextgen\n' % strTime)
        f.write('# ID\tMAXACC\tWEIGHTS\n')
        dfTbfl.to_csv(f, header=False, index=False, sep='\t')

    print('')
    print('------------- subbasin loop Ended -------------', time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime()))

    # Find maximum destination rate (need to be updated because of additiona 2 line header
    # output adapted to requirements for Bjoern tool (see next)

    if (parms.get('topology', 'dissolve_cycle')) == 'yes':
        print('------------- Dissolve cycles -------------', time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime()))
        # adaptation N1
        topoToHruPath = os.path.join(tmpPath, 'topologie_to_hru.par')
        tbflPath = os.path.join(tmpPath, 'topologie_bfl.par')
        topoN1TmpPath = os.path.join(tmpPath, 'topologie_n1_tmp.par')
        # os.system('awk -v TTOHRU=%s -f %s %s/topologie_bfl.par > %s/topologie_n1_tmp.par' %
        # (topoToHruPath, AWK_cmd_2, tmpPath, tmpPath))
        findMax(topoToHruPath, tbflPath, topoN1TmpPath)

        # ADAPTION NM, E.G. ELIMINATION TOOL REQUIRES ,'#', AS SEPARATOR
        # os.system("(head -n 2 topologie_to_hru.par && tail -n +3 topologie_to_hru.par | sed \"s/\t/,'#',/g\")
        # > topologie_to_hru_tmp.par")
        # os.system("cp topologie_to_hru_tmp.par topologie_to_hru.par")
        # os.system("(head -n 2 topologie_bfl.par && tail -n +3 topologie_bfl.par | sed \"s/\t/,'#',/g\")
        # > topologie_bfl_tmp.par")
        # os.system("cp topologie_bfl_tmp.par topologie_bfl.par")

        # N1: REMOVE CYCLES USING BJOERN TOOL (Zirkelkill_1zuN.jar, NOT UPDATED)
        # RESULTS IN topologie_n1.par FILE
        circleKill(topoN1TmpPath, os.path.join(tmpPath, 'topologie_n1_tmp2'))

        # NEED ORIGINAL TABLE FORMAT (AND ADD HEADER) - E.G. FOR FINAL QGIS HRU BROWSER
        # ignore lines beginning with #
        # os.system('grep -v "^#" %s/topologie_n1_tmp2.par | awk -f %s > %s/topologie_n1.par' %
        # (tmpPath, AWK_cmd_4, tmpPath))
        origFormat(os.path.join(tmpPath, 'topologie_n1_tmp2.par'), os.path.join(tmpPath, 'topologie_n1.par'))
        print('------------- Cycles dissolved -------------', time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime()))
    else:
        findMaxNoCycleOpt(
            os.path.join(tmpPath, 'topologie_to_hru.par'),
            os.path.join(tmpPath, 'topologie_bfl.par'),
            os.path.join(tmpPath, 'topologie_n1.par')
        )

    # CREATE REACH ROUTING FIRST
    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)

    # Call the module that creates the file reach.par
    print('------------- r.reach.par started -------------', time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime()))
    reach_par = os.path.join(dir_results, 'reach.par')
    # reachParDir = MY_DIR
    # if reachParDir not in os.environ['PATH'].split(':'):
    #    os.environ['PATH'] = '%s:%s' % (os.environ['PATH'], reachParDir)
    # grass_run_command('r.reach.par',
    #    streams='streams_wk', watershed='hrus', drainage='drain', dem='dem', output=reach_par,
    #    path=configFileDir,
    #    nbcores=nbProc,
    #    #stdout=DEVNULL, stderr=DEVNULL
    # )
    grassDbPath = os.path.join(configFileDir, 'grass_db')
    for i in range(nbProc):
        location = 'hru-delin_%s' % (i + 1)
        buildGrassLocation(grassDbPath, location)
    if generator:
        for pc in buildReachPar('streams_wk', 'hrus', 'drain', 'dem', reach_par, configFileDir, nbProc, generator):
            globalPc = 33 + (pc / 100 * 33)
            yield globalPc
    else:
        res = list(buildReachPar('streams_wk', 'hrus', 'drain', 'dem', reach_par, configFileDir, nbProc, generator))
    print('------------- r.reach.par finished -------------', time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime()))

    print('------------- producing hru.shp and hru.par -------------',
          time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime()))
    print()

    if not generator:
        iterable3 = tqdm(
            total=8,
            initial=0,
            desc='Creating hru.par and hru.shp %s' % (pad('', 5)),
            unit='phase',
            bar_format=bar_format1,
        )
        iterable3.update(1)
    else:
        yield 75

    # Convert hru raster to shape polygon
    grass_run_command('g.region', raster=hrus)
    grass_run_command('g.region', zoom=hrus)
    grass_run_command('r.to.vect', input=hrus, output='hrus_v', type='area', overwrite='True')

    rawTables = decode(grass_read_command('db.tables')).rstrip(os.linesep).split(os.linesep)
    tables = [t.strip() for t in rawTables]
    if 'hrus_v_cat2' in tables:
        grass_run_command('db.droptable', flags='f', table='hrus_v_cat2')
    grass_run_command('r.to.vect', flags='v', input=hrus, output='hrus_v_cat2', type='area', overwrite='True')

    grass_run_command('v.extract', input='hrus_v', type='centroid', output='hrus_c', overwrite='True')
    grass_run_command('v.type', input='hrus_c', output='hrus_c_pnt', from_type='centroid', to_type='point',
                      overwrite='True')

    # Create parameters file for J2000

    if not generator:
        iterable3.update(1)
    else:
        yield 80
    #  header
    ranges = {}
    ranges['ID'] = [0, 999999, 'n/a']

    headerPath = os.path.join(tmpPath, 'header')
    f = open(headerPath, 'w+')
    timestr = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    header = '# hru.par created ' + timestr + ' by HRU-DELIN_BATCH ' + 'projection system is ' + matchingEPSG + ' \n'
    header += 'ID'
    header_min_val = str(ranges['ID'][0])
    header_max_val = str(ranges['ID'][1])
    header_unit = ranges['ID'][2]
    sql = 'cat'

    # versatile way to get resolution (does not depend on output lines order)
    regionOutput = decode(grass_read_command('g.region', flags='m')).rstrip(os.linesep).split(os.linesep)
    nres = None
    sres = None
    for ro in regionOutput:
        if ro.startswith('nsres'):
            nres = int(float(ro.split('=')[1]))
        elif ro.startswith('ewres'):
            sres = int(float(ro.split('=')[1]))

    if nres is None or sres is None:
        sys.exit('problem getting region resolution')

    single_pixel_size = nres * sres

    # os.system("r.stats -cnN input=hrus | awk '{print $1\"=\"$2*%d}' | r.reclass --o input=hrus output='hrus_sz'
    # rules=-" % single_pixel_size)
    p = grass_pipe_command('r.stats', flags='cnN', input='hrus')
    pReclass = grass_feed_command('r.reclass', overwrite='True', input='hrus', output='hrus_sz', rules='-')
    for L in p.stdout:
        spl = decode(L).rstrip(os.linesep).split()
        n = int(spl[1]) * single_pixel_size
        pReclass.stdin.write(encode('%s=%s\n' % (spl[0], n)))
    p.wait()
    pReclass.stdin.close()
    pReclass.wait()

    grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD area INTEGER')
    grass_run_command('v.what.rast', map='hrus_c_pnt', raster='hrus_sz', column='area')
    header += '\tarea'
    ranges['area'] = [0, 9999999, 'm2']
    header_min_val += '\t' + str(ranges['area'][0])
    header_max_val += '\t' + str(ranges['area'][1])
    header_unit += '\t' + ranges['area'][2]
    sql += ',area'

    if (parms.get('layer_overlay', 'dem')) == 'yes':
        dem_avg = 'dem_avg'
        dem_int = 'dem_int'
        grass_run_command('r.mapcalc', expression='dem_int=int(dem)', overwrite='True')
        grass_run_command('r.statistics', base=hrus, cover=dem_int, method='average', output=dem_avg, overwrite='True')
        grass_run_command('r.mapcalc', expression='dem_avg_cval=@dem_avg', overwrite='True')
        grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD elevation INTEGER')
        grass_run_command('v.what.rast', map='hrus_c_pnt', raster='dem_avg_cval', column='elevation', overwrite='True')
        header += '\televation'
        ranges['elevation'] = [0, 10000, 'm']
        header_min_val += '\t' + str(ranges['elevation'][0])
        header_max_val += '\t' + str(ranges['elevation'][1])
        header_unit += '\t' + ranges['elevation'][2]
        sql += ',elevation'

    if not generator:
        iterable3.update(1)
    else:
        yield 83

    if parms.get('layer_overlay', 'slope') == 'yes':

        slope_labeled = 'slope_labeled'
        slope_avg = 'slope_avg'

        grass_run_command('r.mapcalc', expression='slope_mult=int(slope*1000.0)', overwrite='True')
        # os.system("r.stats -nN input=slope_mult | awk '{print $1\"=\"$1\" \"$1/1000}' |
        # r.reclass --o input=slope_mult output=slope_labeled rules=-")

        p = grass_pipe_command('r.stats', flags='nN', input='slope_mult')
        pReclass = grass_feed_command('r.reclass', overwrite='True', input='slope_mult', output='slope_labeled',
                                      rules='-')
        for L in p.stdout:
            spl = decode(L).rstrip(os.linesep).split()
            pReclass.stdin.write(encode('%s = %s %s\n' % (spl[0], spl[0], int(spl[0]) / 1000)))
        p.wait()
        pReclass.stdin.close()
        pReclass.wait()

        grass_run_command('r.statistics', flags='c', base=hrus, cover=slope_labeled, method='average', output=slope_avg,
                          overwrite='True')
        grass_run_command('r.mapcalc', expression='slope_avg_cval=int(@slope_avg*1000)/1000.0', overwrite='True')
        grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD slope DOUBLE PRECISION')
        grass_run_command('v.what.rast', map='hrus_c_pnt', raster='slope_avg_cval', column='slope', overwrite='True')
        header += '\tslope'
        ranges['slope'] = [0, 90, 'deg']
        header_min_val += '\t' + str(ranges['slope'][0])
        header_max_val += '\t' + str(ranges['slope'][1])
        header_unit += '\t' + ranges['slope'][2]
        sql += ',slope'

    if not generator:
        iterable3.update(1)
    else:
        yield 85

    if (parms.get('layer_overlay', 'aspect')) == 'yes':
        grass_run_command('r.statistics', base='hrus', cover='aspect_rcl', method='mode', output='aspect_md',
                          overwrite='True')
        grass_run_command('r.mapcalc', expression='aspect_mj=if((aspect_rcl==@aspect_md),aspect,null())',
                          overwrite='True')
        grass_run_command('r.mapcalc', expression='aspect_mjj=if((aspect_mj>315.0),aspect_mj-360.0,aspect_mj)',
                          overwrite='True')
        grass_run_command('r.mapcalc', expression='aspect_int=int(aspect_mjj)', overwrite='True')
        grass_run_command('r.statistics', base='hrus', cover='aspect_int', method='average', output='aspect_avg',
                          overwrite='True')
        grass_run_command('r.mapcalc', expression='aspect_avg_cval=@aspect_avg', overwrite='True')
        grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD aspect INTEGER')
        grass_run_command('v.what.rast', map='hrus_c_pnt', raster='aspect_avg_cval', column='aspect', overwrite='True')
        header += '\taspect'
        ranges['aspect'] = [0, 360, 'deg']
        header_min_val += '\t' + str(ranges['aspect'][0])
        header_max_val += '\t' + str(ranges['aspect'][1])
        header_unit += '\t' + ranges['aspect'][2]
        sql += ',aspect'

    if not generator:
        iterable3.update(1)
    else:
        yield 90

    grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD x DOUBLE PRECISION')
    grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD y DOUBLE PRECISION')
    grass_run_command('v.to.db', map='hrus_c_pnt', option='coor', columns='x,y', overwrite='True')
    header += '\tx\ty'
    ranges['centroid'] = [0, 9999999, 'm']
    header_min_val += '\t' + str(ranges['centroid'][0]) + '\t' + str(ranges['centroid'][0])
    header_max_val += '\t' + str(ranges['centroid'][1]) + '\t' + str(ranges['centroid'][1])
    header_unit += '\t' + ranges['centroid'][2] + '\t' + ranges['centroid'][2]
    sql += ',x,y'

    grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD watershed INTEGER')
    grass_run_command('v.what.rast', map='hrus_c_pnt', raster='watersheds', column='watershed', overwrite='True')
    header += '\twatershed'
    ranges['watershed'] = [0, 999999, 'n/a']
    header_min_val += '\t' + str(ranges['watershed'][0])
    header_max_val += '\t' + str(ranges['watershed'][1])
    header_unit += '\t' + ranges['watershed'][2]
    sql += ',watershed'

    grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD subbasin INTEGER')
    grass_run_command('v.what.rast', map='hrus_c_pnt', raster='subbasins', column='subbasin', overwrite='True')
    header += '\tsubbasin'
    ranges['subbasin'] = [0, 999999, 'n/a']
    header_min_val += '\t' + str(ranges['subbasin'][0])
    header_max_val += '\t' + str(ranges['subbasin'][1])
    header_unit += '\t' + ranges['subbasin'][2]
    sql += ',subbasin'

    data_tmp2 = 'data_tmp2'
    data_md = 'data_md'

    for data in parms.items('data'):
        col_name = data[0] + 'ID'
        grass_run_command('r.mapcalc', expression='data_tmp2=int(%s)' % data[0], overwrite='True')
        grass_run_command('r.statistics', base=hrus, cover=data_tmp2, method='mode', output=data_md, overwrite='True')
        grass_run_command('r.mapcalc', expression='data_cval=@data_md', overwrite='True')
        grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD %s INTEGER' % col_name)
        grass_run_command('v.what.rast', map='hrus_c_pnt', raster='data_cval', column=col_name, overwrite='True')
        header += '\t' + col_name
        ranges['data1'] = [0, 9999, 'n/a']
        header_min_val += '\t' + str(ranges['data1'][0])
        header_max_val += '\t' + str(ranges['data1'][1])
        header_unit += '\t' + ranges['data1'][2]
        sql += ',' + col_name

    if os.path.exists('%s/to_poly' % tmpPath):
        os.remove('%s/to_poly' % tmpPath)
    if os.path.exists('%s/to_reach' % tmpPath):
        os.remove('%s/to_reach' % tmpPath)
    # FROM N1 ROUTING FILE, JUST SKIP FIRST TWO LINES (HEADER)
    # os.system("tail -n +3 %s/topologie_n1.par | awk 'BEGIN{}{print $1\"=\"$2 >>
    # \"%s/to_poly\";print $1\"=\"$3*-1 >> \"%s/to_reach\";}END{}'" % (tmpPath, tmpPath, tmpPath))
    topoN1Path = os.path.join(tmpPath, 'topologie_n1.par')
    pToPoly = grass_feed_command('r.reclass', input='hrus', output='to_poly_map', rules='-', overwrite='True')
    pToReach = grass_feed_command('r.reclass', input='hrus', output='to_reach_map', rules='-', overwrite='True')
    with open(topoN1Path, 'r') as topoN1:
        for L in topoN1:
            if not L.startswith('#'):
                lSpl = L.rstrip(os.linesep).split()
                pToPoly.stdin.write(encode('%s=%s\n' % (lSpl[0], lSpl[1])))
                pToReach.stdin.write(encode('%s=%s\n' % (lSpl[0], -int(lSpl[2]))))
    pToPoly.stdin.close()
    pToPoly.wait()
    pToReach.stdin.close()
    pToReach.wait()

    if not generator:
        iterable3.update(1)
    else:
        yield 95

    grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD to_poly INTEGER')
    grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD to_reach INTEGER')
    grass_run_command('v.what.rast', map='hrus_c_pnt', raster='to_poly_map', column='to_poly', overwrite='True')
    grass_run_command('v.what.rast', map='hrus_c_pnt', raster='to_reach_map', column='to_reach', overwrite='True')
    header += '\tto_poly'
    sql += ',to_poly'
    header += '\tto_reach'
    sql += ',to_reach'
    ranges['topo'] = [0, 999999, 'n/a']
    header_min_val += '\t' + str(ranges['topo'][0]) + '\t' + str(ranges['topo'][0])
    header_max_val += '\t' + str(ranges['topo'][1]) + '\t' + str(ranges['topo'][1])
    header_unit += '\t' + ranges['topo'][2] + '\t' + ranges['topo'][2]

    # if str(parms.get('irrigation', 'irrig_rast')) != '':
    #    col_name = 'irrigation'
    #    irrig_md = 'irrig_md'
    #    grass_run_command('r.mapcalc', expression='data_tmp2=int(%s)' % irrigation, overwrite='True')
    #    grass_run_command('r.statistics',flags='c', base=hrus, cover=data_tmp2, method='mode',
    #    output=irrig_md, overwrite='True')
    #    grass_run_command('r.mapcalc', expression='irrig_cval=@irrig_md', overwrite='True')
    #    grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD %s INTEGER'%col_name)
    #    grass_run_command('v.what.rast', map='hrus_c_pnt', raster='irrig_cval', column=col_name, overwrite='True')
    #    header += '\t' + col_name
    #    ranges['irrigation'] = [0, 1, 'n/a']
    #    header_min_val += '\t' + str(ranges['irrigation'][0])
    #    header_max_val += '\t' + str(ranges['irrigation'][1])
    #    header_unit += '\t' + ranges['irrigation'][2]
    #    sql += ',irrigation'

    grass_run_command('db.execute', sql='DROP INDEX hrus_c_pnt_cat')
    grass_run_command('db.execute', sql='UPDATE hrus_c_pnt SET cat=value')

    if not generator:
        iterable3.update(1)
    else:
        yield 98

    # Save files
    grass_run_command('v.db.connect', flags='o', table='hrus_c_pnt', map='hrus_v_cat2', key='cat', overwrite='True')
    # added by I. Horner (2018-06-19)
    hrus_shp = os.path.join(dir_results, 'hru.shp')
    grass_run_command('v.out.ogr', flags='c', input='hrus_v_cat2', type='area', format='ESRI_Shapefile',
                      output=hrus_shp)

    # get watershed id from hru to step2_gauges_for_watersheds.shp
    step2_gauges_shape = os.path.join(directory_out, 'step2_gauges_for_watersheds.shp')
    try:
        grass_run_command('v.in.ogr', input=step2_gauges_shape, output='step2_gauges', flags='o')
    except:
        sys.exit('------------> ERROR : Unable to import the gauges vector input')

    try:
        statement = "WtSID integer"
        grass_run_command('v.db.addcolumn', map='step2_gauges', columns=statement)
    except:
        sys.exit('------------> ERROR : Unable to add columns in the gauges vector input')
    # add id of watershed
    grass_run_command('v.what.vect', map='step2_gauges', query_map="hrus_v_cat2", query_column='watershed',
                      column='WtSID')
    # export step2_gauges_for_watersheds in stations.shp
    stations_shp = os.path.join(dir_results, 'stations.shp')
    grass_run_command('v.out.ogr', input='step2_gauges', type='point', format='ESRI_Shapefile', output=stations_shp)

    if str(parms.get('dams', 'to_do')) == 'yes':
        # get watershed id from hru to step2_dams_for_watersheds.shp
        step2_dams_shape = os.path.join(directory_out, 'step2_dams_for_watersheds.shp')
        try:
            grass_run_command('v.in.ogr', input=step2_dams_shape, output='step2_dams', flags='o')
        except:
            sys.exit('------------> ERROR : Unable to import the dams vector input')

        try:
            statement = "WtSID integer"
            grass_run_command('v.db.addcolumn', map='step2_dams', columns=statement)
        except:
            sys.exit('------------> ERROR : Unable to add columns in the dams vector input')
        # add id of watershed
        grass_run_command('v.what.vect', map='step2_dams', query_map="hrus_v_cat2", query_column='watershed',
                          column='WtSID')
        # export step2_dams_for_watersheds in dams.shp
        dams_shp = os.path.join(dir_results, 'dams.shp')
        grass_run_command('v.out.ogr', input='step2_dams', type='point', format='ESRI_Shapefile', output=dams_shp)

    # if irrigation, select surface point and find the nearest reach
    # if irrigation, select gw point and find the nearest HRU
    gauges_col_name = parms.get('gauges', 'gauges_col_name')
    if str(parms.get('irrigation', 'to_do')) == 'yes':
        # import step2_gauges_for_watersheds point into GRASS

        irrig_shape = os.path.join(directory_out, 'irrigation_selected.shp')
        try:
            grass_run_command('v.in.ogr', input=irrig_shape, output='irrigation', flags='o')
        except:
            sys.exit('------------> ERROR : Unable to import the irrigation vector input')
        try:
            statement = "ReachID integer,GUID integer," + gauges_col_name + " integer"
            grass_run_command('v.db.addcolumn', map='irrigation', columns=statement)
        except:
            sys.exit('------------> ERROR : Unable to add columns in the irrigation vector input')
            # v.distance from nearest reachs and update column
        irrig_col_type = parms.get('irrigation', 'irrig_col_type')
        irrig_col_name = parms.get('irrigation', 'irrig_col_name')
        irrig_distance_GU = parms.get('irrigation', 'irrig_distance_GU')

        # for irrig_col_type, 2 for groundwater and 3 for surface
        grass_run_command('v.distance', _from='irrigation', to='streams_shp', from_type='point', to_type='line',
                          upload='to_attr', column='ReachID', to_column='cat')

        statement = "UPDATE irrigation SET ReachID=0 WHERE "
        statement += irrig_col_type + " = 2"
        grass_run_command('db.execute', sql=statement)

        irrig_surf_min_GU = parms.get('irrigation', 'irrig_surf_min_GU')
        irrig_col_min_GU = parms.get('irrigation', 'irrig_col_min_GU')
        if irrig_surf_min_GU == '' and irrig_col_min_GU == '':
            grass_run_command('v.what.vect', map='irrigation', column='GUID', query_map='hrus_v_cat2',
                              query_column='cat')
        else:
            surfaceGlobal = 0
            if irrig_surf_min_GU != '':
                surfaceGlobal = float(irrig_surf_min_GU)

            surfaceSpatialized = False
            if irrig_col_min_GU != '':
                surfaceSpatialized = True
            # found the nearest HRU to irrigation point souterrain within à 5 km distance
            statement = irrig_col_type + " = 2"
            irrig_gu_ids = decode(grass_read_command('v.db.select', flags='c', map='irrigation', columns=irrig_col_name,
                                                     where=statement)).rstrip(os.linesep).split(os.linesep)

            # test if irrigation_GU has features

            for point in irrig_gu_ids:
                statement = irrig_col_name + " = " + point
                grass_run_command('v.extract', input='irrigation', type='point', output='irrigation_GU',
                                  where=statement)
                irrig_gu_dist = decode(
                    grass_read_command('v.distance', flags='pa', _from='irrigation_GU', to='hrus_v_cat2',
                                       from_type='point', to_type='area', upload='dist,to_attr', to_column='area',
                                       dmax=irrig_distance_GU)).rstrip(os.linesep).split(os.linesep)
                # remove header from_cat|to_cat|dist|to_attr from the list
                irrig_gu_dist.pop(0)
                dist_threshold = float(irrig_distance_GU)
                id_GU = 0

                # find nearest HRU and with area > threshold
                for item in irrig_gu_dist:
                    itemsplit = item.split("|")
                    # test if threshold is spatialized
                    surfaceThreshold = -1
                    if surfaceSpatialized:
                        statement = "cat = " + itemsplit[0]
                        surfaceThreshold = decode(
                            grass_read_command('v.db.select', flags='c', map='irrigation', columns=irrig_col_min_GU,
                                               where=statement)).rstrip(os.linesep).split(os.linesep)
                    # use global Threshold
                    if surfaceThreshold <= 0:
                        surfaceThreshold = surfaceGlobal

                    if float(itemsplit[2]) < dist_threshold and float(itemsplit[3]) >= surfaceThreshold:
                        dist_threshold = float(itemsplit[2])
                        id_GU = int(itemsplit[1])
                # update attribute table of irrigation

                grass_run_command('v.db.update', map='irrigation', column='GUID', value=id_GU, where=statement)
                grass_run_command('g.remove', flags='f', type='vector', name='irrigation_GU',
                                  stdout=DEVNULL, stderr=DEVNULL)

        # add id of watershed
        grass_run_command('v.what.vect', map='irrigation', query_map="hrus_v_cat2", query_column='watershed',
                          column=gauges_col_name)
        irrig_shp = os.path.join(dir_results, 'irrigation.shp')
        grass_run_command('v.out.ogr', input='irrigation', type='point', format='ESRI_Shapefile', output=irrig_shp)

        # identification of irrigated HRUs
        # import irrig_sector layer
    # sector_shape = os.path.join(directory_out, 'step2_gauges_for_watersheds.shp')
    # try:
    #     grass_run_command('v.in.ogr', input=irrig_shape, output='irrigation', flags='o')
    # except:
    #     sys.exit('------------> ERROR : Unable to import the irrigation vector input')

    # update hrus_c_pnt
    # grass_run_command('db.execute', sql='ALTER TABLE hrus_c_pnt ADD
    # IrrigatedSourceSurface INTEGER, IrrigatedSourceGU INTEGER')

    if (parms.get('topology', 'hru_no_topology_log')) == 'yes':
        print('------------- Export HRU with no topology in  hru_with_no_toology.par -------------')
        # export in csv the hru with no topology (no downstream reach and hru)
        notopology_file = os.path.join(dir_results, 'hru_with_no_toology.par')
        grass_run_command('v.db.select', flags='c', map='hrus_c_pnt', columns='cat',
                          where='to_reach IS NULL AND to_poly IS NULL', file=notopology_file, overwrite=True)

    # optim
    vals_file = os.path.join(tmpPath, 'vals_file')
    unsortedValsPath = os.path.join(tmpPath, 'unsorted_vals_file')
    grass_run_command('db.select', flags='c', table='hrus_c_pnt', sql='SELECT %s from %s' % (sql, 'hrus_c_pnt'),
                      output=unsortedValsPath, overwrite=True)
    # os.system('cat %s/unsorted_vals_file | sort -n -u > %s/vals_file' % (tmpPath, tmpPath))

    df = pd.read_table(unsortedValsPath, sep='|', header=None, dtype={3: str, 5: str, 6: str})
    df.sort_values(0, inplace=True)
    # keep in mind the result is slightly different than the 'unix sort' one because different duplicates are kept
    df.drop_duplicates(0, inplace=True)
    df.to_csv(vals_file, header=False, index=False, sep='\t')

    # Concatenate header and data
    f.write(header + '\n')
    f.write(header_min_val + '\n')
    f.write(header_max_val + '\n')
    f.write(header_unit + '\n')
    f.close()
    footer = '# end of hru.par'

    par = os.path.join(dir_results, 'hru.par')
    headerPath = os.path.join(tmpPath, 'header')
    # os.system("echo '%s' | cat %s/header %s/vals_file - | tr '|' '\t' > %s" % (footer, tmpPath, tmpPath, par))
    with open(par, 'w') as p:
        with open(headerPath, 'r') as head:
            for L in head:
                p.write(L)
        with open(vals_file, 'r') as vals:
            for L in vals:
                p.write(L)
        p.write(footer)

    if (parms.get('topology', 'OF_domain_export')) == 'yes':
        print('------------- Export HRU and reach into OpenFLUID format domain.fluidx -------------')
        # export in fluidx the topology of HRU and reach

        # open hru.par
        # skip 5 line
        # read first column (cat) and column 13 (to_poly) and column 14 (to_reach)
        # test if column 12 and colum 13 not Null or not 0.0
        domain_file = os.path.join(dir_results, 'domain.fluidx')
        fo = open(domain_file, "w")
        fo.write("<?xml version=\"1.0\" standalone=\"yes\"?>\n")
        fo.write("<openfluid>\n")
        fo.write(" <domain>\n")
        fo.write("  <definition>\n")
        # for loop for each class unit
        # read hru.par
        hru_file = open(par, 'r')
        hru_Lines = hru_file.readlines()
        for i, line in enumerate(hru_Lines):
            if i > 4:
                stripped_line = line.strip()
                line_list = stripped_line.split()
                if isint(line_list[0]):

                    fo.write(" <unit class=\"HRU\" ID=\"%s\" pcsorder=\"1\">\n" % (line_list[0]))
                    # get the length of list
                    if len(line_list) > 12:
                        if float(line_list[12]) > 0:
                            fo.write(" <to class=\"HRU\" ID=\"%i\" />\n" % (int(float(line_list[12]))))
                    if len(line_list) > 13:
                        if float(line_list[13]) > 0:
                            fo.write(" <to class=\"Reach\" ID=\"%s\" />\n" % (int(float(line_list[13]))))
                    fo.write(" </unit>\n")
        hru_file.close()
        # for loop for each class unit
        # read reach.par
        reach_file = open(reach_par, 'r')
        reach_Lines = reach_file.readlines()
        for j, line in enumerate(reach_Lines):
            if j > 4:
                stripped_line = line.strip()
                line_list = stripped_line.split()
                if isint(line_list[0]):

                    fo.write(" <unit class=\"Reach\" ID=\"%s\" pcsorder=\"1\">\n" % (line_list[0]))
                    if float(line_list[1]) > 0:
                        fo.write(" <to class=\"Reach\" ID=\"%s\" />\n" % (line_list[1]))
                    fo.write(" </unit>\n")
        reach_file.close()

        fo.write("  </definition>\n")
        fo.write("  <calendar>\n")
        fo.write("  </calendar>\n")
        fo.write(" </domain>\n")
        fo.write("</openfluid>\n")
        fo.close()

    if (parms.get('hru_param', 'hru_cat')) == 'yes':
        grass_run_command('r.out.gdal',
                          input=hrus, output=os.path.join(dir_results, 'hru_cat.tif'),
                          overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    if (parms.get('hru_param', 'hru_landuse')) == 'yes':
        grass_run_command('r.mapcalc', expression='hrulanduse = if(hrus,landuse)', overwrite='True')
        grass_run_command('r.out.gdal',
                          input='hrulanduse', output=os.path.join(dir_results, 'hru_landuse.tif'),
                          overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    save_config = os.path.join(directory_out, 'hrudelin_config_save.cfg')
    # os.system('cp "%s" "%s" ' % (parms_file, save_config))
    shutil.copyfile(parms_file, save_config)

    # running irrigation programs in R using pyGate by Theo L.

    # execFile = pyGate.ExecFile("/home/michael.rabotin/1_HYBV/HRU_DELIN/hru-delin-dev/rScript/",
    # "MDR_areaselect_irrigated_HRUs.r", "Rscript ")
    # sndStub = pyGate.SenderStub(execFile)
    # dictionaire = pyGate.Dictionary("irrigation_files")
    # dictionaire.addParameter("output_dir", dir_results)
    # dictionaire.addParameter("hruFile", parms.get("irrigation_analysis", "HRU_file"))
    # #passe le chemin depuis le fichier de configuration du fichier HRU.dbf
    # dictionaire.addParameter("cantonFile", parms.get("irrigation_analysis", "cantons_file"))
    # #passe le chemin depuis le fichier de configuration du fichier cantons.dbf

    # sndStub.dictionaries.append(dictionaire)
    # sndStub.run("/home/michael.rabotin/1_HYBV/HRU_DELIN/hru-delin-dev/gateway-main/gateway/build/", "gate.exe")

    if not generator:
        iterable3.update(1)
        iterable3.close()
        print()
    else:
        yield 100

    print('------------- HRU-delin parms_J2000 Ended -------------',
          time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime()))


if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv, \
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    from circleKill import circleKill
    from reach import buildReachPar
    from awk import *

    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'
    nbProcArg = ''
    if len(sys.argv) > 1:
        parms_file = sys.argv[1]
        if len(sys.argv) > 2:
            nbProcArg = sys.argv[2]
    # determine how many processes we can launch
    if str(nbProcArg).isnumeric() and int(nbProcArg) > 0:
        nbProc = int(nbProcArg)
    else:
        nbProc = cpu_count()

    res = list(main(parms_file, nbProc, False))

    try:
        os.system('notify-send "hru-delin-6-2-1 step 4 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv, \
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
    from .circleKill import circleKill
    from .reach import buildReachPar
    from .awk import *
