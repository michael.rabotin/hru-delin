from .argument import *


class File(Serializable):
	def __init__(self, path: str, name: str):
		self.name = name
		self.path = path
		self.actif = True

	def serialize(self) -> dict:
		if self.actif:
			return {
				"File": {
					"name": self.name,
					"path": self.path
				}
			}
		return {}

	def display(self) -> str:
		if self.actif:
			json_line = json.dumps(self.serialize(), indent=2)
			print(json_line)
			return json_line
		return ""


class ExecFile(File):
	def __init__(self, path: str, name: str, cmd: str):
		super().__init__(path, name)
		self.cmd = cmd

	def serialize(self) -> dict:
		if self.actif:
			dico = super().serialize()
			dico["ExecFile"] = dico.pop("File")
			dico["ExecFile"]["cmd"] = self.cmd

			return dico
		return {}

	def display(self) -> str:
		if self.actif:
			json_line = json.dumps(self.serialize(), indent=2)
			print(json_line)
			return json_line
		return ""


class OutputFile(File):
	def __init__(self, path: str, name: str):
		super().__init__(path, name)

	def displayContent(self):
		if self.actif:
			json_line = json.dumps(self.read(), indent=2)
			print(json_line)
			return json_line
		return ""

	def readAsDictionary(self) -> Dictionary:
		if self.actif:
			dictionary_dict = Dictionary("outputFile")

			file = self.read()
			output_dico = Dictionary("Outputs")
			for output in file["Outputs"]:
				dico = Dictionary(output["Dictionary"]["name"])
				dico.deserialize(output["Dictionary"])
				output_dico.addArgument(dico)

			dictionary_dict.addArgument(output_dico)

			error_dico = Dictionary("Error")
			error_dico.addParameter("code", file["Error"]["code"])
			error_dico.addParameter("description", file["Error"]["description"])
			error_dico.addParameter("traceback", file["Error"]["traceback"])

			dictionary_dict.addArgument(error_dico)

			check_dico = Dictionary("Check")
			for dataFile in file["Check"]:
				dataFile_dico = Dictionary(dataFile["DataFile"]["fileName"])
				dataFile_dico.addParameter("name", dataFile["DataFile"]["fileName"])
				dataFile_dico.addParameter("nbMissing", dataFile["DataFile"]["nbMissing"])
				dataFile_dico.addParameter("missing", dataFile["DataFile"]["missing"])
				dataFile_dico.addParameter("nbIncorrect", dataFile["DataFile"]["nbIncorrect"])
				dataFile_dico.addParameter("incorrect", dataFile["DataFile"]["incorrect"])

				check_dico.addArgument(dataFile_dico)

			dictionary_dict.addArgument(check_dico)

			return dictionary_dict
		return Dictionary("")

	def read(self) -> dict:
		if self.actif:
			file = open(self.path + self.name, 'r')
			lines = file.readlines()
			file.close()

			json_file = "\n".join(lines)
			return json.loads(json_file)
		return {}

	def writeOutput(self, dictionary: Dictionary):
		if self.actif:
			file = self.read()
			outputs = file["Outputs"]
			outputs.append(dictionary.serialize())
			file["Outputs"] = outputs

			file_json = json.dumps(file, indent=2)
			file = open(self.path + self.name, 'w')
			file.write(file_json)
			file.close()

	def serialize(self) -> dict:
		if self.actif:
			dico = super().serialize()
			dico["OutputFile"] = dico.pop("File")

			return dico
		return {}

	def display(self) -> str:
		if self.actif:
			json_line = json.dumps(self.serialize(), indent=2)
			print(json_line)
			return json_line
		return ""
