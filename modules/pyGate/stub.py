import subprocess
from typing import List
import sys

from .file import *


class Stub:
	def __init__(self, outputFile: "OutputFile | None"):
		if outputFile is None:
			outputFile = OutputFile("", "")

		self.actif = True
		self.outputFile: "OutputFile" = outputFile
		self.dictionaries: List[Dictionary] = []

	def findArgumentWithName(self, name: str):
		if self.actif:
			dico = None
			for dictionary in self.dictionaries:
				dico = dictionary.getArgument(name)
				if dico is not None:
					break

			return dico
		return None

	def getArgument(self, name: str):
		if self.actif:
			return self.findArgumentWithName(name)
		return None

	def findDictionaryWithName(self, name: str):
		if self.actif:
			for dico in self.dictionaries:
				if dico.name == name:
					return dico

		return None

	def displayDictionaries(self) -> str:
		if self.actif:
			dictionaries = []
			dico = {}

			for dictionary in self.dictionaries:
				dictionaries.append(dictionary.serialize())

			dico["Dictionaries"] = dictionaries
			json_line = json.dumps(dico, indent=2, sort_keys=True)

			print(json_line)
			return json_line
		return ""

	def displayOutputFile(self) -> str:
		if self.actif:
			json_line = json.dumps(self.outputFile.serialize(), indent=2, sort_keys=True)

			print(json_line)
			return json_line
		return ""

	def displayAll(self) -> str:
		if self.actif:
			return self.displayOutputFile() + self.displayDictionaries()
		return ""


class SenderStub(Stub):
	def __init__(self, execFile=None, outputFile=None):
		super().__init__(outputFile)
		self.execFile: ExecFile = execFile

	def run(self, gatePath: str, gateName: str):
		print(" =============== Running gateway =============== ")
		subprocess.run(gatePath + gateName + " '" + self.serialize() + "'", shell=True)
		print(" =============== Gateway ending ================ ")

	def serialize(self) -> str:
		dico = {
			"ExecFile": self.execFile.serialize()["ExecFile"],
			"OutputFile": self.outputFile.serialize()["OutputFile"]
		}

		dictionaries = []

		for dictionary in self.dictionaries:
			dictionaries.append(dictionary.serialize())

		dico["Dictionaries"] = dictionaries

		return json.dumps(dico)

	def displayExecFile(self) -> str:
		json_line = json.dumps(self.execFile.serialize(), indent=2, sort_keys=True)

		print(json_line)
		return json_line

	def displayAll(self) -> str:
		return self.displayExecFile() + super().displayAll()


class ReceiverStub(Stub):
	def __init__(self):
		super().__init__(None)
		self.actif = True

		self.deserialize(self.readArguments())

	def readArguments(self) -> str:
		if len(sys.argv) > 1:
			return str(sys.argv[1])
		self.actif = False
		self.outputFile.actif = False

	def deserialize(self, json_line: str) -> None:
		if self.actif:
			data_line = json.loads(json_line)

			self.outputFile = OutputFile(data_line["OutputFile"]["path"], data_line["OutputFile"]["name"])

			dictionaries_json = data_line["Dictionaries"]

			for dictionary_json in dictionaries_json:
				dictionary = Dictionary(dictionary_json["Dictionary"]["name"])
				dictionary.deserialize(dictionary_json["Dictionary"])

				self.dictionaries.append(dictionary)
