#!/usr/bin/env python3
# -*- coding: utf-8 -*-

############################################################################
#
# MODULE:       reach.py
# AUTHOR(S):    Julien Veyssier
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################





import sys, os, shutil
import time
#import grass.script as grass
from grass.script.utils import decode, encode
import multiprocessing
from multiprocessing import Pool, cpu_count
from utils import isint, write_log
import csv
from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr
import numpy as np
import math
# trick to always be able to import something next to me (QGIS context in particular)
MY_ABS_PATH=os.path.abspath(__file__)
MY_DIR=os.path.dirname(MY_ABS_PATH)
sys.path.append(MY_DIR)
from grassUtils import grass_run_command, grass_parse_command, grass_feed_command, grass_read_command

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

# KNOWN BUGS:
# - IF ONLY ONE REACH EXISTS, REACH.PAR IS EMPTY

def getOutletReachID(topo):
    for i in topo.values():
        if i not in topo.keys():
            return i

def getTopo(inp_vals):
    topo_dict = {}

    # CHANGE NULL VALUE TO 9999
    grass_run_command('r.null', map='%(streams)s' % inp_vals, null=9999, stdout=DEVNULL, stderr=DEVNULL)

    # GET JUNCTION OF REACH (END POINT)
    grass_run_command('r.mapcalc',
        expression='map_a=if(!(isnull(%(ws)s)),eval(m=min(%(streams)s[-1,1],%(streams)s[1,1],%(streams)s[1,-1],%(streams)s[-1,-1],%(streams)s[0,1],%(streams)s[1,0],%(streams)s[-1,0],%(streams)s[0,-1]),if((m!=%(streams)s),%(streams)s,null())))' % inp_vals,
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.null', map='map_a', setnull='9999,0', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.mapcalc',
        expression='map_b=if((map_a),if((%(drain)s==1&&%(streams)s[-1,1]!=map_a),%(streams)s[-1,1],if((%(drain)s==2&&%(streams)s[-1,0]!=map_a),%(streams)s[-1,0],if((%(drain)s==3&&%(streams)s[-1,-1]!=map_a),%(streams)s[-1,-1],if((%(drain)s==4&&%(streams)s[0,-1]!=map_a),%(streams)s[0,-1],if((%(drain)s==5&&%(streams)s[1,-1]!=map_a),%(streams)s[1,-1],if((%(drain)s==6&&%(streams)s[1,0]!=map_a),%(streams)s[1,0],if((%(drain)s==7&&%(streams)s[1,1]!=map_a),%(streams)s[1,1],if((%(drain)s==8&&%(streams)s[0,1]!=map_a),%(streams)s[0,1],null())))))))))' % inp_vals,
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.cross',
        flags='z',
        input='map_a,map_b',
        output='map_c',
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    # don't null 0 because now it's a valid category (new behaviour of r.cross -z)
    # and the old cat zero is not there anymore
    #grass_run_command('r.null', map='map_c', setnull='0', stdout=DEVNULL, stderr=DEVNULL)

    # this is a fix for grass74 which fails to produce categories for the first line of r.cross result
    # so we take it manually
    a = decode(grass_read_command('r.stats', quiet=True, flags='lnN', input='map_a')).rstrip(os.linesep).split(os.linesep)[0]
    b = decode(grass_read_command('r.stats', quiet=True, flags='lnN', input='map_b')).rstrip(os.linesep).split(os.linesep)[0]

    topo = decode(grass_read_command('r.stats', quiet=True, flags='lnN', input='map_c')).rstrip(os.linesep).split(os.linesep)
    for relation in topo:
        relStripped = relation.strip()
        if relStripped != '':
            # this only happens with grass74
            if relStripped == '0':
                from_node = a.strip()
                to_node = b.strip()
                topo_dict[from_node] = to_node
            else:
                from_node = int(relation.split(' ')[2].rstrip(';'))
                to_node = int(relation.split(' ')[4])
                topo_dict[from_node] = to_node
    outletReachID = getOutletReachID(topo_dict)
    topo_dict[outletReachID] = 0
    return topo_dict

def processReach(params):
    id, path, streams, dem, nbProc = params
    # define which grass env we are using
    current = multiprocessing.current_process()
    processN = int(current._identity[0])
    # because multiprocess does not reset workers id numbers and we still use [1, N]
    processN = (processN % nbProc) + 1
    location = 'hru-delin_%s' % (processN)
    os.environ['GISRC'] = os.path.join(path, 'grass_db', 'grassdata', location, '.grassrc')

    # CALC LENGTH OF REACH
    # TODO check why s1 and s13 are not used
    s1 = decode(grass_read_command('r.mask',
        raster=streams, maskcats='%s' % id,
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL
    )).rstrip(os.linesep).split(os.linesep)

    s13 = decode(grass_read_command('r.to.vect',
        flags='v', input='MASK', output='reach_%s' % id, type='line',
        overwrite='True'
    )).rstrip(os.linesep).split(os.linesep)[0]

    report_length = decode(grass_read_command('v.to.db',
        flags='p', map='reach_%s' % id, option='length', columns='s'
    )).rstrip(os.linesep).split(os.linesep)
    try:
        length = round(float(report_length[1].split('|')[1]))
    except Exception:
        # REACH IS A POINT STUPIDLY
        length = 1.0

    # SINUOUSITY
    report_sin = decode(grass_read_command('v.to.db',
        flags='p', map='reach_%s' % id, option='sinuous', columns='s'
    )).rstrip(os.linesep).split(os.linesep)
    try:
        sin = round(float(report_sin[1].split('|')[1]), 2)
    except Exception:
        sin = 1.0

    # CALC SLOPE
    range = decode(grass_read_command('r.describe',
        flags='dr', map=dem
    )).rstrip(os.linesep).split(os.linesep)[0].split('thru')
    try:
        min = int(range[0])
        max = int(range[1])
        #slp_dict[id] = round((max-min)/len_dict[id],4)*100.0
        slp = round((max-min) / length * 100.0, 4)
    except Exception:
        # CASE MIN=MAX IF REACH IS POINT
        slp = 0.0
    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)
    return (id, length, sin, slp)

def buildReachPar(streamsP, watershedP, drainageP, demP, outputP, pathP, nbProcP, generator):
    if not generator:
        from tqdm import tqdm
    #DEFINE RANGES FOR HEADER
    ranges = {
        'ID': [0, 999999, 'n/a'],
        'to-reach': [0, 999999, 'n/a'],
        'length': [0, 99999, 'm'],
        'slope': [0, 90, '%'],
        'rough': [0, 9999, 'n/a'],
        'width': [0, 9999, 'm'],
        'sinuous': [1, 999999, 'n/a']
    }

    inp_vals = {
        'streams': streamsP,
        'ws': watershedP,
        'drain': drainageP
    }

    grass_run_command('r.mapcalc',
        expression='streams_in_ws=if((%s),%s,null())' % (watershedP, streamsP),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    inp_vals['streams'] = 'streams_in_ws'

    grass_run_command('g.region', raster='%(ws)s' % inp_vals, stdout=DEVNULL, stderr=DEVNULL)

    # save projection for reach.par
    regionEPSG = decode(grass_read_command('g.proj', flags='g')).split()
    matchingEPSG = ''.join([s for s in regionEPSG if "EPSG" in s])

    # -t option of r.info does not exist anymore and it was sketchy anyway
    # now we get maptype in a safer way by checking every output lines of r.info
    infos = decode(grass_read_command('r.info', quiet=True, flags='g', map=demP)).rstrip(os.linesep).split(os.linesep)
    mapType = None
    for info in infos:
        if info.startswith('datatype'):
            mapType = info.split('=')[1].strip()
    if mapType == None:
        sys.exit('impossible to get map type')
    if (mapType == 'CELL'):
        #print('DEM already as CELL map...ok!')
        inp_vals['dem'] = demP
    else:
        #print('DEM as FCELL map...convert to CELL...')
        grass_run_command('r.mapcalc',
            expression='dem_int=int(%s)' % demP,
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        inp_vals['dem'] = 'dem_int'

    # GET TOPOLOGY INFORMATION
    topo_dict = getTopo(inp_vals)
    #print('=== '+str(topo_dict))

    # SET NULL VALUE BACK TO DEFAULT
    grass_run_command('r.null', map='%(streams)s' % inp_vals, setnull='0,9999', stdout=DEVNULL, stderr=DEVNULL)
    # GET REACH IDs
    reachIDs = decode(grass_read_command('r.category', map='%(streams)s' % inp_vals, stdout=DEVNULL, stderr=DEVNULL)).rstrip(os.linesep).split(os.linesep)
    # if there is only one reach, it does not make sense to get topology
    # we just put the reach as outlet
    if len(reachIDs) == 1:
        topo_dict = {}
        intReachId = int(reachIDs[0].strip())
        topo_dict[intReachId] = 0
    #print('reach ids: %s' % reachIDs)
    len_dict = {}
    slp_dict = {}
    sin_dict = {}
    rough_const = '30'
    width_const = '1'

    ########### main loop
    #for reach in topo_dict.keys():
    path = pathP

    # export rasters that are necessary for parallel environments
    rastersForWorkers = {
        inp_vals['streams']: os.path.join(path, 'tmp', 'r.r_streams.tif'),
        inp_vals['dem']: os.path.join(path, 'tmp', 'r.r_dem.tif'),
    }
    for name in rastersForWorkers:
        grass_run_command('r.out.gdal',
            input=name,
            output=rastersForWorkers[name],
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # save main grass env which is being overriden later
    MAIN_GISRC = os.environ['GISRC']

    # determine number of processes
    nbProcArg = nbProcP
    if str(nbProcArg).isnumeric() and int(nbProcArg) > 0:
        nbProc = int(nbProcArg)
    else:
        nbProc = cpu_count()

    # build the environments and load exported rasters in each of them
    grassDbPath = os.path.join(path, 'grass_db')
    for i in range(nbProc):
        # location has already been built by step4
        location = 'hru-delin_%s' % (i+1)

        os.environ['GISRC'] = os.path.join(grassDbPath, 'grassdata', location, '.grassrc')
        # set projection
        fp = os.path.join(path, 'tmp', 'r.r_dem.tif')
        grass_run_command('g.proj', flags='c', georef=fp, stdout=DEVNULL, stderr=DEVNULL)
        # import rasters in envs
        for name in rastersForWorkers:
            grass_run_command('r.in.gdal', flags='o', input=rastersForWorkers[name], output=name, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    reach_ids = topo_dict.keys()
    nbReachs = len(reach_ids)
    print()
    COLOR_GREEN='\x1b[32m'
    STYLE_BRIGHT='\x1b[1m'
    STYLE_RESET='\x1b[0m'
    l_bar1 = '{desc}%s%s{percentage:3.0f}%%%s|' % (STYLE_BRIGHT, COLOR_GREEN, STYLE_RESET)
    r_bar1 = '| %s%s{n_fmt}/{total_fmt}%s [{elapsed}<{remaining}, {rate_fmt}{postfix}]' % (STYLE_BRIGHT, COLOR_GREEN, STYLE_RESET)
    bar_format1='%s%s{bar}%s%s' % (l_bar1, COLOR_GREEN, STYLE_RESET, r_bar1)
    # the locks are here to prevent concurrent terminal tqdm writing
    # this is the interesting part, launching N processes in parallel to process basins
    if generator:
        with Pool(nbProc) as p:
            params = [(id, path, inp_vals['streams'], inp_vals['dem'], nbProc) for (i, id) in enumerate(reach_ids)]
            results = []
            for i, _ in enumerate(p.imap_unordered(processReach, params), 1):
                results.append(_)
                yield (i/nbReachs*100)
    else:
        with Pool(nbProc, initializer=tqdm.set_lock, initargs=(tqdm.get_lock(),)) as p:
            params = [(id, path, inp_vals['streams'], inp_vals['dem'], nbProc) for (i, id) in enumerate(reach_ids)]
            results = list(tqdm(p.imap_unordered(processReach, params),
                desc='[main process] Loop on reaches [%s process] ' % nbProc,
                total=nbReachs,
                unit='reach',
                bar_format=bar_format1
            ))
        print()

    # merge results
    for r in results:
        id = r[0]
        len_dict[id] = r[1]
        sin_dict[id] = r[2]
        slp_dict[id] = r[3]

    # restore main grass env
    os.environ['GISRC'] = MAIN_GISRC

    # CREATE .PAR FILE
    with open(outputP, 'w') as outfile:
        timestr = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
        outfile.write('# reach.par created at %s, r.reach.par called by HRU-delin,  ' % timestr)
        outfile.write('projection system is %s \n' % matchingEPSG)
        outfile.write('ID\tto-reach\tlength\tslope\tsinuosity\trough\twidth\n')
        outfile.write(str(ranges['ID'][0])+'\t'+str(ranges['to-reach'][0])+'\t'+str(ranges['length'][0])+'\t'+str(ranges['slope'][0])+'\t'+str(ranges['sinuous'][0])+'\t'+str(ranges['rough'][0])+'\t'+str(ranges['width'][0])+'\n')
        outfile.write(str(ranges['ID'][1])+'\t'+str(ranges['to-reach'][1])+'\t'+str(ranges['length'][1])+'\t'+str(ranges['slope'][1])+str(ranges['sinuous'][1])+'\t'+str(ranges['rough'][1])+'\t'+str(ranges['width'][1])+'\n')
        outfile.write(ranges['ID'][2]+'\t'+ranges['to-reach'][2]+'\t'+ranges['length'][2]+'\t'+ranges['slope'][2]+'\t'+ranges['sinuous'][2]+'\t'+ranges['rough'][2]+'\t'+ranges['width'][2]+'\n')
        for i in topo_dict.keys():
            outfile.write(str(i)+'\t'+str(topo_dict[i])+'\t'+str(len_dict[i])+'\t'+str(slp_dict[i])+'\t'+str(sin_dict[i])+'\t'+rough_const+'\t'+width_const+'\n')
        outfile.write('# end of reach.par')


 
def snapping_points_to_reaches(parms, directory_out,col_name,col_area,reloc_lyr,reloc_file_name,pointType):
    """
    Relocates gauges or dams on the reaches derivated from MNT in step 1 (r.watershed)
    """
    basin_min_size = int(parms.get('basin_min_size', 'size'))
    surface_ok_1 = int(parms.get('auto_relocation', 'surface_tolerance_1'))
    dist_ok_1 = int(parms.get('auto_relocation', 'distance_tolerance_1' ))

    if parms.get('auto_relocation', 'surface_tolerance_2') == '':
        nb_trials = 1
        surface_ok_2 = 0
        dist_ok_2 = 0
    else:
        nb_trials = 2
        if not isint(parms.get('auto_relocation', 'surface_tolerance_2')):
                sys.exit('------------> ERROR : Surface_tolerance_2 is not integer' ) 
        if not isint(parms.get('auto_relocation', 'distance_tolerance_2')):
                sys.exit('------------> ERROR : Distance_tolerance_2 is not integer' ) 
        surface_ok_2 = int(parms.get('auto_relocation', 'surface_tolerance_2'))
        dist_ok_2 = int(parms.get('auto_relocation', 'distance_tolerance_2' ))

    area_unit = parms.get('auto_relocation', 'area_unit')
    # are these the right factors?
    # if values is entered in square meters, fact should be 1, no?
    if area_unit == '1':
        fact = 1000
        unit = 'm2'
    elif area_unit == '2':
        fact = 1000000
        unit = 'km2'
    else:
        print(' =========> ERROR ')
        sys.exit(' =========> CHECK area_unit PARAMETER')

    
    logfile_n = os.path.join(directory_out, reloc_file_name)
    logfile = open(logfile_n, 'w')
    fieldnames = ['point code', 'point type','point area', 'rule', 'horiz shift', 'horiz dir', 'vertic shift', 'vertic dir']
    logf = csv.DictWriter(logfile, fieldnames=fieldnames, delimiter=';')
    logf.writeheader()


    # Read the accumulation raster and get the geo informations
    accum_file = os.path.join(directory_out, 'step1_accum.tif')
    accum_lyr = gdal.Open(accum_file, GA_ReadOnly)
    accum_bd = accum_lyr.GetRasterBand(1)
    georef = accum_lyr.GetGeoTransform()
    hres = georef[1]
    vres = georef[5]
    pixel_sz = (hres * abs(vres))
    # It gets the number of rows and columns of the accumulation raster file
    acc_rast_cols = accum_lyr.RasterXSize
    acc_rast_rows = accum_lyr.RasterYSize

    # Matrix of euclidian distances from the center (gauge position)
    max_dist = max(dist_ok_1, dist_ok_2)
    euclid_dist = np.full((max_dist * 2 + 1, max_dist * 2 + 1), 0, dtype=int)
    for y in range(0, (max_dist * 2 + 1)):
        for x in range(0, (max_dist * 2 + 1)):
            euclid_dist[y, x] = int(math.sqrt((y - max_dist)**2 + (x - max_dist)**2))
   
  
       
    # Points loop
    for point in reloc_lyr:
        if not point.IsFieldSet(col_name):
            sys.exit('------------> ERROR : col_name not found' ) 
        point_code = point.GetField(col_name)
        # Get the drained surface of the point
        point_area = point.GetField(col_area)
        
        # ------------------------------------------------------------
        # if point_area < 0:
        #   nb_pixel_area = basin_min_size + 1
        #   ignore_surface_tolerance = True
        # else:
        #   nb_pixel_area = ((point_area * fact) / pixel_sz) + 0.5
        #   ignore_surface_tolerance = False
        # ------------------------------------------------------------
        # here, the corresponding number of cell of the specified drained area is calculated
        nb_pixel_area = ((point_area * fact) / pixel_sz) + 0.5
        # ------------------------------------------------------------

        if nb_pixel_area > basin_min_size:
            # Get the coordinates of the point and calculate center of the offset
            # I think it relocates the station in the center of the closest pixel
            # in the raster
            geom = point.GetGeometryRef()
            point_x, point_y = geom.GetX(), geom.GetY()
            Xreste = point_x % hres
            Yreste = point_y % abs(vres)
            new_point_x = (point_x - Xreste) + 1
            new_point_y = (point_y - Yreste) - 1
            
            # Read the currents of accum raster around the point
            # (new_point_x - georef[0]) / hres)
            # ==> this gives the column in raster file where the station has been relocated
            # int(((new_point_x - georef[0]) / hres) - (dist_max))
            # ==> get the difference with the maximum number of column of accepted differences
            #     if > 0 then, dist_max is < column number of point
            #     if < 0 then, dist_max is > column number of point
            # max(..., 0) ==> there, we just see if point is or not within a dist_max distance from left border???

            dist_max = max(dist_ok_1, dist_ok_2)
            pix_x = max(int(((new_point_x - georef[0]) / hres) - (dist_max)), 0)

            pix_y = max(int(((georef[3] - new_point_y) / abs(vres)) - (dist_max)), 0)

            #        and don't access beyond the accum raster bounds
            nb_cols = min((dist_max * 2) + 1, acc_rast_cols - pix_x)

            nb_rows = (min((dist_max * 2) + 1, acc_rast_rows - pix_y))
           
            zone = accum_bd.ReadAsArray(pix_x, pix_y, nb_cols, nb_rows).astype(int)
            # in the end, this is just to ensure that we won't look at relocating the gaug outside the raster boudaries
            # isn't that right?
            # zone contains the accumulation raster data within max_dist area

            # Matrix of differences
            zone_diff = abs(nb_pixel_area - abs(zone))
            # here, the differences between the actual drained area and the specified drained area is calculated

            # Prepare for first search
            # here accum_toler is the number of difference in pixel that can be tolerated
            accum_toler = int(nb_pixel_area * surface_ok_1 / 100)
            if dist_ok_1 > dist_ok_2:
                start = 0
                col_max = min(nb_cols, acc_rast_cols - pix_x)
                row_max = min(nb_rows, acc_rast_rows - pix_y)
                end = dist_ok_1 - start
            else:
                start = dist_ok_2 - dist_ok_1
                col_max = min(nb_cols - start, acc_rast_cols - pix_x)
                row_max = min(nb_rows - start, acc_rast_rows - pix_y)
                end = dist_ok_2 - start
            msg = 'shifted with first rule'
            
            trial = 1

            while trial <= nb_trials:
                # best_area is useless... not used anywhere...
                best_area = zone_diff.max()
                best_euclid_dist = euclid_dist[start, start]
                best_x = 0
                best_y = 0
                better_place_found = False
                
                for y in range(start, row_max):
                    for x in range(start, col_max):
                        # ------------------------------------------------------------
                        # if ignore_surface_tolerance:
                        #   if euclid_dist[y, x] < best_euclid_dist:
                        #        best_euclid_dist = euclid_dist[y, x]
                        #        best_y = y
                        #        best_x = x
                        #        better_place_found = True
                        # else:
                        #    if zone_diff[y, x] <= accum_toler:
                        #        if euclid_dist[y, x] < best_euclid_dist:
                        #            best_euclid_dist = euclid_dist[y, x]
                        #            best_y = y
                        #            best_x = x
                        #            better_place_found = True
                        # ------------------------------------------------------------
                        if zone_diff[y, x] <= accum_toler:
                            if euclid_dist[y, x] < best_euclid_dist:
                                # best_area is useless... not used anywhere...
                                best_area = zone_diff[y, x]
                                best_euclid_dist = euclid_dist[y, x]
                                best_y = y
                                best_x = x
                                better_place_found = True
                        # ------------------------------------------------------------
                if better_place_found:
                    new_x = ((pix_x + best_x) * hres) + georef[0] + (hres / 2)
                    new_y = georef[3] - ((pix_y + best_y) * abs(vres)) - (abs(vres) / 2)
                    wkt   = 'POINT(%f %f)' % (new_x , new_y)
                    pointWkt = ogr.CreateGeometryFromWkt(wkt)
                    point.SetGeometryDirectly(pointWkt)
                    reloc_lyr.SetFeature(point)
                    trial = 3
                    write_log(logf,msg, start, end, best_x, best_y,point_code,pointType,point_area)
                else:
                    if trial == 2:
                        logf.writerow({'point code': point_code, 'point type':pointType,'point area': str(point_area), 'rule': ' no better place found'})
                        trial = 3
                    else:
                        if nb_trials == 1:
                            logf.writerow({'point code': point_code, 'point type':pointType,'point area': str(point_area), 'rule': ' no better place found'})
                            trial = 3
                        else:
                            # Prepare for second search
                            msg = 'shifted with second rule'
                            accum_toler = int(nb_pixel_area * surface_ok_2 / 100)
                            trial = 2
                            
                            if dist_ok_2 > dist_ok_1:
                                start = 0
                                end = dist_ok_2 - start
                            else:
                                start = dist_ok_1 - dist_ok_2
                                col_max = min(nb_cols - start, acc_rast_cols - pix_x)
                                row_max = min(nb_rows - start, acc_rast_rows - pix_y )
                                end = dist_ok_1 - start
        else:
           pointID = point.GetFID()
           reloc_lyr.DeleteFeature(pointID)
           logf.writerow({'point code': point_code, 'point type':pointType,'point area': str(point_area), 'rule': ' basin too small ---> DELETED'})

    logfile.close



def cut_streams_at_points(directory_out, list_point,stream_in,stream_out):

    listReachID=[]

    """
    Streams generated at step 1 (by r.watershed) are cutted at the gauges or at the dams
    """
    streams_file = os.path.join(directory_out, stream_in)
    streams_lyr = gdal.Open(streams_file, GA_ReadOnly)
    streams_band = streams_lyr.GetRasterBand(1)
    stream_orig = streams_band.ReadAsArray()

    mask_file = os.path.join(directory_out, 'step2_mask.tif')
    mask_lyr = gdal.Open(mask_file, GA_ReadOnly)
    mask_band = mask_lyr.GetRasterBand(1)
    mask = mask_band.ReadAsArray()
    nodata = mask_band.GetNoDataValue()
    mask[mask == nodata] = 0

    stream_orig = stream_orig * mask

    streams_cols = streams_lyr.RasterXSize
    streams_rows = streams_lyr.RasterYSize
    georef = streams_lyr.GetGeoTransform()
    hres = georef[1]
    vres = georef[5]

    streams_new_file = os.path.join(directory_out, stream_out)
    driver = gdal.GetDriverByName('GTiff')
    dst_ds = driver.Create(streams_new_file, streams_cols, streams_rows, 1, GDT_Int32)
    dst_ds.SetGeoTransform((georef[0], hres, 0, georef[3], 0, vres))
    dst_ds.SetProjection(streams_lyr.GetProjection())

    drain_file = os.path.join(directory_out, 'step1_drain.tif')
    drain_lyr = gdal.Open(drain_file, GA_ReadOnly)
    drain = drain_lyr.GetRasterBand(1).ReadAsArray()
    drain_dir = [[-1,1],[-1,0],[-1,-1],[0,-1],[1,-1],[1,0],[1,1],[0,1]]

    # Give possibility of more than 2 segments in a stream
    stream_wk = np.zeros((streams_rows, streams_cols), dtype=('int64'))
    nodata = streams_band.GetNoDataValue()
    for y in range (0, streams_rows - 1):
        for x in range (0, streams_cols - 1):
            if stream_orig[y, x] != nodata:
                stream_wk[y, x] = stream_orig[y, x] * 100


    for point in list_point:
        current_x = int(((point[0] - georef[0]) / hres))
        current_y = int(((georef[3] - point[1]) / abs(vres)))
        current_stream = stream_orig[current_y, current_x]
       
        

        if current_stream == nodata:
            print('---------- gauge outside of stream network: ', point)
            end_of_stream = True
        else:
            stream_wk[current_y, current_x] += 1
            end_of_stream = False
            i = 0
            d = abs(drain[current_y, current_x])
        while not end_of_stream:
            if d == 8:
                d = 0
            y = current_y + drain_dir[d][0]
            x = current_x + drain_dir[d][1]
            d2 = abs(drain[y, x]) - 1
            drain_into_y = y + drain_dir[d2][0]
            drain_into_x = x + drain_dir[d2][1]
            if (stream_orig[y, x] == current_stream) and (drain_into_y == current_y) and (drain_into_x == current_x):
                stream_wk[y, x] += 1
                current_y = y
                current_x = x
                i = 0
                d = abs(drain[y, x])
            else:
                i += 1
                if i == 7:
                    end_of_stream = True
                else:
                    d += 1
    dst_ds.GetRasterBand(1).WriteArray(stream_wk)
    dst_ds = None

    for point in list_point:
        current_x = int(((point[0] - georef[0]) / hres))
        current_y = int(((georef[3] - point[1]) / abs(vres)))
       
        current_stream = stream_wk[current_y, current_x]
        tuple_ReachID=(point[2],current_stream)
        listReachID.append(tuple_ReachID) 
        



    return listReachID

def updateAttributeTable(directory_out,list_point,shapefile,colname):

    point_file = os.path.join(directory_out, shapefile)
    point_in = ogr.Open(point_file, 1)
    point_lyr = point_in.GetLayer()

    list_of_field=[]
    ldefn = point_lyr.GetLayerDefn()
    for n in range(ldefn.GetFieldCount()):
        fdefn = ldefn.GetFieldDefn(n)
        list_of_field.append(fdefn.name)

    fieldReach='ReachID'
    if fieldReach not in list_of_field:
        point_lyr.CreateField(ogr.FieldDefn('ReachID', ogr.OFTInteger))

    point_features = point_lyr.GetNextFeature()
        
    while point_features:
        pointID=point_features.GetField(colname)
       
        reachID=0
        for point in list_point:
            if (point[0]==pointID):
                reachID=point[1]
                break
       
        point_features.SetField('ReachID', int(reachID))
        point_lyr.SetFeature(point_features)
        point_features = point_lyr.GetNextFeature()
        
def processReachStats(params):
    id, configFileDir, nbProc = params
    # define which grass env we are using
    current = multiprocessing.current_process()
    processN = int(current._identity[0])
    # because multiprocess does not reset workers id numbers and we still use [1, N]
    processN = (processN % nbProc) + 1

    location = 'hru-delin_%s' % (processN)
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', location, '.grassrc')

    cleanId = id.strip()

    grass_run_command('r.mask', raster='reachraster', maskcats='%s' % cleanId, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    tmp_val = decode(grass_read_command('r.stats', quiet=True, flags='nN', input='cross1')).rstrip(os.linesep).split(os.linesep)
    return (cleanId, tmp_val[0].rstrip(os.linesep))
