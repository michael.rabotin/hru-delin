#***** SCRIPT déterminant la culture dominante à affecter à une HRU *******
#  ***   en fonction des données de culture irriguées du RGA et ***
#   *    du besoin en eau théorique de chaque culture présente   *
  
  
# auteur : IG
# date : 15-12-2015

# --------------------------------------------------------------------------
library(gdata)
library(lubridate)
library(foreign)

source('lib/utilitaire_irrigation.R')


# 1. Rassembler les différentes sources de données 
# ************************************************

# RGA
#--------------

RGA <- read.xls('~/Documents/MDR/irrigation/RGACultures2010_Cantons_BVRhone_sanssecret_20131001.xlsx', sheet = 3)
cantons <- RGA[, 1]
cultures <- c('Vigne', 'Mais', 'Tournesol', 'Blé dur', 'maraichage', 'PdT', 'Vergers', 'Prairies', 'Protéagineux', 'Riz', "Jachère", "Divers", "Industrielles")
RGA <- RGA[, c(19:26, 28, 30, 31, 29, 27)]# colonnes irriguées, 13 types de culture. ACGTUNG !!! les colonnes ne sont pas dans le meme ordre que dans J2000 !!
rownames(RGA) <- cantons
colnames(RGA) <- cultures


# param de J2000
#-----------------

# nom abrégé et numéro des cultures
# cette manière de faire est fragile, si les données venait à changer, il faudrait changer le code
numJ2000_cultures <- 19:31
numnomcultures <- rbind(cultures, numJ2000_cultures)

# Kc mensuels par cultures
luparam <- Chargement_param('~/JAMS/modeldata/J2K_Rhone_Irrigation/parameter/', 'landuse.par')
kc <- luparam[19:31, 3:14]
colnames(kc) <- 1:12
rownames(kc) <- cultures

# Période d'irrigation, transformées en fraction mensuelles
irrigstart <- luparam[19:31, 36]
irrigend <- luparam[19:31, 37]
              # on met des valeurs réalistes là où pas d'info de base
# on dirait que cette partie de code à été fait pour des données très précise, pas sur que ça marche avec autre chose
irrigstart[9] <- irrigstart[6]; irrigend[9] <- irrigend[6] # prot==pdt
irrigstart[10] <- 100; irrigend[10] <- 250 # riz
irrigstart[11] <- irrigstart[2]; irrigend[11] <- irrigend[2] # jach et autres = mais
irrigstart[12] <- irrigstart[7]; irrigend[12] <- irrigend[7] # jardins et autres == vergers
irrigstart[13] <- irrigstart[7]; irrigend[13] <- irrigend[7] # industrielles == prairies

date1 <- ymd_hms("2000/01/01 00:00:00")
irrigperiod <- interval(as.Date(irrigstart, date1), as.Date(irrigend, date1))
debutmois <- c(date1, date1+months(1:11))
finmois <- date1+months(1:12)
monthsperiods <- interval(debutmois, finmois)

wheightedperiod <- NULL
for (cult in seq_along(cultures)){
    wheightedperiod <- rbind(wheightedperiod, as.period(intersect(monthsperiods, irrigperiod[cult]))/months(1))
}
wheightedperiod[which(is.na(wheightedperiod))] <- 0 # rmqs : le calcul n'est pas tout à fait exact en raison de la conversion imprécise JulianDay -> Date
                                                    # pour la période d'irrigation ==> à améliorer.


# ETO mensuelle interannuelle par hru irriguée
# --------------------------------------------
# This part of the program reads in a file of data on irrigated HRUs and loops through each HRU to calculate the monthly reference evapotranspiration. 
# It outputs a file called "HRULoop.dat" with the monthly reference evapotranspiration for each HRU. 
# It also assigns a variable to each HRU's monthly reference evapotranspiration, with the variable name being "refET_" followed by the HRU number. 
# So, for example, if HRU 1 had a monthly reference evapotranspiration of 3 mm/month, the program would output a file with a single column and 12 rows, and would also create a variable called "refET_1" with the value 3. 
# If HRU 2 had a monthly reference evapotranspiration of 4 mm/month, the program would output a file with a single column and 12 rows, and would also create a variable called "refET_2" with the value 4. 
# And so on. 
# The program does not produce any visual output. 
# It is important to note that this program requires the source('lib/aggregateZoo_functions.r') in order to run properly.
# 
# - liste des HRUs irriguées 
hrus_all <- read.csv('~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig/Irrigated_AleatoireHRUselect.csv')
irrigated <- hrus_all[which(hrus_all$irrigated ==1),]

HRULoop <- ReadLoopDaily('~/JAMS/modeldata/J2K_Rhone_Irrigation/output/refET/',"HRULoop.dat",TRUE)
Dates <- HRULoop$dates

# - ET0 mensuelles interannuelles
for (myhru in irrigated[,1]){
    myrefET <- HRULoop$Data[which(HRULoop$Data[, 1]==myhru), which(colnames(HRULoop$Data)=='refET')]
    myrefET <- aggregateZoo(zoo(myrefET, Dates), 'm', 'mean')
    assign(paste0('refET_', myhru), myrefET)
}

# 2. Comparaison des besoins théoriques sur chaque HRU et affectation du type de culture irrigué
# ***********************************************************************************************

hrus_et_cantons <- read.dbf('~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig/AleatoirIrrig_hrus_decoupees.dbf')
culture_finale <- NULL
for (hrus in hrus_et_cantons$CAT[order(hrus_et_cantons$CAT)]){

    un_canton <- hrus_et_cantons$CODE_CAN_1[which(hrus_et_cantons$CAT==hrus)]
    sommeprod <- (as.matrix(kc) *as.matrix(wheightedperiod)) %*% as.vector(get(paste0('refET_', hrus))) # une valeur par culture
    refETmoyyear_ponderee <- t(sommeprod)*RGA[as.character(un_canton), 1:13] # .. pondérée par la surface en culture sur le canton.
    culture_retenue <- cultures[which(refETmoyyear_ponderee==max(refETmoyyear_ponderee))]
    numculture_retenue <- numJ2000_cultures[which(refETmoyyear_ponderee==max(refETmoyyear_ponderee))]
    culture_finale <- c(culture_finale, numculture_retenue)
}



# 3. modifications du fichiers hrus.par et des .dbf
# ***************************************************

# hrus.par
#-------------
hruparam <- Chargement_param('~/JAMS/modeldata/J2K_Rhone_Irrigation/parameter/', 'hrus.par')
culture_init <- hruparam[which(hruparam$V1 %in% hrus_et_cantons$CAT[order(hrus_et_cantons$CAT)] ), 11]
#culture_finale[which(culture_init!=culture_finale)]
corresp_irrigtype <- c(2, 1, 1, 1, 2, 1, 2, 1, 2, 3, 1, 1, 2)
irrigtype <- NULL
for (cult in culture_finale){
  ind <- which(numJ2000_cultures==cult)
  irrigtype <- c(irrigtype, corresp_irrigtype[ind])
}
# 11: landuseID
hruparam[which(hruparam$V1 %in% hrus_et_cantons$CAT[order(hrus_et_cantons$CAT)] ), 11] <-culture_finale
# 15: irrigated
hruparam[which(hruparam$V1 %in% hrus_et_cantons$CAT[order(hrus_et_cantons$CAT)] ), 15] <- 1
hruparam[which(!(hruparam$V1 %in% hrus_et_cantons$CAT[order(hrus_et_cantons$CAT)] )), 15] <- 0
# 16: irrig_type
hruparam[which(hruparam$V1 %in% hrus_et_cantons$CAT[order(hrus_et_cantons$CAT)] ), 16] <- irrigtype
hruparam[which(!(hruparam$V1 %in% hrus_et_cantons$CAT[order(hrus_et_cantons$CAT)] )), 16] <- 0


# on remet à 4 les landuseID agricoles des HRUs qui ne sont plus irriguées maintenant (proposition pour plus tard : on met la culture dominante non-irriguée, pour prendre en compte des Kc améliorés)
# c'est un peu compliqué car on n'a plus le hrus.par de référence sans irrigation....
hruparam[which(!(hruparam$V1 %in% hrus_et_cantons$CAT[order(hrus_et_cantons$CAT)] ) & (hruparam$V11>18) ), 11] <-4
# step 1: Montagne (V4 : slope ; V11: landuseID)
indices <- which((hruparam$V4 > 10) & (hruparam$V11== 4))
if (length(indices !=0)){
  hruparam[indices,11] <-12}

# step 2: Dombes
Dombes_Chalaronne <- 6832
Dombes_Veyle <- 6800

reach <- Chargement_param ('~/JAMS/modeldata/J2K_Rhone_Irrigation/parameter/','reach.par')
indID <- 1
indLand <- 11
indSub <- 9

brins_chala <- Topologie(Dombes_Chalaronne, reach)
brins_veyle <- Topologie(Dombes_Veyle, reach)

Total_hru_Chala  <-  NULL
for (k in brins_chala){
  Total_hru_Chala  <-  c (Total_hru_Chala,hruparam[hruparam[,indSub] == k,indID])}
Total_hru_Veyle  <-  NULL
for (k in brins_veyle){
  Total_hru_Veyle  <-  c (Total_hru_Veyle,hruparam[hruparam[,indSub] == k,indID])}

for (k in Total_hru_Chala){
  if(length(which(k == hruparam[which(hruparam[,indLand] == 4 ), indID ])) != 0) {hruparam[which(k == hruparam[,indID]),indLand] <- 18}
}
for (k in Total_hru_Veyle){
  if(length(which(k == hruparam[which(hruparam[,indLand] == 4 ), indID ]))!= 0) {hruparam[which(k == hruparam[,indID]),indLand] <- 18}
}
#test : which(hruparam$V11 == 18)
write_new_paramfile('~/JAMS/modeldata/J2K_Rhone_Irrigation/parameter/hrus.par', hruparam, '~/JAMS/modeldata/J2K_Rhone_Irrigation/parameter/hrus_AleatoirIrrig_NewCult.par')



# .dbf
#-------------

# dbf decoupee sur irrig
hrus_et_cantons$LANDUSEID[order(hrus_et_cantons$CAT)]<-culture_finale
hrus_et_cantons$IRRIG_TYPE[order(hrus_et_cantons$CAT)]<-irrigtype

# dbf de toutes les hrus
hrus_irrigation_all <- read.dbf('~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig/AleatoirIrrig_hrus.dbf')
for (hrus in hrus_et_cantons$CAT){
  hrus_irrigation_all$LANDUSEID[which(hrus_irrigation_all$CAT == hrus)] <- hrus_et_cantons$LANDUSEID[which(hrus_et_cantons$CAT == hrus)]
  hrus_irrigation_all$IRRIG_TYPE[which(hrus_irrigation_all$CAT == hrus)] <- hrus_et_cantons$IRRIG_TYPE[which(hrus_et_cantons$CAT == hrus)]
}
hrus_irrigation_all$IRRIGATED[which(hrus_irrigation_all$CAT %in% hrus_et_cantons$CAT)] <-1
hrus_irrigation_all$IRRIGATED[which(!(hrus_irrigation_all$CAT %in% hrus_et_cantons$CAT))] <-0

# 4/1/2015 correction ex-post pour rétablir 4, 12 ou 18 selon agri plaine, montagne, dombes:
hruparam <- Chargement_param('~/JAMS/modeldata/J2K_Rhone_Irrigation/parameter/','hrus_AleatoirIrrig_NewCult.par')
for (hrus in hruparam$V1){
  hrus_irrigation_all$LANDUSEID[which(hrus_irrigation_all$CAT == hrus)] <- hruparam[which(hruparam$V1==hrus),11]
  hrus_irrigation_all$IRRIGATED[which(hrus_irrigation_all$CAT == hrus)] <- hruparam[which(hruparam$V1==hrus),15]
  hrus_irrigation_all$IRRIG_TYPE[which(hrus_irrigation_all$CAT == hrus)] <- hruparam[which(hruparam$V1==hrus),16]
}
write.dbf(hrus_irrigation_all, '~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig_CultureNew/AleatoirIrrig_CN_hrus_corr.dbf')


# écriture
write.dbf(hrus_et_cantons, '~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig_CultureNew/AleatoirIrrig_CN_hrus_decoupees.dbf')
write.dbf(hrus_irrigation_all, '~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig_CultureNew/AleatoirIrrig_CN_hrus.dbf')

