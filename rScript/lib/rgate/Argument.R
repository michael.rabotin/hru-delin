library(R6)

# class Argument avec R6
Argument <- R6Class("Argument",
  list( # attributs et méthods public
    name = "",
    value = NULL,

    initialize = function(name) {
      stopifnot(is.character(name), length(name) == 1)

      self$name <- name
    }
  )
)

# class Parameter avec R6
Parameter <- R6Class("Parameter", inherit = Argument,
public = list(
  initialize = function(name, value) {
    stopifnot(is.character(value), length(value) == 1)

    self$value <- value
    super$initialize(name = name)
  },

  display = function() {
    json_line = toJSON(self$serialize())
    print(json_line)
    return(json_line)
  },

  getArgument = function(name) {
    if(self$name == name)
      return(self)
    return(NULL)
  },

  getValueAsNumeric = function() {
    return(as.numeric(self$value))
  },

  getValueAsList = function() {
    return(as.list(fromJSON(self$value)))
  },

  serialize = function() {
    return(list(
      "Parameter" = list(
        "name" = self$name,
        "value" = self$value
      )
    ))
  }
))



# class Dictionary avec R6
Dictionary <- R6Class("Dictionary", inherit = Argument,
public = list(
  value = list(),
  initialize = function(name) {
    self$value = list()

    super$initialize(name)
  },

  display = function() {
    json_line = toJSON(self$serialize())
    print(json_line)
    return(json_line)
  },

  addArgument = function(argument) {
    self$value = append(self$value, argument)
    invisible(self)
  },

  addParameter = function(name, value) {
    self$addArgument(Parameter$new(name, value))
  },

  getArgument = function(name) {
    if(self$name == name)
      return(self)

    for(argument in self$value) {
      arg = argument$getArgument(name)
      if(!is.null(arg))
        return(arg)
    }

    return(NULL)
  },

  getParameter = function(name) {
    for(argument in self$value) {
      arg = argument$getArgument(name)
      if(!is.null(arg))
        if(class(arg) == "Parameter")
          return(arg)
    }
  },

  serialize = function() {
    dico <- list(
      "Dictionary" = list(
        "name" = self$name
      )
    )

    value = list()
    for(val in self$value) {
      value = append(value, val$serialize())
    }
    dico$Dictionary$value = value

    return(dico)
  },

  deserialize = function(dico) {
    value_json = dico

    # Dictionary
    for(i in seq_along(value_json$Dictionary$name)) {

      if(!is.null(value_json$Dictionary$name[[i]]) && !is.null(value_json$Dictionary$value[[i]])) {
        dictionary = Dictionary$new(value_json$Dictionary$name[[i]])
        dictionary = dictionary$deserialize(value_json$Dictionary$value[[i]])

        self$value = append(self$value, dictionary)
      }
    }

    # Paramater
    for(i in seq_along(value_json$Parameter$name)) {

      if(!is.null(value_json$Parameter$name[[i]]) && !is.null(value_json$Parameter$value[[i]])) {
        parameter = Parameter$new(value_json$Parameter$name[[i]], value_json$Parameter$value[[i]])

        self$value = append(self$value, parameter)
      }
    }

    invisible(self)
  }
))
